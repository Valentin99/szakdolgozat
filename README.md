# Szakdolgozat

_Adatok hatékony kezelési módjainak vizsgálata .Net környezetben_


## Feladatkiírás

A dolgozat labdarúgás statisztikák példáján keresztül vizsgálja, hogy a .Net keretrendszerben milyen módjai vannak az adatok kezelésének, és hogy ezek milyen formában tekinthetők hatékonynak. A cél az, hogy a játékosok és csapatok teljesítményét játék közben, a korábbi eredmények figyelembevételével valós időben lehessen majd vizsgálni. Például, egy 90 perces játék esetén percenként léptékkel lehet a kimutatásokat aktualizálni, amelynél nagy mennyiségű adat esetében így figyelembe kell venni a gyorsítótárazás lehetőségeit.

Az alkalmazás backend része C# programozási nyelven készül, míg a frontend Angular keretrendszerben. Az adatok perzisztens tárolása hagyományos módon relációs adatbázis segítségével történik. A működés helyességének ellenőrzése automatizált tesztek végzik.


## Részletesebben kidolgozandó részek

* Relációs séma definiálása (alternatív sémák összehasonlításával)
* Relációs adatbázisok hatékonyságának vizsgálata, összevetés NoSQL adatbázisokkal
* Aszinkron végrehajtás, cache-elési mechanizmus kialakítása
* Játékosok teljesítményének becslése, labda birtoklás, sárgalap
* Annak bemutatása, hogy az alkalmazás hogyan teszi elemezhetővé az adatokat

## Rövidebben taglalandó részek

* Online formában elérhető, naprakész statisztikák
* Nagy mennyiségű adat feladolgozási módjai
* .Net/C# webalkalmazás, Angular frontend
* Entity Framework
* Egységtesztek, tesztek szerepe a fejlesztésnél
* Hibák kezelése
* REST API használati módjai

## Futtatás

Frontend:

```csharp
npm install
ng serve
```

Backend:

```csharp
dotnet ef migrations add InitialCreate
dotnet ef database update
dotnet run
```

Restore NuGet:

```csharp
dotnet restore
```

Install .Net CLI

https://docs.microsoft.com/en-us/ef/core/cli/dotnet

.Net 5.0 SDK

https://dotnet.microsoft.com/download

