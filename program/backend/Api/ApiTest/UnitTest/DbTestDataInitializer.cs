﻿using Api.Data;
using Api.Models;
using System;

namespace ApiTest.UnitTest
{
    public class DbTestDataInitializer
    {
        public DbTestDataInitializer() { }

        public static void Seed(SportContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            context.League.AddRange(
                new League() { LeagueId = 1, Name = "Premier League" },
                new League() { LeagueId = 2, Name = "Other" }
            );

            context.Season.AddRange(
                new Season() { SeasonId = 1, Value = "2018/2019", LeagueId = 1 }
            );

            context.Round.AddRange(
                new Round() { RoundId = 1, GameWeek = 1, Simulated = true, SeasonId = 1 },
                new Round() { RoundId = 2, GameWeek = 2, Simulated = true, SeasonId = 1 },
                new Round() { RoundId = 3, GameWeek = 3, Simulated = false, SeasonId = 1 }
            );

            context.Team.AddRange(
                new Team() { TeamId = 1, Name = "Arsenal", Logo = "Arsnenal.png", NickName = "Arsenal", Formation = "4-2-3-1", Stadium = "Emirates Stadium", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 2, Name = "Bournemouth", Logo = "Bournemouth.png", NickName = "Bournemouth", Formation = "4-3-3", Stadium = "Vitality Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 3, Name = "Brighton & Hove Albion", Logo = "Brighton & Hove Albion.png", NickName = "Brighton", Formation = "4-2-3-1", Stadium = "American Express Community Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 4, Name = "Burnley", Logo = "Burnley.png", NickName = "Burnley", Formation = "4-2-4-2", Stadium = "Turf Moor", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 5, Name = "Cardiff City", Logo = "Cardiff City.png", NickName = "Cardiff", Formation = "4-4-2", Stadium = "Millenium Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 6, Name = "Chelsea", Logo = "Chelsea.png", NickName = "Chelsea", Formation = "4-2-3-1", Stadium = "Stamford Bridge", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 7, Name = "Crystal Palace", Logo = "Crystal Palace.png", NickName = "Crystal Palace", Formation = "4-2-3-1", Stadium = "Selhurst Park", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 8, Name = "Everton", Logo = "Everton.png", NickName = "Everton", Formation = "4-2-3-1", Stadium = "Goodison Park", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 9, Name = "Fulham", Logo = "Fulham.png", NickName = "Fulham", Formation = "4-2-3-1", Stadium = "Craven Cottage", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 10, Name = "Huddersfield Town", Logo = "Huddersfield.png", NickName = "Huddersfield", Formation = "4-2-3-1", Stadium = "Kirklees Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 11, Name = "Leicester City", Logo = "Leaicester.png", NickName = "Leicester", Formation = "3-4-2-1", Stadium = "King Power Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 12, Name = "Liverpool", Logo = "Liverpool.png", NickName = "Liverpool", Formation = "4-2-3-1", Stadium = "Anfield Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 13, Name = "Newcastle United", Logo = "NewCastle.png", NickName = "Newcastle", Formation = "4-1-4-1", Stadium = "St James' Park", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 14, Name = "Manchester City", Logo = "ManCity.png", NickName = "Man city", Formation = "4-2-2-2", Stadium = "Etiahd Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 15, Name = "Manchester United", Logo = "ManUnited.png", NickName = "Man united", Formation = "4-4-2", Stadium = "Old Trafford", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 16, Name = "Southampton", Logo = "Southapton.png", NickName = "Soton", Formation = "4-4-2", Stadium = "St.Mary Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 17, Name = "Tottenham Hotspur", Logo = "Sprurs.png", NickName = "Spurs", Formation = "4-2-3-1", Stadium = "Tottenham Hotspur Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 18, Name = "Watford", Logo = "Watford.png", NickName = "Watford", Formation = "4-3-3", Stadium = "Vicarage Road Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 19, Name = "West Ham United", Logo = "WestHam.png", NickName = "West Ham", Formation = "5-3-2", Stadium = "London Stadion", LeagueId = 1, SeasonId = 1 },
                new Team() { TeamId = 20, Name = "Wolverhampton Wanderers", Logo = "Wolwes.png", NickName = "Wolwes", Formation = "5-3-2", Stadium = "Molinexu Stadion", LeagueId = 1, SeasonId = 1 }
            );

            context.Transfer.AddRange(
                new Transfer() { TransferId = 1, SeasonId = 1, TeamId = 2, InvolvedTeamId = 0, PlayerId = 0, LeagueId = 1, LeagueName = "Premier League", TransferSeason = "2018/2019", TeamName = "AFC Bournemouth", PlayerName = "Aaron Ramsdale", InvolvedTeamName = "AFC Wimbledon", Fee = "Loan", CleanedFee = "0", TransferMovement = "out" },
                new Transfer() { TransferId = 2, SeasonId = 1, TeamId = 11, InvolvedTeamId = 0, PlayerId = 0, LeagueId = 1, LeagueName = "Premier League", TransferSeason = "2018/2019", TeamName = "Leicester City", PlayerName = "James Maddison", InvolvedTeamName = "Celtic", Fee = "£22.50m", CleanedFee = "22.5", TransferMovement = "in" },
                new Transfer() { TransferId = 3, SeasonId = 1, TeamId = 11, InvolvedTeamId = 0, PlayerId = 0, LeagueId = 1, LeagueName = "Premier League", TransferSeason = "2018/2019", TeamName = "Leicester City", PlayerName = "Jonny Evans", InvolvedTeamName = "Galatasaray", Fee = "£3.60m", CleanedFee = "3.60", TransferMovement = "in" },
                new Transfer() { TransferId = 4, SeasonId = 1, TeamId = 11, InvolvedTeamId = 0, PlayerId = 0, LeagueId = 1, LeagueName = "Premier League", TransferSeason = "2018/2019", TeamName = "Josh Gordon", PlayerName = "Aaron Ramsdale", InvolvedTeamName = "Burnley", Fee = "Free Transfer", CleanedFee = "0", TransferMovement = "out" }
            );

            context.Match.AddRange(
                new Match() { MatchId = 1, RoundId = 1, HomeTeamId = 15, AwayTeamId = 11, Stadium = "NULL", Referee = "NULL", Simulated = true },
                new Match() { MatchId = 5, RoundId = 1, HomeTeamId = 13, AwayTeamId = 17, Stadium = "NULL", Referee = "NULL", Simulated = true },
                new Match() { MatchId = 12, RoundId = 2, HomeTeamId = 6, AwayTeamId = 1, Stadium = "NULL", Referee = "NULL", Simulated = true },
                new Match() { MatchId = 17, RoundId = 2, HomeTeamId = 3, AwayTeamId = 15, Stadium = "NULL", Referee = "NULL", Simulated = true }
            );

            context.SaveChanges();

            context.MatchFact.AddRange(
                new MatchFact() { MatchFactId = 1, MatchId = 1, Simulated = true, HalfTime = "1-0", FullTime = "2-1", HomeCorners = 11, AwayCorners = 4, HomeFouls = 1, AwayFouls = 5, HomeHalfTimeGoalCount = 1, AwayHalfTimeGoalCount = 0, HomeGoalCount = 2, AwayGoalCount = 0, HomeOffsides = 5, AwayOffsides = 3, HomeRedCards = 0, AwayRedCards = 0, HomeShotsOnTarget = 7, AwayShotsOnTarget = 9, HomeTotalShots = 11, AwayTotalShots = 14, HomeYellowCards = 3, AwayYellowCards = 1 },
                new MatchFact() { MatchFactId = 5, MatchId = 5, Simulated = true, HalfTime = "1-2", FullTime = "2-2", HomeCorners = 3, AwayCorners = 9, HomeFouls = 4, AwayFouls = 2, HomeHalfTimeGoalCount = 1, AwayHalfTimeGoalCount = 2, HomeGoalCount = 2, AwayGoalCount = 2, HomeOffsides = 2, AwayOffsides = 5, HomeRedCards = 0, AwayRedCards = 0, HomeShotsOnTarget = 8, AwayShotsOnTarget = 4, HomeTotalShots = 11, AwayTotalShots = 13, HomeYellowCards = 2, AwayYellowCards = 2 },
                new MatchFact() { MatchFactId = 12, MatchId = 12, Simulated = true, HalfTime = "2-2", FullTime = "4-3", HomeCorners = 13, AwayCorners = 7, HomeFouls = 5, AwayFouls = 6, HomeHalfTimeGoalCount = 2, AwayHalfTimeGoalCount = 2, HomeGoalCount = 4, AwayGoalCount = 3, HomeOffsides = 5, AwayOffsides = 2, HomeRedCards = 0, AwayRedCards = 0, HomeShotsOnTarget = 7, AwayShotsOnTarget = 4, HomeTotalShots = 7, AwayTotalShots = 5, HomeYellowCards = 3, AwayYellowCards = 1 },
                new MatchFact() { MatchFactId = 17, MatchId = 17, Simulated = true, HalfTime = "1-1", FullTime = "1-1", HomeCorners = 2, AwayCorners = 8, HomeFouls = 5, AwayFouls = 3, HomeHalfTimeGoalCount = 1, AwayHalfTimeGoalCount = 1, HomeGoalCount = 1, AwayGoalCount = 1, HomeOffsides = 0, AwayOffsides = 1, HomeRedCards = 0, AwayRedCards = 1, HomeShotsOnTarget = 7, AwayShotsOnTarget = 9, HomeTotalShots = 10, AwayTotalShots = 12, HomeYellowCards = 2, AwayYellowCards = 2 }
            );

            context.SaveChanges();

            context.Player.AddRange(
                new Player() { PlayerId = 1, TeamId = 1, Name = "Pierre-Emerick Aubameyang", NickName = "P. Aubameyang", Age = 30, Birthday = "18/06/1989", Nationality = "Gabon", CurrentTeam = "Arsenal", MarketValue = 57000000, TeamPosition = "LM", TeamYerseyNumber = 14 },
                new Player() { PlayerId = 2, TeamId = 3, Name = "Lewis Dunk", NickName = "L.Dunk", Age = 27, Birthday = "21/11/1991", Nationality = "England", CurrentTeam = "Brighton & Hove Albion", MarketValue = 57000000, TeamPosition = "CB", TeamYerseyNumber = 5 },
                new Player() { PlayerId = 3, TeamId = 3, Name = "Pascal Groay", NickName = "P. Groay", Age = 28, Birthday = "15/06/1991", Nationality = "Germany", CurrentTeam = "Brighton & Hove Albion", MarketValue = 11000, TeamPosition = "RW", TeamYerseyNumber = 13 },
                new Player() { PlayerId = 4, TeamId = 3, Name = "Davy Prppper", NickName = "D. Propper", Age = 27, Birthday = "18/06/1989", Nationality = "Netherlands", CurrentTeam = "Brighton & Hove Albion", MarketValue = 56500, TeamPosition = "LCM", TeamYerseyNumber = 24 },
                new Player() { PlayerId = 5, TeamId = 6, Name = "Olivier Giroud", NickName = "O. Giroud", Age = 32, Birthday = "03/01/1988", Nationality = "France", CurrentTeam = "Chelsea", MarketValue = 784000, TeamPosition = "SUB", TeamYerseyNumber = 18 },
                new Player() { PlayerId = 6, TeamId = 11, Name = "Jonny Evans", NickName = "J. Evans", Age = 31, Birthday = "02/09/1991", Nationality = "Northern Ireland", CurrentTeam = "Leicester City", MarketValue = 123000, TeamPosition = "RCM", TeamYerseyNumber = 6 },
                new Player() { PlayerId = 7, TeamId = 11, Name = "Ayoze Perez ", NickName = "A. Perez", Age = 22, Birthday = "18/06/1989", Nationality = "Spain", CurrentTeam = "Leicester City", MarketValue = 9800000, TeamPosition = "RS", TeamYerseyNumber = 17 },
                new Player() { PlayerId = 8, TeamId = 13, Name = "Joelinton", NickName = "Joelinton", Age = 26, Birthday = "15/03/1993", Nationality = "Brazil", CurrentTeam = "Newcastle United", MarketValue = 800000, TeamPosition = "LDM", TeamYerseyNumber = 8 },
                new Player() { PlayerId = 9, TeamId = 15, Name = "Paul Pogba", NickName = "P. Pogba", Age = 27, Birthday = "18/06/1989", Nationality = "France", CurrentTeam = "Manchester United", MarketValue = 500000, TeamPosition = "ST", TeamYerseyNumber = 6 },
                new Player() { PlayerId = 10, TeamId = 15, Name = "Anthony Martial", NickName = "A. Martial", Age = 23, Birthday = "18/06/1989", Nationality = "France", CurrentTeam = "Manchester United", MarketValue = 1000000, TeamPosition = "CF", TeamYerseyNumber = 10 },
                new Player() { PlayerId = 11, TeamId = 17, Name = "Son", NickName = "H. Son", Age = 33, Birthday = "26/12/1986", Nationality = "Korea", CurrentTeam = "Tottenham Hotspur", MarketValue = 36000000, TeamPosition = "ST", TeamYerseyNumber = 11 }
            );

            context.SaveChanges();

            context.MatchPlayer.AddRange(
                new MatchPlayer() { MatchPlayerId = 1, PlayerId = 6, MatchId = 1, TeamId = 11, PlayerName = "Johny Evans", TeamName = "Leicester City", PlayerStatus = PlayerStatus.Away11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 },
                new MatchPlayer() { MatchPlayerId = 2, PlayerId = 7, MatchId = 1, TeamId = 11, PlayerName = "Ayoze Perez", TeamName = "Leicester City", PlayerStatus = PlayerStatus.Away11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 },
                new MatchPlayer() { MatchPlayerId = 3, PlayerId = 9, MatchId = 1, TeamId = 15, PlayerName = "Paul Pogba", TeamName = "Manchester United", PlayerStatus = PlayerStatus.Home11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 },
                new MatchPlayer() { MatchPlayerId = 4, PlayerId = 10, MatchId = 1, TeamId = 15, PlayerName = "Anthony Martial", TeamName = "Manchester United", PlayerStatus = PlayerStatus.Home11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 },
                new MatchPlayer() { MatchPlayerId = 5, PlayerId = 8, MatchId = 5, TeamId = 13, PlayerName = "Joelinton", TeamName = "Newcastle United", PlayerStatus = PlayerStatus.Home11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 },
                new MatchPlayer() { MatchPlayerId = 6, PlayerId = 11, MatchId = 5, TeamId = 17, PlayerName = "Son", TeamName = "Tottenham Hotspur", PlayerStatus = PlayerStatus.Away11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 },
                new MatchPlayer() { MatchPlayerId = 7, PlayerId = 5, MatchId = 12, TeamId = 6, PlayerName = "Oliver Giroud", TeamName = "Chelsea", PlayerStatus = PlayerStatus.Away11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 },
                new MatchPlayer() { MatchPlayerId = 8, PlayerId = 2, MatchId = 17, TeamId = 3, PlayerName = "Lewis Dunk", TeamName = "Brighton & Hove Albion", PlayerStatus = PlayerStatus.Home11, PlayedMinutes = 90, Goal = 0, Assist = 0, YellowCard = 0, RedCard = 0, StartingTeamPlayer = true, InGame = true, Bench = false, Substitute = false, SubPlayerId = 0 }
            );

            context.SaveChanges();

            context.Shot.AddRange(
                new Shot() { ShotId = 1, Minute = 5, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 11, ShotPlayerId = 6, AssistPlayerId = 0, MatchId = 1, Goal = true },
                new Shot() { ShotId = 2, Minute = 69, Event = Event.Freekick, LeagueId = 1, SeasonId = 1, TeamId = 11, ShotPlayerId = 7, AssistPlayerId = 0, MatchId = 1, Goal = false },
                new Shot() { ShotId = 3, Minute = 64, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 11, ShotPlayerId = 7, AssistPlayerId = 0, MatchId = 1, Goal = true },
                new Shot() { ShotId = 4, Minute = 57, Event = Event.OffTarget, LeagueId = 1, SeasonId = 1, TeamId = 15, ShotPlayerId = 9, AssistPlayerId = 0, MatchId = 1, Goal = false },
                new Shot() { ShotId = 5, Minute = 5, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 15, ShotPlayerId = 10, AssistPlayerId = 0, MatchId = 1, Goal = true },
                new Shot() { ShotId = 6, Minute = 43, Event = Event.Corner, LeagueId = 1, SeasonId = 1, TeamId = 15, ShotPlayerId = 10, AssistPlayerId = 0, MatchId = 1, Goal = false },
                new Shot() { ShotId = 7, Minute = 22, Event = Event.BlockedShot, LeagueId = 1, SeasonId = 1, TeamId = 13, ShotPlayerId = 8, AssistPlayerId = 0, MatchId = 5, Goal = false },
                new Shot() { ShotId = 8, Minute = 24, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 17, ShotPlayerId = 11, AssistPlayerId = 0, MatchId = 5, Goal = true },
                new Shot() { ShotId = 9, Minute = 38, Event = Event.BlockedShot, LeagueId = 1, SeasonId = 1, TeamId = 17, ShotPlayerId = 11, AssistPlayerId = 0, MatchId = 5, Goal = false },
                new Shot() { ShotId = 10, Minute = 6, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 6, ShotPlayerId = 5, AssistPlayerId = 0, MatchId = 12, Goal = true },
                new Shot() { ShotId = 11, Minute = 27, Event = Event.SavedShot, LeagueId = 1, SeasonId = 1, TeamId = 6, ShotPlayerId = 5, AssistPlayerId = 0, MatchId = 12, Goal = false },
                new Shot() { ShotId = 12, Minute = 83, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 6, ShotPlayerId = 5, AssistPlayerId = 0, MatchId = 12, Goal = true },
                new Shot() { ShotId = 13, Minute = 23, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 3, ShotPlayerId = 2, AssistPlayerId = 0, MatchId = 17, Goal = true },
                new Shot() { ShotId = 14, Minute = 37, Event = Event.Goal, LeagueId = 1, SeasonId = 1, TeamId = 3, ShotPlayerId = 2, AssistPlayerId = 0, MatchId = 17, Goal = true }
            );

            context.SaveChanges();

            context.Fault.AddRange(
                new Fault() { FaultId = 1, MatchId = 1, TeamId = 11, PlayerId = 6, Minute = 17, Cardtype = Cardtype.None },
                new Fault() { FaultId = 2, MatchId = 1, TeamId = 11, PlayerId = 7, Minute = 61, Cardtype = Cardtype.None },
                new Fault() { FaultId = 3, MatchId = 1, TeamId = 15, PlayerId = 9, Minute = 10, Cardtype = Cardtype.None },
                new Fault() { FaultId = 4, MatchId = 5, TeamId = 15, PlayerId = 10, Minute = 7, Cardtype = Cardtype.Yellow },
                new Fault() { FaultId = 5, MatchId = 5, TeamId = 13, PlayerId = 8, Minute = 48, Cardtype = Cardtype.Yellow },
                new Fault() { FaultId = 6, MatchId = 5, TeamId = 17, PlayerId = 11, Minute = 14, Cardtype = Cardtype.Yellow },
                new Fault() { FaultId = 7, MatchId = 12, TeamId = 17, PlayerId = 11, Minute = 4, Cardtype = Cardtype.Yellow },
                new Fault() { FaultId = 8, MatchId = 17, TeamId = 6, PlayerId = 5, Minute = 88, Cardtype = Cardtype.Red }
            );

            context.SaveChanges();

            context.Comment.AddRange(
                new Comment() { CommentId = 1, Type = CommentType.Fault, MatchId = 1, Minute = 2, Description = "Ayoze Perez  volt durva" },
                new Comment() { CommentId = 2, Type = CommentType.Offside, MatchId = 1, Minute = 3, Description = "A(z) Leicester City egyik játékosa feledkezett lesen" },
                new Comment() { CommentId = 3, Type = CommentType.Ballstatus, MatchId = 1, Minute = 3, Description = "A bíró fújt a sípjába" },
                new Comment() { CommentId = 4, Type = CommentType.Ballstatus, MatchId = 1, Minute = 5, Description = "55:45 % – ez a labdabirtoklás aránya." },
                new Comment() { CommentId = 5, Type = CommentType.Goal, MatchId = 1, Minute = 5, Description = "Gól! S. McTominay (Manchester United) nyerte az idegek csatáját" },
                new Comment() { CommentId = 6, Type = CommentType.Corner, MatchId = 1, Minute = 7, Description = "De Gea (Manchester United) szögletet végez el" },
                new Comment() { CommentId = 7, Type = CommentType.Blockedshot, MatchId = 1, Minute = 8, Description = "Manchester United csapat játékosa rázza le a védőket a kapu előterében és kapura fejel.A labda messze kapu fölé megy.A labda kiment az alapvonalon" },
                new Comment() { CommentId = 8, Type = CommentType.YellowCard, MatchId = 1, Minute = 9, Description = "Egyértelmű sárga lap.L.Shaw neve bekerül a mérkőzés jegyzőkönyvébe." },
                new Comment() { CommentId = 9, Type = CommentType.Ballstatus, MatchId = 1, Minute = 10, Description = "43:57 % – ez a labdabirtoklás aránya." },
                new Comment() { CommentId = 10, Type = CommentType.Offside, MatchId = 1, Minute = 13, Description = "A(z) Manchester United egyik játékosa feledkezett lesen" }
            );

            context.SaveChanges();

            context.Possesion.AddRange(
                new Possesion() { PossesionId = 1, HomeTeamId = 15, AwayTeamId = 11, Minute = 5, HomeTeamPass = 22, AwayTeamPass = 18, HomePossesion = 55, AwayPossesion = 45, MatchId = 1 },
                new Possesion() { PossesionId = 2, HomeTeamId = 15, AwayTeamId = 11, Minute = 10, HomeTeamPass = 17, AwayTeamPass = 22, HomePossesion = 43, AwayPossesion = 57, MatchId = 1 },
                new Possesion() { PossesionId = 3, HomeTeamId = 15, AwayTeamId = 11, Minute = 15, HomeTeamPass = 20, AwayTeamPass = 20, HomePossesion = 50, AwayPossesion = 50, MatchId = 1 },
                new Possesion() { PossesionId = 4, HomeTeamId = 15, AwayTeamId = 11, Minute = 20, HomeTeamPass = 20, AwayTeamPass = 20, HomePossesion = 50, AwayPossesion = 50, MatchId = 1 },
                new Possesion() { PossesionId = 5, HomeTeamId = 15, AwayTeamId = 11, Minute = 25, HomeTeamPass = 22, AwayTeamPass = 17, HomePossesion = 57, AwayPossesion = 43, MatchId = 1 },
                new Possesion() { PossesionId = 6, HomeTeamId = 15, AwayTeamId = 11, Minute = 30, HomeTeamPass = 25, AwayTeamPass = 22, HomePossesion = 44, AwayPossesion = 56, MatchId = 1 },
                new Possesion() { PossesionId = 7, HomeTeamId = 15, AwayTeamId = 11, Minute = 35, HomeTeamPass = 30, AwayTeamPass = 24, HomePossesion = 40, AwayPossesion = 60, MatchId = 1 },
                new Possesion() { PossesionId = 8, HomeTeamId = 15, AwayTeamId = 11, Minute = 40, HomeTeamPass = 27, AwayTeamPass = 22, HomePossesion = 45, AwayPossesion = 55, MatchId = 1 },
                new Possesion() { PossesionId = 9, HomeTeamId = 15, AwayTeamId = 11, Minute = 45, HomeTeamPass = 18, AwayTeamPass = 20, HomePossesion = 49, AwayPossesion = 51, MatchId = 1 },
                new Possesion() { PossesionId = 10, HomeTeamId = 15, AwayTeamId = 11, Minute = 50, HomeTeamPass = 15, AwayTeamPass = 22, HomePossesion = 47, AwayPossesion = 53, MatchId = 1 },
                new Possesion() { PossesionId = 11, HomeTeamId = 15, AwayTeamId = 11, Minute = 55, HomeTeamPass = 35, AwayTeamPass = 21, HomePossesion = 50, AwayPossesion = 50, MatchId = 1 },
                new Possesion() { PossesionId = 12, HomeTeamId = 15, AwayTeamId = 11, Minute = 60, HomeTeamPass = 15, AwayTeamPass = 22, HomePossesion = 43, AwayPossesion = 57, MatchId = 1 },
                new Possesion() { PossesionId = 13, HomeTeamId = 15, AwayTeamId = 11, Minute = 65, HomeTeamPass = 30, AwayTeamPass = 21, HomePossesion = 46, AwayPossesion = 54, MatchId = 1 },
                new Possesion() { PossesionId = 14, HomeTeamId = 15, AwayTeamId = 11, Minute = 70, HomeTeamPass = 28, AwayTeamPass = 20, HomePossesion = 49, AwayPossesion = 51, MatchId = 1 },
                new Possesion() { PossesionId = 15, HomeTeamId = 15, AwayTeamId = 11, Minute = 75, HomeTeamPass = 48, AwayTeamPass = 16, HomePossesion = 58, AwayPossesion = 42, MatchId = 1 },
                new Possesion() { PossesionId = 16, HomeTeamId = 15, AwayTeamId = 11, Minute = 80, HomeTeamPass = 8, AwayTeamPass = 22, HomePossesion = 44, AwayPossesion = 56, MatchId = 1 },
                new Possesion() { PossesionId = 17, HomeTeamId = 15, AwayTeamId = 11, Minute = 85, HomeTeamPass = 25, AwayTeamPass = 16, HomePossesion = 58, AwayPossesion = 42, MatchId = 1 },
                new Possesion() { PossesionId = 18, HomeTeamId = 15, AwayTeamId = 11, Minute = 90, HomeTeamPass = 29, AwayTeamPass = 20, HomePossesion = 49, AwayPossesion = 51, MatchId = 1 }
            );

            context.SaveChanges();

        }
    }
}
