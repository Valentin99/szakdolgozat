﻿using Api.Controllers;
using Api.Data;
using Api.Dtos.MatchDto.Comment;
using Api.Dtos.MatchDto.LineUp;
using Api.Dtos.MatchDto.MatchReport;
using Api.Dtos.MatchDto.MatchStatistics;
using Api.Dtos.PlayerDto.PositionDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using Api.Services.MatchService;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ApiTest.UnitTest
{
    public class MatchTest
    {

        private readonly MatchService service;

        public static DbContextOptions<SportContext> DbContextOptions { get; }
        const string connectionString = "Filename=match.db";

        static MatchTest()
        {
            DbContextOptions = new DbContextOptionsBuilder<SportContext>()
            .UseSqlite(connectionString)
            .Options;
        }

        public MatchTest()
        {
            var context = new SportContext(DbContextOptions);
            _ = new DbTestDataInitializer();
            DbTestDataInitializer.Seed(context);
            service = new MatchService(context, null);
        }

        [Fact]
        public async Task Task_Match_GetPlayerPositions_Return_Notnull()
        {
            //Arrange  
            var controller = new MatchController(service);

            var positions = new GetPlayerPositionsById()
            {
                MatchId = 1
            };

            //Act  
            var data = await controller.GetPlayerPositions(positions);

            //Assert
            Assert.NotNull(data);
        }


        [Fact]
        public async Task Task_Match_GetPlayerPositions_Return_Positions()
        {
            //Arrange  
            var controller = new MatchController(service);

            var positions = new GetPlayerPositionsById()
            {
                MatchId = 1
            };

            //Act  
            var data = await controller.GetPlayerPositions(positions);

            //Assert
            Assert.IsType<List<PositionDto>>(data);
        }

        [Fact]
        public async Task Task_Match_Return_SimulationHeadline()
        {
            //Arrange  
            var controller = new MatchController(service);

            var headline = new GetMatchHeadLineRequest()
            {
                MatchId = 1
            };

            //Act  
            var data = await controller.SimulationHeadLine(headline);

            //Assert
            Assert.Equal(1, data.MatchId);
        }

        [Fact]
        public async Task Task_Match_Lineup_Return_Players()
        {
            //Arrange  
            var controller = new MatchController(service);

            var lineup = new GetLineupByMatchIdRequest()
            {
                MatchId = 5
            };

            //Act  
            var data = await controller.Lineup(lineup);

            //Assert
            Assert.IsType<List<LineupDto>>(data);
        }

        [Fact]
        public async Task Task_Match_Lineup_Return_Notnull()
        {
            //Arrange  
            var controller = new MatchController(service);

            var lineup = new GetLineupByMatchIdRequest()
            {
                MatchId = 1
            };

            //Act  
            var data = await controller.Lineup(lineup);

            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_Match_Result_Return_Notnull()
        {
            //Arrange  
            var controller = new MatchController(service);

            var result = new GetMatchResultRequest()
            {
                MatchId = 1,
                Minute = 15
            };

            //Act  
            var data = await controller.MatchResultResponse(result);

            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_Match_Result_Equals_TestData()
        {
            //Arrange  
            var controller = new MatchController(service);

            var result = new GetMatchResultRequest()
            {
                MatchId = 1,
                Minute = 3
            };

            //Act  
            var data = await controller.MatchResultResponse(result);

            //Assert
            Assert.Equal("0 : 0", data.Result);
        }

        [Fact]
        public async Task Task_Match_Result_NotEquals_TestData()
        {
            //Arrange  
            var controller = new MatchController(service);

            var result = new GetMatchResultRequest()
            {
                MatchId = 1,
                Minute = 83
            };

            //Act  
            var data = await controller.MatchResultResponse(result);

            //Assert
            Assert.NotEqual("2 : 0", data.Result);
        }

        [Fact]
        public void Match_Comments_Return_Notnull()
        {
            //Arrange  
            var controller = new MatchController(service);

            //Act  

            var getComments_1 = new GetCommentsByMatchIdRequest()
            {
                MatchId = 1,
                Start = 0,
                End = 5
            };

            var comments = controller.NormalComment(getComments_1);

            //Assert
            Assert.NotNull(comments);
        }

        [Fact]
        public void Match_Statistics_Return_Home_NotZero()
        {
            //Arrange  
            var controller = new MatchController(service);

            //Act  

            var getStatistics = new GetMatchStatisticsByIdRequest()
            {
                MatchId = 1,
                Minute = 51
            };

            var statistics = controller.GetMatchStatistics(getStatistics);

            //Assert
            Assert.True(statistics[0].HomeTeam > 0);
        }


        [Fact]
        public void Match_Statistics_Return_Away_NotZero()
        {
            //Arrange  
            var controller = new MatchController(service);

            //Act  

            var getStatistics = new GetMatchStatisticsByIdRequest()
            {
                MatchId = 1,
                Minute = 51
            };

            var statistics = controller.GetMatchStatistics(getStatistics);

            //Assert
            Assert.True(statistics[0].AwayTeam > 0);
        }

        [Fact]
        public void Match_Report_Return_NotZero_Count()
        {
            //Arrange  
            var controller = new MatchController(service);

            //Act  

            var getReport = new GetMatchReportByIdRequest()
            {
                MatchId = 1,
                Minute = 76
            };

            var report = controller.MatchReport(getReport);

            //Assert
            Assert.True(report.Count() > 0);
        }

    }
}
