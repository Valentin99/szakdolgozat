﻿using Api.Controllers;
using Api.Data;
using Api.Dtos.MatchDto.MatchList;
using Api.Dtos.TeamDto.HeadLineDto;
using Api.Dtos.TeamDto.PlayerDto;
using Api.Dtos.TeamDto.TransferDto;
using Api.Services.TeamService;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApiTest.UnitTest
{
    public class TeamTest
    {

        private readonly TeamService service;

        public static DbContextOptions<SportContext> DbContextOptions { get; }
        const string connectionString = "Filename=team.db";

        static TeamTest()
        {
            DbContextOptions = new DbContextOptionsBuilder<SportContext>()
            .UseSqlite(connectionString)
            .Options;
        }

        public TeamTest()
        {
            var context = new SportContext(DbContextOptions);
            _ = new DbTestDataInitializer();
            DbTestDataInitializer.Seed(context);
            service = new TeamService(context);
        }

        [Fact]
        public async Task Task_Team_GetTeamHeadline_Return_Ok()
        {
            //Arrange  
            var controller = new TeamController(service);

            var getHeadline = new GetHeadlineById()
            {
                TeamId = 1
            };

            //Act  
            var data = await controller.GetTeamHeadlineById(getHeadline);

            //Assert
            Assert.IsType<OkObjectResult>(data);
        }

        [Fact]
        public async Task Task_Team_GetTeamResult_Return_Ok()
        {
            //Arrange  
            var controller = new TeamController(service);

            var getResult = new GetMatchResultsByTeamIdRequest()
            {
                TeamId = 1
            };

            //Act  
            var data = await controller.GetMatchResultsByTeamId(getResult);

            //Assert
            Assert.IsType<OkObjectResult>(data);
        }

        [Fact]
        public async Task Task_Team_GetTeamTransfer_Return_Ok()
        {
            //Arrange  
            var controller = new TeamController(service);

            var getTransfers = new GetTeamTransfersById()
            {
                TeamId = 1
            };

            //Act  
            var data = await controller.GeTransfersByTeamId(getTransfers);

            //Assert
            Assert.IsType<OkObjectResult>(data);
        }

        [Fact]
        public async Task Task_Team_GetTeamPlayers_Return_Ok()
        {
            //Arrange  
            var controller = new TeamController(service);

            var getPlayers = new GetTeamPlayersByIdRequest()
            {
                TeamId = 1
            };

            //Act  
            var data = await controller.GetTeamPlayersByTeamId(getPlayers);

            //Assert
            Assert.IsType<OkObjectResult>(data);
        }

    }
}
