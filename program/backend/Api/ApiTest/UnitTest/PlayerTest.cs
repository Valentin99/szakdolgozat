﻿using Api.Controllers;
using Api.Data;
using Api.Dtos.PlayerDto.CareerDto;
using Api.Dtos.PlayerDto.HeadlineDto;
using Api.Dtos.PlayerDto.MatchDto;
using Api.Services.PlayerService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ApiTest.UnitTest
{
    public class PlayerTest
    {
        private readonly PlayerService service;

        public static DbContextOptions<SportContext> DbContextOptions { get; }
        const string connectionString = "Filename=player.db";

        static PlayerTest()
        {
            DbContextOptions = new DbContextOptionsBuilder<SportContext>()
            .UseSqlite(connectionString)
            .Options;
        }

        public PlayerTest()
        {
            var context = new SportContext(DbContextOptions);
            _ = new DbTestDataInitializer();
            DbTestDataInitializer.Seed(context);
            service = new PlayerService(context);
        }

        [Fact]
        public async Task Task_Player_GetPlayerHeadline_Return_PlayerHeadlineDto()
        {
            //Arrange  
            var controller = new PlayerController(service);

            var getHeadline = new GetPlayerHeadlineById()
            {
               PlayerId = 1
            };

            //Act  
            var data = await controller.GetPlayerHeadlineById(getHeadline);

            //Assert
            Assert.IsType<PlayerHeadLineDto>(data);
        }

        [Fact]
        public async Task Task_Player_GetCareer_Return_PlayerCareer()
        {
            //Arrange  
            var controller = new PlayerController(service);

            var getPlayerCareerById = new GetPlayerCareerById()
            {
                PlayerId = 6
            };

            //Act  
            var data = await controller.GetPlayerCareerById(getPlayerCareerById);

            //Assert
            Assert.IsType<List<CareerDto>>(data);
        }

        [Fact]
        public async Task Task_Player_GetMatch_Return_Macthes()
        {
            //Arrange  
            var controller = new PlayerController(service);

            var getPlayerMatchListById = new GetPlayerMatchListById()
            {
                PlayerId = 6
            };

            //Act  
            var data = await controller.GetPlayerMatch(getPlayerMatchListById);

            //Assert
            Assert.IsType<List<PlayerMatchDto>>(data);
        }

    }
}
