﻿using Api.Controllers;
using Api.Data;
using Api.Dtos.LeagueDto;
using Api.Dtos.LeagueDto.CardDto;
using Api.Dtos.LeagueDto.GoalscorerlistDto;
using Api.Dtos.LeagueDto.LastResultDto;
using Api.Dtos.LeagueDto.StandingDto;
using Api.Dtos.SimulationDto.RoundDto;
using Api.Models;
using Api.Services.LeagueService;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ApiTest.UnitTest
{
    public class LeagueTest
    {
        private readonly LeagueService _service;

        public static DbContextOptions<SportContext> DbContextOptions { get; }
        const string connectionString = "Filename=league.db";

        static LeagueTest()
        {
            DbContextOptions = new DbContextOptionsBuilder<SportContext>()
            .UseSqlite(connectionString)
            .Options;
        }

        public LeagueTest()
        {
            var context = new SportContext(DbContextOptions);
            _ = new DbTestDataInitializer();
            DbTestDataInitializer.Seed(context);
            _service = new LeagueService(context);
        }

        [Fact]
        public async Task Task_GetLeague_RedCard_Return_Players()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getCardById = new GetCardById()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.RedCard(getCardById);

            //Assert
            Assert.IsType<List<RedcardDto>>(data);
        }

        [Fact]
        public async Task Task_GetLeague_RedCard_Return_Notnull()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getCardById = new GetCardById()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.RedCard(getCardById);

            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_GetLeague_YellowCard_Return_Notnull()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getCardById = new GetCardById()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.YellowCard(getCardById);

            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_GetLeague_YellowCard_Return_Players()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getCardById = new GetCardById()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.YellowCard(getCardById);

            //Assert
            Assert.IsType<List<YellowcardDto>>(data);
        }

        [Fact]
        public async Task Task_GetLeague_GoalScorers_Return_Notnull()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getGoalscorerListById = new GetGoalscorerListById()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.Goalscorer(getGoalscorerListById);

            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_GetLeague_GoalScorers_Return_Goalscorers()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getGoalscorerListById = new GetGoalscorerListById()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.Goalscorer(getGoalscorerListById);

            //Assert
            Assert.IsType<List<GoalscorerDto>>(data);
        }

        [Fact]
        public async Task Task_GetLeague_Results_Return_Notnull()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getLastResultsByIdRequest = new GetLastResultsByIdRequest()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.Result(getLastResultsByIdRequest);

            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_GetLeague_Results_Return_LastResults()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getLastResultsByIdRequest = new GetLastResultsByIdRequest()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.Result(getLastResultsByIdRequest);

            //Assert
            Assert.IsType<List<LastResultDto>>(data);
        }

        [Fact]
        public async Task Task_GetLeague_Summary_Headline_Return_OkResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var createRoundSimulationRequest = new CreateRoundSimulationRequest()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.HeadLine(createRoundSimulationRequest);

            //Assert
            Assert.IsType<OkObjectResult>(data);
        }

        [Fact]
        public async Task Task_GetLeague_Summary_Headline_Return_Notnull()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var createRoundSimulationRequest = new CreateRoundSimulationRequest()
            {
                RoundId = 1
            };

            //Act  
            var data = await controller.HeadLine(createRoundSimulationRequest);

            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_GetLeague_Standings_Return_OkResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getLeagueStandingsRequest = new GetLeagueStandingsRequest()
            {
                LeagueId = 1,
                SeasonId = 1,
                RoundId = 1
            };

            //Act  
            var data = await controller.LeagueStanding(getLeagueStandingsRequest);


            //Assert
            Assert.IsType<OkObjectResult>(data);
        }

        [Fact]
        public async Task Task_GetLeague_Standings_Return_Notnull()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            var getLeagueStandingsRequest = new GetLeagueStandingsRequest()
            {
                LeagueId = 1,
                SeasonId = 1,
                RoundId = 1
            };

            //Act  
            var data = await controller.LeagueStanding(getLeagueStandingsRequest);


            //Assert
            Assert.NotNull(data);
        }

        [Fact]
        public async Task Task_GetLeague_Standings_Return_BadRequestResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            //Act  
            var data = await controller.LeagueStanding(null);
                
                
            //Assert
            Assert.IsType<BadRequestResult>(data);
        }


        [Fact]
        public async Task Task_GetLeagues_Return_OkResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            //Act  
            var data = await controller.GetLeagues();

            //Assert  
            Assert.IsType<OkObjectResult>(data);
        }

        [Fact]
        public async void Task_GetLeagues_MatchResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            //Act  
            var data = await controller.GetLeagues();

            //Assert  
            Assert.IsType<OkObjectResult>(data);

            var okResult = data.Should().BeOfType<OkObjectResult>().Subject;
            var leagues = okResult.Value.Should().BeAssignableTo<List<League>>().Subject;

            Assert.Equal("Premier League", leagues[0].Name);
            Assert.Equal("Other", leagues[1].Name);
        }

        [Fact]
        public async Task Task_GetLeagueById_Return_OkResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            GetLeagueByIdRequest getLeagueByIdRequest = new GetLeagueByIdRequest()
            {
                Id = 2
            };

            //Act  
            var data = await controller.GetLeagueById(getLeagueByIdRequest);
            //Assert  
            Assert.IsType<OkObjectResult>(data);
        }

        [Fact]
        public async void Task_GetLeagueById_Return_NotFoundResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            GetLeagueByIdRequest getLeagueByIdRequest = new GetLeagueByIdRequest()
            {
                Id = 10
            };

            //Act  
            var data = await controller.GetLeagueById(getLeagueByIdRequest);

            //Assert  
            Assert.IsType<NotFoundResult>(data);
        }

        [Fact]
        public async void Task_GetLeagueById_Return_BadRequestResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            //Act  
            var data = await controller.GetLeagueById(null);

            //Assert  
            Assert.IsType<BadRequestResult>(data);
        }


        [Fact]
        public async void Task_GetLeagueById_MatchResult()
        {
            //Arrange  
            var controller = new LeagueController(_service);

            GetLeagueByIdRequest getLeagueByIdRequest = new GetLeagueByIdRequest()
            {
                Id = 2
            };

            //Act  
            var data = await controller.GetLeagueById(getLeagueByIdRequest);

            //Assert  
            Assert.IsType<OkObjectResult>(data);

            var okResult = data.Should().BeOfType<OkObjectResult>().Subject;
            var league = okResult.Value.Should().BeAssignableTo<League>().Subject;

            Assert.Equal("Other", league.Name);
        }

    }
}
