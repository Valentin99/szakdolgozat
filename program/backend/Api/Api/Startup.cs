using Api.Data;
using Api.Repository.LeagueRepository;
using Api.Repository.MatchRepository;
using Api.Repository.PlayerRepository;
using Api.Repository.SimulationRepository;
using Api.Repository.TeamRepository;
using Api.Services.LeagueService;
using Api.Services.MatchService;
using Api.Services.PlayerService;
using Api.Services.SimulationService;
using Api.Services.TeamService;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        //Cors Policy

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            // Memory Cache
            services.AddMemoryCache();

            // MSSQL

            // services.AddDbContext<SportContext>(options =>
            // options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // SQLite

            services.AddDbContext<SportContext>(options => options.UseSqlite("Filename=sport.db"));

            services.AddCors(
                options =>
                {
                    options.AddPolicy(MyAllowSpecificOrigins, builder => builder
                    .WithOrigins("*")
                    .AllowAnyHeader()
                    .AllowAnyMethod());

                });

            // Newtonsoft Json 

            services.AddControllers().AddNewtonsoftJson(
            options =>
            {
                options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            });

            // Dependency Injection

            // simulation evaluation

            services.AddScoped<ILeagueService, LeagueService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IPlayerService, PlayerService>();
            services.AddScoped<IMatchService, MatchService>();

            // simulation

            services.AddScoped<ISimulationService, SimulationService>();
            services.AddScoped<ICommentService, SimulationService>();
            services.AddScoped<IEventService, SimulationService>();

            // db Initializer

            services.AddScoped<IDbInitializer, DbInitializer>();

            // Open Api Swagger Documentation

            services.AddSwaggerGen();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {

            // Initial database

            dbInitializer.Initialize();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Cors

            app.UseCors(MyAllowSpecificOrigins);

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
            });

            app.UseStaticFiles();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
