﻿using Api.Dtos.MatchDto.Comment;
using Api.Dtos.MatchDto.LineUp;
using Api.Dtos.MatchDto.MatchReport;
using Api.Dtos.MatchDto.MatchStatistics;
using Api.Dtos.PlayerDto.PositionDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Repository.MatchRepository
{
    public interface IMatchService
    {
        Task<SimulationHeadLineDto> SimulationHeadLine(GetMatchHeadLineRequest getMatchHeadLineRequest);
        Task<List<PositionDto>> PlayerPosition(GetPlayerPositionsById getPlayerPositionsById);
        List<PositionDto> GenerateLineup(List<PositionDto> positions, TmpFormation tmpFormation);
        TmpFormation GetFormationInfo(int teamId, string team);
        Task<MatchResultDto> MatchResultResponse(GetMatchResultRequest getMatchResult);
        Task<List<LineupDto>> Lineup(GetLineupByMatchIdRequest getLineupRequest);
        List<CommentDto> GetNormalComments(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest);                            // normál kommentek
        Task<List<CommentDto>> GetComments(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest);                            // gyorsítótárazott kommentek
        Task<List<CommentDto>> GetCachedComments(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest);
        List<MatchStatisticsDto> GetMatchStatistics(GetMatchStatisticsByIdRequest getMatchStatisticsByIdRequest);
        List<MatchReportDto> MatchReport(GetMatchReportByIdRequest getMatchReportByIdRequest);
    }
}
