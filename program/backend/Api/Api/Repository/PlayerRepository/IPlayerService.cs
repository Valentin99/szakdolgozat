﻿using Api.Dtos.PlayerDto.CareerDto;
using Api.Dtos.PlayerDto.HeadlineDto;
using Api.Dtos.PlayerDto.MatchDto;
using Api.Dtos.PlayerDto.PositionDto;
using Api.Dtos.PlayerDto.SettingsDto;
using Api.Dtos.TeamDto.SettingsDto;
using Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Repository.PlayerRepository
{
    public interface IPlayerService
    {
        Task<PlayerHeadLineDto> GetPlayerHeadlineById(GetPlayerHeadlineById getPlayerHeadlineById);
        Task<List<CareerDto>> GetPlayerCareerById(GetPlayerCareerById getPlayerCareerById);
        Task<List<PlayerMatchDto>> PlayerMatch(GetPlayerMatchListById getPlayerMatchListById);
        void DeletePlayer(int id);
        void CreatePlayer(PlayerDto playerDto);
        void UpdatePlayer(PlayerDto playerDto, int id);
        List<PlayerDto> GetPlayers();
    }
}
