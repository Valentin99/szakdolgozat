﻿using Api.Dtos.MatchDto.MatchList;
using Api.Dtos.TeamDto.HeadLineDto;
using Api.Dtos.TeamDto.PlayerDto;
using Api.Dtos.TeamDto.SettingsDto;
using Api.Dtos.TeamDto.TransferDto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Repository.TeamRepository
{
    public interface ITeamService
    {
        Task<List<TeamPlayerDto>> GetTeamPlayersByTeamId(GetTeamPlayersByIdRequest getTeamPlayersByIdRequest);
        Task<List<MatchDto>> GetMatchResults(GetMatchResultsByTeamIdRequest getMatchResultByIdRequest);
        Task<List<TeamTransferDto>> TeamTransfers(GetTeamTransfersById getTeamTransfersById);
        Task<TeamHeadlineDto> TeamHeadline(GetHeadlineById getHeadlineById);
        void CreateTeam(TeamDto TeamDto);
        string UpdateTeam(TeamDto teamDto, int id);
        void DeleteTeam(int id);
        List<TeamDto> GetTeams();
    }
}
