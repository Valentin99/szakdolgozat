﻿using Api.Dtos.LeagueDto;
using Api.Dtos.LeagueDto.CardDto;
using Api.Dtos.LeagueDto.CleansheetDto;
using Api.Dtos.LeagueDto.GoalscorerlistDto;
using Api.Dtos.LeagueDto.LastResultDto;
using Api.Dtos.LeagueDto.StandingDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using Api.Dtos.SimulationDto.RoundDto;
using Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Repository.LeagueRepository
{
    /// <summary>
    /// Dependency Injection for League Entity
    /// </summary>
    public interface ILeagueService
    {
        Task<List<GoalscorerDto>> GoalscorerList(GetGoalscorerListById getGoalscorerListById);
        Task<List<YellowcardDto>> YellowCard(GetCardById getCardById);
        Task<List<RedcardDto>> RedCard(GetCardById getCardById);
        Task<List<LastResultDto>> Result(GetLastResultsByIdRequest getLastResultsByIdRequest);
        Task<List<TeamStandingDto>> Standing(GetLeagueStandingsRequest getLeagueStandingsRequest);
        Task<SimulationPageHeadlineDto> SimulationPageHeadLineRespone(CreateRoundSimulationRequest createRoundSimulationRequest);

        // Unit Test Example
        Task<List<League>> GetLeagues();
        Task<League> GetLeagueById(GetLeagueByIdRequest getLeagueByIdRequest);

    }
}
