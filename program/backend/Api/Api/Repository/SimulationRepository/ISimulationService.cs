﻿using Api.Dtos.MatchDto.LineUp;
using Api.Dtos.SimulationDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using Api.Dtos.SimulationDto.MatchDto;
using Api.Dtos.SimulationDto.RoundDto;
using Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Repository.SimulationRepository
{
    public interface ISimulationService
    {
        Task<List<RoundMatchDto>> CreateRoundSimulationRequest(CreateRoundSimulationRequest createSimulationRequest);
        Task<SimulationPageHeadlineDto> SimulationPageHeadLineRespone(CreateRoundSimulationRequest createSimulationRequest);
        Task<SimulationHeadLineDto> SimulationHeadLineResponse(CreateMatchSimulationRequest createMatchSimulationRequest);
        Task BeginMatchSimulationRequest(CreateMatchSimulationRequest createMatchSimulationRequest);
        void GenerateStartingTeam(int matchId, int homeTeamId, int awayTeamId);
        void GenerateSubstitutes(int matchId, int homeTeamId, int awayTeamId);
        void GenerateShot(int matchId, List<MatchPlayer> hometeam, List<MatchPlayer> awayteam); 
        void GenerateFault(int matchId, List<MatchPlayer> hometeam, List<MatchPlayer> awayteam);
        Task GenerateSeason(int leagueId);
    }
}
