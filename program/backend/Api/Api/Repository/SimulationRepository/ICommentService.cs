﻿using Api.Dtos.MatchDto.Comment;
using Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Repository.SimulationRepository
{
    public interface ICommentService
    {
        void BlockedShotComment(int matchId, int minute, int category, string shotPlayer, string attackingTeam);
        void SavedShotComment(int matchId, int minute, int category, string shotPlayer, string shotTeam, string referee);
        void CornerComment(int matchId, int minute, int category, string cornerKicker, string cornerTeam);
        void SubStituteComment(int matchId, int minute, int category, string inPlayer, string outPlayer, string substituteTeam);
        void FaultComment(int matchId, int minute, int category, string abusePlayer);
        void YellowCardComment(int matchId, int minute, int category, string abusePlayer);
        void GoalComment(int category, int matchId, int minute, string player, string team);
        void RedCardComment(int matchId, int minute, string abusePlayer);
        void BallStatusComment(int matchId, string team);
        void FreekickComment(int matchId, int minute, int category, string freeKickPlayer, string freeKickTeam, string referee);
        void OffsideComment(int matchId, int minute, int category, string offsideTeam);
        void PossesionComment(int matchId, int hometeamId, int awayteamId);
        void AddComment(Comment comment);
        void SecondHalfBeginCommentar(int matchId, string referee);
        void GameExtensionComment(int matchId);
    }
}
