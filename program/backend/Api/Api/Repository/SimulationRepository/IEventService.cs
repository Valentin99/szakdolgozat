﻿using Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Repository.SimulationRepository
{
    public interface IEventService
    {
        void GenerateOffside(int matchId, int hometeamId, int hometeamOffside, int awayteamId, int awayteamoffside);        // les
        void GenerateOffTargetShot(int matchId, int teamId, int offtarget);                                                 // kaput nem találó lövések száma
        void GenerateBlockedShot(int matchId, int teamId, int blockedShot, List<MatchPlayer> matchPlayers);                 // blokkolt lövések száma
        void GenerateSavedShot(int matchid, int teamid, int savedShot, List<MatchPlayer> matchPlayers);                     // védett lövések száma
        void GenerateGoal(int matchId, int teamid, int firsthalf, int secondhalf, List<MatchPlayer> matchPlayers);          // első és második félidőben szerzett gólok száma a first és second half
        void GenerateFreeKick(int matchId, int teamId, int freekick, List<MatchPlayer> matchPlayers);                       // szabadrugás
        void GenerateCorner(int matchId, int teamId, int corner, List<MatchPlayer> matchPlayers);                                                           // szöglet
        void GenerateYellowCard(int matchId, int teamId, int yellowCard, List<MatchPlayer> players);                        // sárga lap
        void GenerateRedCard(int matchId, int teamId, int redCard, List<MatchPlayer> players);                              // piros lap
    }
}
