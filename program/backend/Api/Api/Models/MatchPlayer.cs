﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    public enum PlayerStatus
    {
        Home11,                 // 0
        Away11,                 // 1
        Home11Sub,              // 2
        Away11Sub,              // 3
        HomeSub,                // 4
        AwaySub,                // 5
        HomeBench,              // 6
        AwayBench               // 7
    }

    /// <summary>
    /// Ez a tábla leírja azt, hogy mely játékosok vettek részt egy mérközésen továbbá a mérközésen elért teljesítmény
    /// statisztika itt is könyvelésre kerül a csapat és a játékosok számára. 
    /// </summary>
    public class MatchPlayer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MatchPlayerId { get; set; }                  // elsődleges kulcs
        public Player Player { get; set; }
        public int PlayerId { get; set; }                       // idegen kulcs     
        public Match Match { get; set; }
        public int MatchId { get; set; }                        // idegen kulcs        
        public int TeamId { get; set; }                         // idegen kulcs
        [MaxLength(100)]
        public string PlayerName { get; set; }
        [MaxLength(100)]
        public string TeamName { get; set; }
        public PlayerStatus PlayerStatus { get; set; }          // megadja, hogy valaki kezdő, kezdő és csere, csere vagy kispados
        public int PlayedMinutes { get; set; }                  // játszott percek száma
        public int Goal { get; set; }                           // gól
        public int Assist { get; set; }                         // assziszt
        public int YellowCard { get; set; }                     // sárga lap
        public int RedCard { get; set; }                        // piros lap
        public bool StartingTeamPlayer { get; set; }            // kezdőcsapat játékos
        public bool InGame { get; set; }                        // jelenleg játszik
        public bool Bench { get; set; }                         // kispados játékos
        public bool Substitute { get; set; }                    // becserélt vagy lecserélt játékos esetén igaz
        public int SubPlayerId { get; set; }                    // van értéke ha egy játékost felcserélek és a lejövő játékos id-ját tárolom benne
    }
}
