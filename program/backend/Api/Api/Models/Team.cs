﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    /// <summary>
    /// csapat tábla
    /// </summary>
    public class Team
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TeamId { get; set; }                         // elsődleges kulcs
        [MaxLength(30)]
        public string Name { get; set; }                        // csapat neve
        [MaxLength(40)]
        public string Logo { get; set; }                        // logo
        [MaxLength(30)]
        public string NickName { get; set; }                    // csapat beceneve
        [MaxLength(10)]
        public string Formation { get; set; }                   // preferált formáció
        [MaxLength(100)]
        public string Stadium { get; set; }                     // stadion
        public int SeasonId { get; set; }                       // idegen kulcs
        public Season Season { get; set; }
        public int LeagueId { get; set; }                       // idegen kulcs
        public ICollection<Player> Player { get; set; }         // játékosok 1:n
    }
}
