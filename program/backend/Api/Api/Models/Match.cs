﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    /// <summary>
    /// 1 db mérközést leíró tábla
    /// </summary>
    public class Match
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MatchId { get; set; }                                // elsődleges kulcs
        public Round Round { get; set; }
        public int RoundId { get; set; }                                // idegen kulcs
        public int HomeTeamId { get; set; }                             // hazai csapat idegen kulcs
        public int AwayTeamId { get; set; }                             // vendég csapat idegen kulcs
        [MaxLength(30)]
        public string Stadium { get; set; }                             // stadion
        [MaxLength(30)]
        public string Referee { get; set; }                             // bíró
        public bool Simulated { get; set; }                             // logikai változó, hogy egy mérközés szimulált-e
        public MatchFact MatchFact { get; set; }                        // match 1:1 matchfact mérkőzés jegyzőkönyve
        public ICollection<MatchPlayer> MatchPlayer { get; set; }       // résztvevő játékosok
        public ICollection<Shot> Shot { get; set; }                     // kapura lövés, védett, blokkolt, gól, kaput nem találó lövés
        public ICollection<Fault> Fault { get; set; }                   // sárga lap és piros lap és szabálytalanság
        public ICollection<Comment> Comment { get; set; }               // mérközés kommentár
        public ICollection<Possesion> Possesion { get; set; }
    }
}
