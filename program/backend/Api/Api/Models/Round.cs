﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    /// <summary>
    /// 1 fordulót ír le ez a tábla
    /// </summary>
    public class Round
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RoundId { get; set; }                    // elsődleges kulcs
        public int GameWeek { get; set; }                   // forduló száma
        public bool Simulated { get; set; }                 // logikai változó igaz ha szimulált
        public Season Season { get; set; }
        public int SeasonId { get; set; }                   // idegen kulcs
        public ICollection<Match> Match { get; set; }
    }
}
