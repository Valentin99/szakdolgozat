﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using Api.Static;

namespace Api.Models
{
    /// <summary>
    /// 1 szezont ír le ez a tábla
    /// </summary>
    public class Season
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SeasonId { get; set; }                           // elsődleges kulcs
        [MaxLength(20)]
        public string Value { get; set; }                           // szezon
        public League League { get; set; }
        public int LeagueId { get; set; }                           // idegen kulcs
        public ICollection<Transfer> Transfer { get; set; }         // átigazolások 1:n
        public ICollection<Round> Round { get; set; }               // fordulók 1:n
        public ICollection<Team> Team { get; set; }                 // csapatok 1:n
    }
}
