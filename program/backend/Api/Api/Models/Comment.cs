﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{

    public enum CommentType
    {
        Goal,                   
        Fault,                 
        Card,                   
        Freekick,              
        Blockedshot,            
        Savedshot,              
        Corner,                
        SecondHalf,            
        GameExtension,         
        SubStitute,            
        YellowCard,            
        RedCard,                
        Ballstatus,             
        Offside,               
        Possesion,             
    }

    /// <summary>
    /// Ez a tábla a 90 perces mérközés eseményeit kommentálja 1-1 szöveges üzenetben
    /// </summary>

    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }          // elsődleges kulcs
        public CommentType Type { get; set; }       // esemény típusa
        public int MatchId { get; set; }            // idegen kulcs
        public int Minute { get; set; }             // perc
        [MaxLength(400)]
        public string Description { get; set; }     // leírás
        public Match Match { get; set; }
    }
}
