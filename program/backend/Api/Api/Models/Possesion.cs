﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    public class Possesion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PossesionId { get; set; }                                // elsődleges kulcs
        public int HomeTeamId { get; set; }                                 // idegen kulcs
        public int AwayTeamId { get; set; }                                 // idegen kulcs
        public int Minute { get; set; }                                     // perc
        public int HomeTeamPass { get; set; }                               // hazai csapat passzok száma
        public int AwayTeamPass { get; set; }                               // vendég csapat passzok száma
        public int HomePossesion { get; set; }                              // hazai csapat labdabirtoklási arány
        public int AwayPossesion { get; set; }                              // vendég csapat labdabirtoklási arány
        public Match Match { get; set; }
        public int MatchId { get; set; }                                    // idegen kulcs
    }
}
