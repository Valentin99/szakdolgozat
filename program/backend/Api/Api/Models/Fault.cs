﻿namespace Api.Models
{
    /// <summary>
    /// Kártya típusa
    /// </summary>
    public enum Cardtype
    {
        Red,
        Yellow,
        None
    }


    /// <summary>
    /// Játékosok büntető kártyáit és szabálytalanságait gyüjti tábla egy mérközés alatt
    /// </summary>
    public class Fault
    {
        public int FaultId { get; set; }         // Elsődleges kulcs
        public Match Match { get; set; }
        public int MatchId { get; set; }        // Idegen kulcs
        public int TeamId { get; set; }         // Idegen kulcs
        public int PlayerId { get; set; }       // Idegen kulcs
        public int Minute { get; set; }         // Perc
        public Cardtype Cardtype { get; set; }  // Kártya típusa
    }
}
