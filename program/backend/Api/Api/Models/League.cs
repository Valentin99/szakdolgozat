﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    /// <summary>
    /// Ligákat gyüjtő tábla
    /// </summary>
    public class League
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LeagueId { get; set; }                       // elsődleges kulcs
        [MaxLength(30)]
        public string Name { get; set;  }                        // liga neve
        public ICollection<Season> Season { get; set; }         // szezonok 1:n
    }
}
