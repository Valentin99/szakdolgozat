﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{

    /// <summary>
    /// lövés kimenete
    /// </summary>
    public enum Event
    {
        OffTarget,      // kaput nem találó lövés
        OnTarget,       // kaput eltaláló lövés
        Goal,           // gól
        AssistedGoal,   // assziszt
        BlockedShot,    // blokkolt lövés
        SavedShot,      // védett lövés
        Freekick,       // szabadrugás
        Penalty,        // büntető
        Corner          // Szöglet
    }

    public class Shot
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShotId { get; set; }                         // elsődleges kulcs
        public int Minute { get; set; }                         // perc
        public Event Event { get; set; }                        // lövés kimenete
        public int LeagueId { get; set; }                       // idegenn kulcs
        public int SeasonId { get; set; }                       // idegen kulcs
        public int TeamId { get; set; }                         // idegen kulcs
        public int ShotPlayerId { get; set; }                   // idegen kulcs
        public int AssistPlayerId { get; set; }                   // idegen kulcs
        public int MatchId { get; set; }                        // idegen kulcs
        public bool Goal { get; set; }
        public Match Match { get; set; }                    
    }
}