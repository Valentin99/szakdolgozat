﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    /// <summary>
    /// Mérközés jegyzőkönyvet leíró tábla amiből ki vannak generáltatva az adatok
    /// </summary>
    public class MatchFact
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MatchFactId { get; set; }                        // elsődleges kulcs
        public int MatchId { get; set; }                            // idegen kulcs
        public Match Match { get; set; }
        public bool Simulated { get; set; }                         // logikai változó, hogy egy mérközés szimulált-e
        [MaxLength(10)]
        public string HalfTime { get; set; }                        // félidő 
        [MaxLength(10)]
        public string FullTime { get; set; }                        // végeredmény
        public int HomeTotalShots { get; set; }                     // hazai csapat összes lövések száma
        public int AwayTotalShots { get; set; }                     // vendég csapat összes lövések száma
        public int HomeShotsOnTarget { get; set; }                  // hazai csapat félidei gólok száma
        public int AwayShotsOnTarget { get; set; }                  // vendég csapat félidei gólok száma
        public int HomeGoalCount { get; set; }                      // hazai csapat gólok száma végeredmény
        public int AwayGoalCount { get; set; }                      // vendég csapat gólok száma végeredmény
        public int HomeHalfTimeGoalCount { get; set; }              // hazai csapat félidei gólok száma
        public int AwayHalfTimeGoalCount { get; set; }              // vendég csapat félidei gólok száma
        public int HomeOffsides { get; set; }                       // hazai csapat les
        public int AwayOffsides { get; set; }                       // vendég csapat les
        public int HomeCorners { get; set; }                        // hazai csapat szögletei
        public int AwayCorners { get; set; }                        // vendég csapat szögletei
        public int HomeYellowCards { get; set; }                    // hazai csapat sárga lap
        public int AwayYellowCards { get; set; }                    // vendég csapat sárga lap
        public int HomeRedCards { get; set; }                       // hazai csapat piros lap
        public int AwayRedCards { get; set; }                       // vendég csapat piros lap
        public int HomeFouls { get; set; }                          // hazai csapat szabálytalanság
        public int AwayFouls { get; set; }                          // vendég csapat szabálytalanság
    }
}
