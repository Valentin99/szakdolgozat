﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    /// <summary>
    /// játékos tábla
    /// </summary>
    public class Player
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PlayerId { get; set; }                                           // elsődleges kulcs
        public Team Team { get; set; }
        public int TeamId { get; set; }                                             // idegen kulcs
        [MaxLength(100)]
        public string Name { get; set; }                                            // név
        [MaxLength(100)]
        public string NickName { get; set; }                                        // becenév
        public int Age { get; set; }                                                // kor
        [MaxLength(30)]
        public string Birthday { get; set; }                                        // születésnap
        [MaxLength(30)]
        public string Nationality { get; set; }                                     // nemzetiség
        [MaxLength(30)]
        public string CurrentTeam { get; set; }                                     // jelenlegi csapat
        public int MarketValue { get; set; }                                        // piaci érték
        [MaxLength(10)]
        public string TeamPosition { get; set; }                                    // pozíció
        public int TeamYerseyNumber { get; set; }                                   // mez szám
        public ICollection<MatchPlayer> MatchPlayer { get; set; }                   // lejátszott mérközések
    }
}
