﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Models
{
    /// <summary>
    /// átigazolás tábla
    /// </summary>
    public class Transfer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TransferId { get; set; }                     // elsődleges kulcs
        public Season Season { get; set; }
        public int SeasonId { get; set; }                       // idegen kulcs
        public int TeamId { get; set; }                         // idegen kulcs
        public int InvolvedTeamId { get; set; }                 // idegen kulcs
        public int PlayerId { get; set; }                       // idegen kulcs
        public int LeagueId { get; set; }                       // idegen kulcs
        [MaxLength(60)]
        public string LeagueName { get; set; }                  // liga neve
        [MaxLength(30)]
        public string TransferSeason { get; set; }              // átigazolási szezon
        [MaxLength(100)]
        public string TeamName { get; set; }                    // igazoló csapat neve
        [MaxLength(100)]
        public string PlayerName { get; set; }                  // játékos neve
        [MaxLength(100)]
        public string InvolvedTeamName { get; set; }            // korábbi csapat neve
        [MaxLength(30)]
        public string Fee { get; set; }                         // átigazolás összege
        [MaxLength(30)]
        public string CleanedFee { get; set; }                  // átigazolás összege
        [MaxLength(30)]
        public string TransferMovement { get; set; }            // átigazolás típusa
    }
}
