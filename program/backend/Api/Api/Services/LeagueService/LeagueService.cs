﻿using Api.Data;
using Api.Dtos.LeagueDto;
using Api.Dtos.LeagueDto.GoalscorerlistDto;
using Api.Repository.LeagueRepository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models;
using Api.Dtos.LeagueDto.CleansheetDto;
using Api.Dtos.LeagueDto.CardDto;
using Api.Dtos.LeagueDto.LastResultDto;
using Api.Dtos.LeagueDto.StandingDto;
using Api.Dtos.SimulationDto.RoundDto;
using Api.Dtos.SimulationDto.HeadlineDto;

namespace Api.Services.LeagueService
{
    public class LeagueService : ILeagueService
    {
        private readonly SportContext _context;

        public LeagueService(SportContext context)
        {
            _context = context;
        }

        public async Task<SimulationPageHeadlineDto> SimulationPageHeadLineRespone(CreateRoundSimulationRequest createRoundSimulationRequest)
        {
            return await (from r in _context.Round
                          from s in _context.Season
                          from l in _context.League
                          where r.RoundId == createRoundSimulationRequest.RoundId
                          && s.SeasonId == r.SeasonId
                          && l.LeagueId == s.LeagueId
                          select new SimulationPageHeadlineDto
                          {
                              Title = $"{s.Value} {l.Name} {r.GameWeek}. forduló"
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<GoalscorerDto>> GoalscorerList(GetGoalscorerListById getGoalscorerListById)
        {
            var last_round = _context.Round.FirstOrDefault(x => x.RoundId == getGoalscorerListById.RoundId);

            var rounds = _context.Round.Where(x => x.GameWeek <= last_round.GameWeek).ToList();

            var match_list = new List<Match>();

            foreach (var round in rounds)
            {
                match_list.AddRange(_context.Match.Where(x => x.RoundId == round.RoundId).ToList());
            }

            var shot_list = new List<Shot>();

            foreach (var match in match_list)
            {
                var match_goalscorers = _context.Shot.Where(x => x.Goal == true && x.MatchId == match.MatchId).ToList();
                shot_list.AddRange(match_goalscorers);
            }

            var players = shot_list.GroupBy(x => x.ShotPlayerId).ToList();

            List<GoalscorerDto> goalscorers = new List<GoalscorerDto>();

            foreach (var player in players)
            {
                goalscorers.Add(new GoalscorerDto { 
                    PlayerId = player.Key,
                    Goal = _context.Shot.Where(x =>x.ShotPlayerId == player.Key && x.Goal == true).Count(),
                    PlayerName = _context.Player.Where(x => x.PlayerId == player.Key).Select(x => x.NickName).FirstOrDefault(),
                    Position = 0
                });
            }

            if (shot_list.Count == 0)        // ha még nem szereztek gólt listázzon ki 5 db random playert
            {
                goalscorers = await (from p in _context.Player
                                     select new GoalscorerDto
                                     {
                                         PlayerId = p.PlayerId,
                                         PlayerName = p.Name,
                                         Goal = 0,
                                         Position = 0,
                                     }).Take(5).ToListAsync();
            }

            goalscorers = goalscorers.OrderByDescending(x => x.Goal).ToList();

            for (int i = 0; i < goalscorers.Count; i++)
            {
                goalscorers[i].Position = i + 1;
            }

            return goalscorers;
        }

        public async Task<List<RedcardDto>> RedCard(GetCardById getCardById)
        {
            var last_round = _context.Round.FirstOrDefault(x => x.RoundId == getCardById.RoundId);

            var rounds = _context.Round.Where(x => x.GameWeek <= last_round.GameWeek).ToList();

            var match_list = new List<Match>();

            foreach (var round in rounds)
            {
                match_list.AddRange(_context.Match.Where(x => x.RoundId == round.RoundId).ToList());
            }

            var redcard_list = new List<Fault>();

            foreach (var match in match_list)
            {
                var match_redcards = _context.Fault.Where(x => x.Cardtype == Cardtype.Red && x.MatchId == match.MatchId).ToList();
                redcard_list.AddRange(match_redcards);
            }

            var players = redcard_list.GroupBy(x => x.PlayerId).ToList();

            List<RedcardDto> redcards = new List<RedcardDto>();

            foreach (var player in players)
            {
                redcards.Add(new RedcardDto
                {
                    PlayerId = player.Key,
                    RedCard = _context.Fault.Where(x => x.PlayerId == player.Key && x.Cardtype == Cardtype.Red).Count(),
                    PlayerName = _context.Player.Where(x => x.PlayerId == player.Key).Select(x => x.NickName).FirstOrDefault(),
                    Position = 0
                });
            }

            if (redcard_list.Count == 0)        // ha még nem szereztek gólt listázzon ki 5 db random playert
            {
                redcards = await (from p in _context.Player
                                     select new RedcardDto
                                     {
                                         PlayerId = p.PlayerId,
                                         PlayerName = p.Name,
                                         RedCard = 0,
                                         Position = 0,
                                     }).Take(5).ToListAsync();
            }

            redcards = redcards.OrderByDescending(x => x.RedCard).ToList();

            for (int i = 0; i < redcards.Count; i++)
            {
                redcards[i].Position = i + 1;
            }

            return redcards;
        }

        public async Task<List<YellowcardDto>> YellowCard(GetCardById getCardById)
        {
            var last_round = _context.Round.FirstOrDefault(x => x.RoundId == getCardById.RoundId);

            var rounds = _context.Round.Where(x => x.GameWeek <= last_round.GameWeek).ToList();

            var match_list = new List<Match>();

            foreach (var round in rounds)
            {
                match_list.AddRange(_context.Match.Where(x => x.RoundId == round.RoundId).ToList());
            }

            var yellowcard_list = new List<Fault>();

            foreach (var match in match_list)
            {
                var match_yellowcards = _context.Fault.Where(x => x.Cardtype == Cardtype.Yellow && x.MatchId == match.MatchId).ToList();
                yellowcard_list.AddRange(match_yellowcards);
            }

            var players = yellowcard_list.GroupBy(x => x.PlayerId).ToList();

            List<YellowcardDto> yellowcards = new List<YellowcardDto>();

            foreach (var player in players)
            {
                yellowcards.Add(new YellowcardDto
                {
                    PlayerId = player.Key,
                    YellowCard = _context.Fault.Where(x => x.PlayerId == player.Key && x.Cardtype == Cardtype.Yellow).Count(),
                    PlayerName = _context.Player.Where(x => x.PlayerId == player.Key).Select(x => x.NickName).FirstOrDefault(),
                    Position = 0
                });
            }

            if (yellowcard_list.Count == 0)        // ha még nem szereztek gólt listázzon ki 5 db random playert
            {
                yellowcards = await (from p in _context.Player
                                  select new YellowcardDto
                                  {
                                      PlayerId = p.PlayerId,
                                      PlayerName = p.Name,
                                      YellowCard = 0,
                                      Position = 0,
                                  }).Take(5).ToListAsync();
            }

            yellowcards = yellowcards.OrderByDescending(x => x.YellowCard).ToList();

            for (int i = 0; i < yellowcards.Count; i++)
            {
                yellowcards[i].Position = i + 1;
            }

            return yellowcards;
        }

        public async Task<List<LastResultDto>> Result(GetLastResultsByIdRequest getLastResultsByIdRequest)
        {
            return await (from x in _context.Match
                          from mf in _context.MatchFact
                          where x.Simulated == true
                          && x.MatchId == mf.MatchId
                          && x.RoundId == getLastResultsByIdRequest.RoundId
                          select new LastResultDto
                          {
                              MatchId = x.MatchId,
                              AwayTeam = _context.Team.Where(y => y.TeamId == x.AwayTeamId).Select(y => y.Name).FirstOrDefault(),
                              HomeTeam = _context.Team.Where(y => y.TeamId == x.HomeTeamId).Select(y => y.Name).FirstOrDefault(),
                              Fulltime = $"{mf.HomeGoalCount} : {mf.AwayGoalCount}"
                          }).ToListAsync();
        }

        // Unit Test example

        public async Task<List<League>> GetLeagues()
        {
            return await _context.League.ToListAsync();
        }

        public async Task<League> GetLeagueById(GetLeagueByIdRequest getLeagueByIdRequest)
        {
            return await _context.League.FindAsync(getLeagueByIdRequest.Id);
        }

        public async Task<List<TeamStandingDto>> Standing(GetLeagueStandingsRequest getLeagueStandingsRequest)
        {
            var league_teamId_list = await _context.Team
                                    .Where(x => x.LeagueId == getLeagueStandingsRequest.LeagueId)
                                    .Select(x => x.TeamId)
                                    .ToListAsync();

            var last_round_gameweek = await _context.Round.Where(x => x.RoundId == getLeagueStandingsRequest.RoundId)
                                            .Select(x => x.GameWeek)
                                            .FirstOrDefaultAsync();

            var standing_rounds_roundId = await _context.Round
                                            .Where(x => x.GameWeek <= last_round_gameweek)
                                            .Select(x => x.RoundId)
                                            .ToListAsync();

            var match_list = new List<Match>();
            var match_fact_list = new List<MatchFact>();

            foreach (var roundId in standing_rounds_roundId)
            {
                match_list.AddRange(_context.Match
                            .Where(x => x.RoundId == roundId)
                            .ToList());
            }

            foreach (var match in match_list)
            {
                match_fact_list
                    .Add(_context.MatchFact
                    .Where(x => x.MatchId == match.MatchId)
                    .FirstOrDefault());
            }

            List<TeamStandingDto> team_standings = new List<TeamStandingDto>();

            for (int i = 0; i < league_teamId_list.Count; i++)
            {
                team_standings.Add(new TeamStandingDto
                {
                    TeamId = league_teamId_list[i],
                    Logo = $"assets/logo/{_context.Team.Where(x => x.TeamId == league_teamId_list[i]).Select(x => x.Logo).FirstOrDefault()}",
                    Win = 0,
                    Defeat = 0,
                    Draw = 0,
                    PlayedGames = 0,
                    Point = 0,
                    Position = 0,
                    TeamName = _context.Team.Where(x => x.TeamId == league_teamId_list[i]).Select(x => x.Name).FirstOrDefault()
                });
            }


            foreach (var team in team_standings)
            {
                foreach (var match in match_list)
                {
                    foreach (var matchfact in match_fact_list)
                    {
                        if (match.MatchId == matchfact.MatchFactId)
                        {
                            if (match.HomeTeamId == team.TeamId)
                            {
                                team.PlayedGames++;

                                if (matchfact.HomeGoalCount > matchfact.AwayGoalCount)
                                {
                                    team.Win++;
                                }
                                else if (matchfact.HomeGoalCount == matchfact.AwayGoalCount)
                                {
                                    team.Draw++;
                                }
                                else
                                {
                                    team.Defeat++;
                                }
                            }
                            else if (match.AwayTeamId == team.TeamId)
                            {
                                team.PlayedGames++;

                                if (matchfact.HomeGoalCount < matchfact.AwayGoalCount)
                                {
                                    team.Win++;
                                }
                                else if (matchfact.HomeGoalCount == matchfact.AwayGoalCount)
                                {
                                    team.Draw++;
                                }
                                else
                                {
                                    team.Defeat++;
                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < team_standings.Count; i++)
            {
                team_standings[i].Point = (3 * team_standings[i].Win) + team_standings[i].Draw;
            }

            team_standings = team_standings.OrderByDescending(x => x.Point).ToList();

            for (int i = 0; i < team_standings.Count; i++)
            {
                team_standings[i].Position = i + 1;
            }


            return team_standings;
        }

    }
}
