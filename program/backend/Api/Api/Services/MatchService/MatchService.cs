﻿using Api.Data;
using Api.Dtos.MatchDto.Comment;
using Api.Dtos.MatchDto.LineUp;
using Api.Dtos.MatchDto.MatchReport;
using Api.Dtos.MatchDto.MatchStatistics;
using Api.Dtos.PlayerDto.PositionDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using Api.Models;
using Api.Repository.MatchRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services.MatchService
{
    public class MatchService : IMatchService
    {
        private readonly SportContext _context;
        private readonly IMemoryCache _cache;

        public MatchService(SportContext context, IMemoryCache cache)
        {
            _context = context;
            _cache = cache;
        }

        public List<PositionDto> GenerateLineup(List<PositionDto> positions, TmpFormation tmpFormation)
        {
            // kapus

            positions.Add(new PositionDto
            {
                Type = tmpFormation.Team,
                X = 30,
                Y = 255
            });

            // védő

            for (int i = 0; i < tmpFormation.Defender; i++)
            {
                if (tmpFormation.Defender == 3)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 150,
                        Y = 150 + i * 100
                    });
                }
                else if (tmpFormation.Defender == 4)
                {
                    if (i == 0 || i == 3)
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 200,
                            Y = 100 + i * 100
                        });
                    }
                    else
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 150,
                            Y = 100 + i * 100
                        });
                    }
                }
                else if (tmpFormation.Defender == 5)
                {
                    if (i == 0 || i == 4)
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 200,
                            Y = 100 + i * 75
                        });
                    }
                    else
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 150,
                            Y = 100 + i * 75
                        });
                    }
                }
            }

            // középpálya

            for (int i = 0; i < tmpFormation.Midfielder; i++)
            {
                if (tmpFormation.Midfielder == 1)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 235,
                        Y = 255
                    });
                }
                else if (tmpFormation.Midfielder == 2)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 215,
                        Y = 200 + i * 100
                    });
                }
                else if (tmpFormation.Midfielder == 3)
                {
                    if (i % 2 == 1)
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 255,
                            Y = 150 + i * 100
                        });
                    }
                    else
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 275,
                            Y = 150 + i * 100
                        });
                    }
                }
                else if (tmpFormation.Midfielder == 4)
                {
                    if (i == 0 || i == 3)
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 285,
                            Y = 100 + i * 100
                        });
                    }
                    else
                    {
                        positions.Add(new PositionDto
                        {
                            Type = tmpFormation.Team,
                            X = 255,
                            Y = 100 + i * 100
                        });
                    }
                }
            }

            // támadó

            for (int i = 0; i < tmpFormation.Attacker; i++)
            {
                if (tmpFormation.Attacker == 2)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 300,
                        Y = 200 + i * 100
                    });
                }
                else if (tmpFormation.Attacker == 3)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 300,
                        Y = 150 + i * 100
                    });
                }
                else if (tmpFormation.Attacker == 4)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 275,
                        Y = 100 + i * 100
                    });
                }
            }

            // striker

            for (int i = 0; i < tmpFormation.Striker; i++)
            {
                if (tmpFormation.Striker == 1)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 350,
                        Y = 255
                    });
                }
                else if (tmpFormation.Striker == 2)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 350,
                        Y = 200 + i * 100
                    });
                }
                else if (tmpFormation.Striker == 3)
                {
                    positions.Add(new PositionDto
                    {
                        Type = tmpFormation.Team,
                        X = 350,
                        Y = 150 + i * 100
                    });
                }
            }

            return positions;
        }

        public async Task<List<PositionDto>> PlayerPosition(GetPlayerPositionsById getPlayerPositionsById)
        {
            var match = await _context.Match.FirstOrDefaultAsync(x => x.MatchId == getPlayerPositionsById.MatchId);

            List<PositionDto> positions = new List<PositionDto>();
            List<PositionDto> tmpPositions = new List<PositionDto>();
            TmpFormation tmpFormation = new TmpFormation();

            tmpFormation = GetFormationInfo(match.HomeTeamId, "home");
            positions = GenerateLineup(positions, tmpFormation);
            tmpFormation = GetFormationInfo(match.AwayTeamId, "away");
            positions = GenerateLineup(positions, tmpFormation);

            // tükrözés

            for (int i = 0; i < positions.Count; i++)
            {
                if (positions[i].Type == "away")
                {
                    positions[i].X = 800 - positions[i].X;
                    positions[i].Y = 500 - positions[i].Y;
                }
            }

            return positions;
        }

        public TmpFormation GetFormationInfo(int teamId, string team)
        {
            TmpFormation tmpFormation = new TmpFormation();

            string formation = _context.Team
                               .Where(x => x.TeamId == teamId)
                               .Select(x => x.Formation)
                               .FirstOrDefault();

            string[] charArray = formation.Split("-");

            tmpFormation.Team = team;
            tmpFormation.Defender = Convert.ToInt32(charArray[0]);
            tmpFormation.Midfielder = Convert.ToInt32(charArray[1]);

            if (charArray.Length == 3)
            {
                tmpFormation.Attacker = 0;
                tmpFormation.Striker = Convert.ToInt32(charArray[2]);
            }
            else
            {
                tmpFormation.Attacker = Convert.ToInt32(charArray[2]);
                tmpFormation.Striker = Convert.ToInt32(charArray[3]);
            }

            return tmpFormation;
        }

        public async Task<SimulationHeadLineDto> SimulationHeadLine(GetMatchHeadLineRequest getMatchHeadLineRequest)
        {
            return await (from m in _context.Match
                          where m.MatchId == getMatchHeadLineRequest.MatchId
                          select new SimulationHeadLineDto
                          {
                              MatchId = m.MatchId,
                              Home = _context.Team.Where(x => x.TeamId == m.HomeTeamId).FirstOrDefault().Name,
                              Away = _context.Team.Where(x => x.TeamId == m.AwayTeamId).FirstOrDefault().Name,
                              HomeLogo = $"assets/logo/{_context.Team.Where(x => x.TeamId == m.HomeTeamId).FirstOrDefault().Logo}",
                              AwayLogo = $"assets/logo/{_context.Team.Where(x => x.TeamId == m.AwayTeamId).FirstOrDefault().Logo}",
                          }).FirstOrDefaultAsync();
        }

        public async Task<MatchResultDto> MatchResultResponse(GetMatchResultRequest getMatchResultRequest)
        {
            var match = await _context.Match.Where(x => x.MatchId == getMatchResultRequest.MatchId).FirstOrDefaultAsync();
            var goals = await _context.Shot.Where(x => x.MatchId == match.MatchId && x.Minute <= getMatchResultRequest.Minute && x.Goal == true).ToListAsync();

            int homeGoals = goals.Where(x => x.TeamId == match.HomeTeamId).ToList().Count();
            int awayGoals = goals.Where(x => x.TeamId == match.AwayTeamId).ToList().Count();

            return new MatchResultDto
            {
                Result = $"{homeGoals} : {awayGoals}"
            };
        }

        public async Task<List<LineupDto>> Lineup(GetLineupByMatchIdRequest getLineupRequest)
        {
            var playerLineUp = new List<LineupDto>();

            // hazai csapat kezdő 11

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId && mp.PlayerStatus == PlayerStatus.Home11
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.Name,
                                             Type = "home11"
                                         }).ToListAsync());

            // hazai csapat lecserélt játékosok

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId && mp.PlayerStatus == PlayerStatus.Home11Sub
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.NickName,
                                             Type = "home11"
                                         }).ToListAsync());

            // vendég csapat kezdő 11

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId && mp.PlayerStatus == PlayerStatus.Away11
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.NickName,
                                             Type = "away11"
                                         }).ToListAsync());

            // vendég csapat lecserélt játékosok

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.NickName,
                                             Type = "away11"
                                         }).ToListAsync());

            // hazai csapat csere játékosok

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId && mp.PlayerStatus == PlayerStatus.HomeSub
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.NickName,
                                             Type = "homesub"
                                         }).ToListAsync());

            // hazai csapat kispados játékosok

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId && mp.PlayerStatus == PlayerStatus.HomeBench
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.NickName,
                                             Type = "homesub"
                                         }).ToListAsync());

            // vendég csapat csere játékosok

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId && mp.PlayerStatus == PlayerStatus.AwaySub
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.NickName,
                                             Type = "awaysub"
                                         }).ToListAsync());

            // vendég csapat kispados játékosok

            playerLineUp.AddRange(await (from p in _context.Player
                                         from mp in _context.MatchPlayer
                                         where p.PlayerId == mp.PlayerId && mp.MatchId == getLineupRequest.MatchId && mp.PlayerStatus == PlayerStatus.AwayBench
                                         select new LineupDto
                                         {
                                             PlayerId = p.PlayerId,
                                             Name = p.NickName,
                                             Type = "awaysub"
                                         }).ToListAsync());
            return playerLineUp;
        }

        public List<CommentDto> GetNormalComments(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest)
        {
            return (from l in _context.Comment
                    where l.MatchId == getCommentsByMatchIdRequest.MatchId
                    && l.Minute > getCommentsByMatchIdRequest.Start
                    && l.Minute <= getCommentsByMatchIdRequest.End
                    select new CommentDto
                    {
                        Description = l.Description,
                        Minute = l.Minute,
                    }).ToList();
        }

        public async Task<List<CommentDto>> GetComments(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest)
        {
            _cache.Set("comments", await (from l in _context.Comment
                                          where l.MatchId == getCommentsByMatchIdRequest.MatchId
                                          && l.Minute > getCommentsByMatchIdRequest.Start
                                          && l.Minute <= getCommentsByMatchIdRequest.End
                                          select new CommentDto
                                          {
                                              Description = l.Description,
                                              Minute = l.Minute,
                                          }).ToListAsync());
            return _cache.Get("comments") as List<CommentDto>;
        }

        public async Task<List<CommentDto>> GetCachedComments(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest)
        {
            List<CommentDto> commentResponse = new List<CommentDto>();

            commentResponse = _cache.Get("comments") as List<CommentDto>;

            if (commentResponse == null)
            {
                var begin = new List<CommentDto>();

                begin.AddRange(new List<CommentDto> {
                new CommentDto {
                    Minute = 0,
                    Description = "Üdvözöljük Önöket, tisztelt hölgyeim és uraim, kezdődik élő szöveges közvetítésünk, üdvözöljük Önöket. A csapatok összeállításai hamarosan elérhetőek lesznek. Bárkinek szurkoljanak is, mi segítünk Önöknek, hogy élvezhessék és végigizgulhassák a meccs minden pillanatát. Jó szórakozást kívánunk!",

                },
                new CommentDto {
                    Minute = 0,
                    Description = "A meccs hamarosan kezdődik, az összeállítások már elérhetőek.",
                },
                new CommentDto {
                    Minute = 0,
                    Description = "Elkezdődött a meccs, jó szórakozást kívánunk hozzá!"
                }});

                _cache.Remove("comments");
                _cache.Set("comments", begin);

                return begin.OrderByDescending(x => x.Minute).ToList();
            }

            commentResponse.AddRange(await GetComments(getCommentsByMatchIdRequest));
            _cache.Remove("comments");
            _cache.Set("comments", commentResponse);
            return commentResponse.OrderByDescending(x => x.Minute).ToList();
        }

        public List<MatchStatisticsDto> GetMatchStatistics(GetMatchStatisticsByIdRequest getMatchStatisticsByIdRequest)
        {
            var match = _context.Match.Where(x => x.MatchId == getMatchStatisticsByIdRequest.MatchId).FirstOrDefault();
            var possesion = _context.Possesion.Where(x => x.MatchId == getMatchStatisticsByIdRequest.MatchId && x.Minute <= getMatchStatisticsByIdRequest.Minute).ToList();
            var shots = _context.Shot.Where(x => x.MatchId == getMatchStatisticsByIdRequest.MatchId && x.Minute <= getMatchStatisticsByIdRequest.Minute).ToList();
            var faults = _context.Fault.Where(x => x.MatchId == getMatchStatisticsByIdRequest.MatchId && x.Minute <= getMatchStatisticsByIdRequest.Minute).ToList();

            return new List<MatchStatisticsDto> {
            new MatchStatisticsDto {
                HomeTeam =  (int) possesion.Select(x => x.HomePossesion).Average(),
                AwayTeam = (int) possesion.Select(x => x.AwayPossesion).Average(),
                Type = "Labdabirtoklás"
            },
            new MatchStatisticsDto {
                HomeTeam =  (int) possesion.Select(x => x.HomeTeamPass).Sum(),
                AwayTeam = (int) possesion.Select(x => x.AwayTeamPass).Sum(),
                Type = "Összes passz"
            },
            new MatchStatisticsDto {
                HomeTeam =  shots.Where(x => x.TeamId == match.HomeTeamId).Count(),
                AwayTeam = shots.Where(x => x.TeamId == match.AwayTeamId).Count(),
                Type = "Kapura lövés"
            },
            new MatchStatisticsDto {
                HomeTeam =  shots.Where(x => x.TeamId == match.HomeTeamId && x.Event != Event.BlockedShot).Count(),
                AwayTeam = shots.Where(x => x.TeamId == match.AwayTeamId && x.Event != Event.BlockedShot).Count(),
                Type = "Kaput eltaláló lövés"
            },
            new MatchStatisticsDto {
                HomeTeam =  shots.Where(x => x.TeamId == match.HomeTeamId && x.Event == Event.Goal).Count(),
                AwayTeam = shots.Where(x => x.TeamId == match.AwayTeamId && x.Event == Event.Goal).Count(),
                Type = "Gól"
            },
            new MatchStatisticsDto {
                HomeTeam =  shots.Where(x => x.TeamId == match.HomeTeamId && x.Event == Event.BlockedShot).Count(),
                AwayTeam = shots.Where(x => x.TeamId == match.AwayTeamId && x.Event == Event.BlockedShot).Count(),
                Type = "Blokkolt lövés"
            },
            new MatchStatisticsDto {
                HomeTeam =  shots.Where(x => x.TeamId == match.HomeTeamId && x.Event == Event.Freekick).Count(),
                AwayTeam = shots.Where(x => x.TeamId == match.AwayTeamId && x.Event == Event.Freekick).Count(),
                Type = "Szabadrugás"     
            },
           new MatchStatisticsDto {
                HomeTeam =  shots.Where(x => x.TeamId == match.HomeTeamId && x.Event == Event.Corner).Count(),
                AwayTeam = shots.Where(x => x.TeamId == match.AwayTeamId && x.Event == Event.Corner).Count(),
                Type = "Szöglet"
            },
            new MatchStatisticsDto {
                HomeTeam =  shots.Where(x => x.TeamId == match.HomeTeamId && x.Event == Event.SavedShot).Count(),
                AwayTeam = shots.Where(x => x.TeamId == match.AwayTeamId && x.Event == Event.SavedShot).Count(),
                Type = "Védés"
            },
            new MatchStatisticsDto {
                HomeTeam = faults.Where(x => x.TeamId == match.HomeTeamId).Count(),
                AwayTeam = faults.Where(x => x.TeamId == match.AwayTeamId).Count(),
                Type = "Szabálytalanság"
            },
            new MatchStatisticsDto {
                HomeTeam = faults.Where(x => x.TeamId == match.HomeTeamId && x.Cardtype == Cardtype.Yellow).Count(),
                AwayTeam = faults.Where(x => x.TeamId == match.HomeTeamId && x.Cardtype == Cardtype.Yellow).Count(),
                Type = "Sárga lap"
            },
            new MatchStatisticsDto {
                HomeTeam = faults.Where(x => x.TeamId == match.HomeTeamId && x.Cardtype == Cardtype.Red).Count(),
                AwayTeam = faults.Where(x => x.TeamId == match.HomeTeamId && x.Cardtype == Cardtype.Red).Count(),
                Type = "Piros lap"
            }};
        }

        public List<MatchReportDto> MatchReport(GetMatchReportByIdRequest getMatchReportByIdRequest)
        {
            var match = _context.Match.Where(x => x.MatchId == getMatchReportByIdRequest.MatchId).FirstOrDefault();
            var subs = _context.MatchPlayer.Where(x => x.MatchId == getMatchReportByIdRequest.MatchId && x.Substitute == true && x.StartingTeamPlayer == false).ToList();
            var goals = _context.Shot.Where(x => x.MatchId == getMatchReportByIdRequest.MatchId && x.Minute <= getMatchReportByIdRequest.Minute && x.Goal == true).ToList();
            var yellow_cards = _context.Fault.Where(x => x.MatchId == getMatchReportByIdRequest.MatchId && x.Minute <= getMatchReportByIdRequest.Minute && x.Cardtype == Cardtype.Yellow).ToList();
            var red_cards = _context.Fault.Where(x => x.MatchId == getMatchReportByIdRequest.MatchId && x.Minute <= getMatchReportByIdRequest.Minute && x.Cardtype == Cardtype.Red).ToList();

            var report = new List<MatchReportDto>();

            // sárgalap

            if (yellow_cards != null)
            {
                foreach (var yellow in yellow_cards)
                {
                    var player = _context.Player.Where(x => x.PlayerId == yellow.PlayerId).Select(x => x.NickName).FirstOrDefault();

                    if (yellow.TeamId == match.HomeTeamId)
                    {
                        if (yellow.Minute <= 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = yellow.PlayerId,
                                Description = $"{yellow.Minute} {player} (sárgalap)",
                                Type = "home1",
                                Minute = yellow.Minute
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = yellow.PlayerId,
                                Description = $"{yellow.Minute} {player} (sárgalap)",
                                Type = "home2",
                                Minute = yellow.Minute
                            });
                        }
                    }
                    else if (yellow.TeamId == match.AwayTeamId)
                    {
                        if (yellow.Minute <= 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = yellow.PlayerId,
                                Description = $"{yellow.Minute} {player} (sárgalap)",
                                Type = "away1",
                                Minute = yellow.Minute
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = yellow.PlayerId,
                                Description = $"{yellow.Minute} {player} (sárgalap)",
                                Type = "away2",
                                Minute = yellow.Minute
                            });
                        }
                    }
                }
            }

            // piroslap

            if (red_cards != null)
            {
                foreach (var red in red_cards)
                {
                    var player = _context.Player.Where(x => x.PlayerId == red.PlayerId).Select(x => x.NickName).FirstOrDefault();

                    if (red.TeamId == match.HomeTeamId)
                    {
                        if (red.Minute <= 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = red.PlayerId,
                                Description = $"{red.Minute} {player} (piroslap)",
                                Type = "home1",
                                Minute = red.Minute
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = red.PlayerId,
                                Description = $"{red.Minute} {player} (piroslap)",
                                Type = "home2",
                                Minute = red.Minute
                            });
                        }
                    }
                    else if (red.TeamId == match.AwayTeamId)
                    {
                        if (red.Minute <= 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = red.PlayerId,
                                Description = $"{red.Minute} {player} (piroslap)",
                                Type = "away1",
                                Minute = red.Minute
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = red.PlayerId,
                                Description = $"{red.Minute} {player} (piroslap)",
                                Type = "away2",
                                Minute = red.Minute
                            });
                        }
                    }
                }
            }

            // csere

            foreach (var sub in subs)
            {
                if ((90 - sub.PlayedMinutes) <= getMatchReportByIdRequest.Minute)
                {

                    var sub_in = _context.Player.Where(x => x.PlayerId == sub.PlayerId).Select(x => x.NickName).FirstOrDefault();
                    var sub_off = _context.Player.Where(x => x.PlayerId == sub.SubPlayerId).Select(x => x.NickName).FirstOrDefault();

                    if (sub.TeamId == match.HomeTeamId)
                    {
                        if ((90 - sub.PlayedMinutes) < 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = sub.PlayerId,
                                Description = $"{(90 - sub.PlayedMinutes)} Csere ({sub_in}) fel ({sub_off}) le",
                                Type = "home1",
                                Minute = (90 - sub.PlayedMinutes)
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = sub.PlayerId,
                                Description = $"{(90 - sub.PlayedMinutes)} Csere ({sub_in}) fel ({sub_off}) le",
                                Type = "home2",
                                Minute = (90 - sub.PlayedMinutes)
                            });
                        }
                    }
                    else if (sub.TeamId == match.AwayTeamId)
                    {
                        if ((90 - sub.PlayedMinutes) < 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = sub.PlayerId,
                                Description = $"{(90 - sub.PlayedMinutes)} Csere ({sub_in}) fel ({sub_off}) le",
                                Type = "away1",
                                Minute = (90 - sub.PlayedMinutes)
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = sub.PlayerId,
                                Description = $"{(90 - sub.PlayedMinutes)} Csere ({sub_in}) fel ({sub_off}) le",
                                Type = "away2",
                                Minute = (90 - sub.PlayedMinutes)
                            });
                        }
                    }

                }
            }

            // gól

            if (goals != null)
            {
                foreach (var goal in goals)
                {
                    var goalscorer = _context.Player.Where(x => x.PlayerId == goal.ShotPlayerId).Select(x => x.NickName).FirstOrDefault();

                    if (goal.TeamId == match.HomeTeamId)
                    {
                        if (goal.Minute <= 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = goal.ShotPlayerId,
                                Description = $"{goal.Minute} {goalscorer} (gól)",
                                Type = "home1",
                                Minute = goal.Minute
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = goal.ShotPlayerId,
                                Description = $"{goal.Minute} {goalscorer} (gól)",
                                Type = "home2",
                                Minute = goal.Minute
                            });
                        }
                    }
                    else if (goal.TeamId == match.AwayTeamId)
                    {
                        if (goal.Minute <= 45)
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = goal.ShotPlayerId,
                                Description = $"{goal.Minute} {goalscorer} (gól)",
                                Type = "away1",
                                Minute = goal.Minute
                            });
                        }
                        else
                        {
                            report.Add(new MatchReportDto
                            {
                                PlayerId = goal.ShotPlayerId,
                                Description = $"{goal.Minute} {goalscorer} (gól)",
                                Type = "away2",
                                Minute = goal.Minute
                            });
                        }
                    }
                }
            }

            return report.OrderBy(x => x.Minute).ToList();
        }

    }
}