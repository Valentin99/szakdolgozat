﻿using Api.Data;
using Api.Dtos.MatchDto.MatchList;
using Api.Dtos.TeamDto.HeadLineDto;
using Api.Dtos.TeamDto.PlayerDto;
using Api.Dtos.TeamDto.TransferDto;
using Api.Repository.TeamRepository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Models;
using Api.Dtos.TeamDto.SettingsDto;
using System;

namespace Api.Services.TeamService
{
    public class TeamService : ITeamService
    {

        private readonly SportContext _context;

        public TeamService(SportContext context)
        {
            _context = context;
        }

        public List<TeamDto> GetTeams()
        {
            return (from t in _context.Team
                    select new TeamDto
                    {
                        TeamId = t.TeamId,
                        Name = t.Name,
                        NickName = t.NickName,
                        Logo = $"assets/logo/{t.Logo}",
                        Stadium = t.Stadium,
                        Formation = t.Formation
                    }).ToList();
        }

        public void CreateTeam(TeamDto teamDto)
        {
            var team = new Team()
            {
                Name = teamDto.Name,
                NickName = teamDto.NickName,
                Formation = teamDto.Formation,
                LeagueId = 1,
                Logo = "Unkown.png",
                SeasonId = 1,
                Stadium = teamDto.Stadium,
            };

            _context.Add(team);
            _context.SaveChanges();
        }

        public string UpdateTeam(TeamDto teamDto, int id)
        {
            var team = new Team()
            {
                TeamId = id,
                Name = teamDto.Name,
                NickName = teamDto.NickName,
                Formation = teamDto.Formation,
                LeagueId = 1,
                Logo = teamDto.Logo,
                SeasonId = 1,
                Stadium = teamDto.Stadium,
            };

            _context.Update(team);
            _context.SaveChanges();
            return "Sikeres módosítás";
        }

        public void DeleteTeam(int id)
        {
            var team = _context.Team.FirstOrDefault(x => x.TeamId == id);
            _context.Remove(team);
            _context.SaveChanges();
        }

        public async Task<List<MatchDto>> GetMatchResults(GetMatchResultsByTeamIdRequest getMatchResultsByTeamIdRequest)
        {
            var match_list = await _context.Match.Where(x => x.Simulated == true && x.HomeTeamId == getMatchResultsByTeamIdRequest.TeamId || x.AwayTeamId == getMatchResultsByTeamIdRequest.TeamId).ToListAsync();

            var matchdto = new List<MatchDto>();

            foreach (var match in match_list)
            {
                matchdto.Add(new MatchDto
                {
                    MatchId = match.MatchId,
                    HomeTeam = _context.Team.FirstOrDefault(x => x.TeamId == match.HomeTeamId).Name,
                    AwayTeam = _context.Team.FirstOrDefault(x => x.TeamId == match.AwayTeamId).Name,
                    HalfTimeResult = _context.MatchFact.FirstOrDefault(x => x.MatchId == match.MatchId).HalfTime,
                    FulTimeResult = _context.MatchFact.FirstOrDefault(x => x.MatchId == match.MatchId).FullTime
                });
            }

            return matchdto;
        }

        public async Task<List<TeamPlayerDto>> GetTeamPlayersByTeamId(GetTeamPlayersByIdRequest getTeamPlayersByIdRequest)
        {
            var players = await _context.Player.Where(x => x.TeamId == getTeamPlayersByIdRequest.TeamId).ToListAsync();

            List<TeamPlayerDto> teamPlayers = new List<TeamPlayerDto>();

            foreach (var player in players)
            {
                teamPlayers.Add(new TeamPlayerDto
                {
                    TeamId = player.TeamId,
                    PlayerId = player.PlayerId,
                    Name = player.NickName,
                    Position = player.TeamPosition,
                    YerseyNumber = player.TeamYerseyNumber,
                    Age = player.Age,
                    Apps = _context.MatchPlayer.Where(x => x.PlayerId == player.PlayerId).Count(),
                    Goal = _context.Shot.Where(x => x.ShotPlayerId == player.PlayerId && x.Goal == true).Count(),
                    RedCard = _context.Fault.Where(x => x.PlayerId == player.PlayerId && x.Cardtype == Cardtype.Red).Count(),
                    YellowCard = _context.Fault.Where(x => x.PlayerId == player.PlayerId && x.Cardtype == Cardtype.Yellow).Count(),
                });
            }

            return teamPlayers.OrderBy(x => x.YerseyNumber).ToList();
        }

        public async Task<TeamHeadlineDto> TeamHeadline(GetHeadlineById getHeadlineById)
        {
            return await (from t in _context.Team
                          where t.TeamId == getHeadlineById.TeamId
                          select new TeamHeadlineDto
                          {
                              Title = t.Name,
                              Logo = $"assets/logo/{t.Logo}"
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<TeamTransferDto>> TeamTransfers(GetTeamTransfersById getTeamTransfersById)
        {
            var transfers = await _context.Transfer.Where(x => x.TeamId == getTeamTransfersById.TeamId).ToListAsync();

            List<TeamTransferDto> teamTransfers = new List<TeamTransferDto>();

            foreach (var transfer in transfers)
            {

                if (transfer.Fee == "Loan")
                {
                    if (transfer.TransferMovement == "in")
                    {
                        teamTransfers.Add(new TeamTransferDto
                        {
                            PlayerId = transfer.PlayerId,
                            Name = transfer.PlayerName,
                            Movement = "Kölcsön",
                            From = transfer.TeamName,
                            To = transfer.InvolvedTeamName
                        });
                    }
                    else if (transfer.TransferMovement == "out")
                    {
                        teamTransfers.Add(new TeamTransferDto
                        {
                            PlayerId = transfer.PlayerId,
                            Name = transfer.PlayerName,
                            Movement = "Kölcsönből vissza",
                            From = transfer.TeamName,
                            To = transfer.InvolvedTeamName
                        });
                    }
                }
                else if (transfer.Fee == "Free Transfer")
                {
                    teamTransfers.Add(new TeamTransferDto
                    {
                        PlayerId = transfer.PlayerId,
                        Name = transfer.PlayerName,
                        Movement = "Ingyenes átigazolás",
                        From = transfer.TeamName,
                        To = transfer.InvolvedTeamName
                    });
                }
                else
                {
                    teamTransfers.Add(new TeamTransferDto
                    {
                        PlayerId = transfer.PlayerId,
                        Name = transfer.PlayerName,
                        Movement = "Átigazolás",
                        From = transfer.TeamName,
                        To = transfer.InvolvedTeamName
                    });
                }
            }

            return teamTransfers;
        }

    }
}
