﻿using Api.Data;
using Api.Dtos.PlayerDto.CareerDto;
using Api.Dtos.PlayerDto.HeadlineDto;
using Api.Dtos.PlayerDto.MatchDto;
using Api.Dtos.PlayerDto.PositionDto;
using Api.Dtos.PlayerDto.SettingsDto;
using Api.Dtos.TeamDto.SettingsDto;
using Api.Models;
using Api.Repository.PlayerRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services.PlayerService
{
    public class PlayerService : IPlayerService
    {

        private readonly SportContext _context;

        public PlayerService(SportContext context)
        {
            _context = context;
        }

        public List<PlayerDto> GetPlayers()
        {
            return (from p in _context.Player
                    select new PlayerDto
                    {
                        PlayerId = p.PlayerId,
                        TeamId = p.TeamId,
                        Name = p.Name,
                        NickName = p.NickName,
                        Age = p.Age,
                        Birthday = p.Birthday.Substring(0,10),
                        Nationality = p.Nationality,
                        CurrentTeam = p.CurrentTeam,
                        MarketValue = p.MarketValue,
                        TeamPosition = p.TeamPosition,
                        TeamYerseyNumber = p.TeamYerseyNumber
                    }).ToList();
        }

        public void CreatePlayer(PlayerDto playerDto)
        {
            var player = new Player()
            {
                TeamId = playerDto.TeamId,
                Age = playerDto.Age,
                Birthday = playerDto.Birthday,
                CurrentTeam = _context.Team.Where(x => x.TeamId == playerDto.TeamId).Select(x => x.Name).FirstOrDefault(),
                MarketValue = playerDto.MarketValue,
                Name = playerDto.Name,
                Nationality = playerDto.Nationality,
                NickName = playerDto.NickName,
                TeamPosition = playerDto.TeamPosition,
                TeamYerseyNumber = playerDto.TeamYerseyNumber
            };

            _context.Add(player);
            _context.SaveChanges();
        }

        public void DeletePlayer(int id)
        {
            var player = _context.Player.Find(id);
            _context.Remove(player);
            _context.SaveChanges();
        }

        public void UpdatePlayer(PlayerDto playerDto, int id)
        {
            var player = new Player()
            {
                PlayerId = id,
                TeamId = playerDto.TeamId,
                Age = playerDto.Age,
                Birthday = playerDto.Birthday,
                CurrentTeam = playerDto.CurrentTeam,
                MarketValue = playerDto.MarketValue,
                Name = playerDto.Name,
                Nationality = playerDto.Nationality,
                NickName = playerDto.NickName,
                TeamPosition = playerDto.TeamPosition,
                TeamYerseyNumber = playerDto.TeamYerseyNumber
            };

            _context.Update(player);
            _context.SaveChanges();
        }

        public async Task<List<CareerDto>> GetPlayerCareerById(GetPlayerCareerById getPlayerCareerById)
        {
            var player = await _context.Player.FirstOrDefaultAsync(x => x.PlayerId == getPlayerCareerById.PlayerId);
            var team = new Team();
            var season = new Season();
            var league = new League();

            List<CareerDto> careerList = new List<CareerDto>();


            team = await _context.Team.FirstOrDefaultAsync(x => x.TeamId == player.TeamId);
            season = await _context.Season.FirstOrDefaultAsync(x => x.SeasonId == team.SeasonId);
            league = await _context.League.FirstOrDefaultAsync(x => x.LeagueId == season.LeagueId);

            careerList.Add(new CareerDto
            {
                PlayerId = player.PlayerId,
                Apps = _context.MatchPlayer.Where(x => x.PlayerId == player.PlayerId).Count(),
                Goal = _context.Shot.Where(x => x.ShotPlayerId == player.PlayerId && x.Goal == true).Count(),
                League = league.Name,
                RedCard = _context.Fault.Where(x => x.PlayerId == player.PlayerId && x.Cardtype == Cardtype.Red).Count(),
                Season = season.Value,
                Team = team.Name,
                YellowCard = _context.Fault.Where(x => x.PlayerId == player.PlayerId && x.Cardtype == Cardtype.Yellow).Count(),
            });

            return careerList;
        }

        public async Task<PlayerHeadLineDto> GetPlayerHeadlineById(GetPlayerHeadlineById getPlayerHeadlineById)
        {
            return await (from p in _context.Player
                          from t in _context.Team
                          where p.PlayerId == getPlayerHeadlineById.PlayerId && p.TeamId == t.TeamId
                          select new PlayerHeadLineDto
                          {
                              Name = p.Name,
                              Age = $"Kor: {p.Age} ({p.Birthday})",
                              teamLogo = $"assets/logo/{t.Logo}",
                              Position = $"{p.TeamPosition } ({t.Name})"
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<PlayerMatchDto>> PlayerMatch(GetPlayerMatchListById getPlayerMatchListById)
        {
            var match_list = await _context.MatchPlayer.Where(x => x.PlayerId == getPlayerMatchListById.PlayerId).ToListAsync();

            var playerMatch = new List<PlayerMatchDto>();

            foreach (var player_match in match_list)
            {
                var match = _context.Match.Where(x => x.MatchId == player_match.MatchId).FirstOrDefault();

                playerMatch.Add(new PlayerMatchDto
                {
                    PlayerId = player_match.MatchId,
                    MatchId = match.MatchId,
                    AwayTeam = _context.Team.FirstOrDefault(x => x.TeamId == match.AwayTeamId).Name,
                    HomeTeam = _context.Team.FirstOrDefault(x => x.TeamId == match.HomeTeamId).Name,
                    Fulltime = _context.MatchFact.FirstOrDefault(x => x.MatchId == match.MatchId).FullTime,
                    GameWeek = $"{_context.Round.FirstOrDefault(x => x.RoundId == match.RoundId).GameWeek}. forduló",
                    Goal = _context.Shot.Where(x => x.ShotPlayerId == player_match.PlayerId && x.MatchId == match.MatchId && x.Goal == true).Count(),
                    League = "Premier League",
                    PlayedMinutes = player_match.PlayedMinutes,
                    RedCard = _context.Fault.Where(x => x.PlayerId == player_match.PlayerId && x.MatchId == match.MatchId && x.Cardtype == Cardtype.Red).Count(),
                    YellowCard = _context.Fault.Where(x => x.PlayerId == player_match.PlayerId && x.MatchId == match.MatchId && x.Cardtype == Cardtype.Yellow).Count(),
                });
            }

            return playerMatch;
        }

    }
}
