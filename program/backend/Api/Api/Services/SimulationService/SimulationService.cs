﻿using Api.Data;
using Api.Dtos.MatchDto.Comment;
using Api.Dtos.MatchDto.LineUp;
using Api.Dtos.SimulationDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using Api.Dtos.SimulationDto.MatchDto;
using Api.Dtos.SimulationDto.RoundDto;
using Api.Models;
using Api.Repository.SimulationRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services.SimulationService
{
    public class SimulationService : ISimulationService, ICommentService, IEventService
    {
        private readonly SportContext _context;

        public SimulationService(SportContext context)
        {
            _context = context;
        }

        public async Task<List<RoundMatchDto>> CreateRoundSimulationRequest(CreateRoundSimulationRequest createRoundSimulationRequest)
        {
            var round = await _context.Round.FirstOrDefaultAsync(x => x.RoundId == createRoundSimulationRequest.RoundId);

            if (round.Simulated == false)
            {
                round.Simulated = true;
                await _context.SaveChangesAsync();
            }

            return await (from m in _context.Match
                          from mf in _context.MatchFact
                          where m.RoundId == createRoundSimulationRequest.RoundId && m.MatchId == mf.MatchId
                          select new RoundMatchDto
                          {
                              MatchId = m.MatchId,
                              Status = m.Simulated ? "Szimulált" : "Nem szimulált",
                              HomeTeam = _context.Team.Where(x => x.TeamId == m.HomeTeamId).FirstOrDefault().Name,
                              AwayTeam = _context.Team.Where(x => x.TeamId == m.AwayTeamId).FirstOrDefault().Name,
                              Halftime = mf.HalfTime,
                              Fulltime = mf.FullTime
                          }).ToListAsync();
        }

        public async Task<SimulationPageHeadlineDto> SimulationPageHeadLineRespone(CreateRoundSimulationRequest createRoundSimulationRequest)
        {
            return await (from r in _context.Round
                          from s in _context.Season
                          from l in _context.League
                          where r.RoundId == createRoundSimulationRequest.RoundId
                          && s.SeasonId == r.SeasonId
                          && l.LeagueId == s.LeagueId
                          select new SimulationPageHeadlineDto
                          {
                              Title = $"{s.Value} {l.Name} {r.GameWeek}. forduló"
                          }).FirstOrDefaultAsync();
        }


        public async Task BeginMatchSimulationRequest(CreateMatchSimulationRequest createMatchSimulationRequest)
        {
            var match = await _context.Match.FirstOrDefaultAsync(x => x.MatchId == createMatchSimulationRequest.MatchId);

            if (match.Simulated == false)
            {
                match.Simulated = true;
                _context.Update(match);
                await _context.SaveChangesAsync();

                int players = _context.MatchPlayer.Where(x => x.MatchId == createMatchSimulationRequest.MatchId).Count();

                if (players == 0)
                {
                    GenerateStartingTeam(match.MatchId, match.HomeTeamId, match.AwayTeamId);          // kezdőcsapatok
                    GenerateSubstitutes(match.MatchId, match.HomeTeamId, match.AwayTeamId);           // cserék
                    SecondHalfBeginCommentar(match.MatchId, match.Referee);                           // második félidő kezdő kommentár
                    GameExtensionComment(match.MatchId);
                    PossesionComment(match.MatchId, match.HomeTeamId, match.AwayTeamId);

                    var matchFact = await _context.MatchFact.FirstOrDefaultAsync(x => x.MatchId == createMatchSimulationRequest.MatchId);

                    GenerateOffside(match.MatchId, match.HomeTeamId, matchFact.HomeOffsides, match.AwayTeamId, matchFact.AwayOffsides);

                    var homePlayers = _context.MatchPlayer.Where(x => x.TeamId == match.HomeTeamId).ToList();
                    var awayPlayers = _context.MatchPlayer.Where(x => x.TeamId == match.AwayTeamId).ToList();

                    GenerateShot(match.MatchId, homePlayers, awayPlayers);
                    GenerateFault(match.MatchId, homePlayers, awayPlayers);

                    string homeTeam = _context.Team.Where(x => x.TeamId == match.HomeTeamId).Select(x => x.Name).FirstOrDefault();
                    string awayTeam = _context.Team.Where(x => x.TeamId == match.AwayTeamId).Select(x => x.Name).FirstOrDefault();

                    BallStatusComment(match.MatchId, homeTeam);
                    BallStatusComment(match.MatchId, awayTeam);

                }
            }
        }

        public void GenerateStartingTeam(int matchId, int homeTeamId, int awayTeamId)
        {
            var homePlayers = _context.Player.Where(x => x.TeamId == homeTeamId && x.TeamPosition != "SUB" && x.TeamPosition != "RES").ToList();
            var awayPlayers = _context.Player.Where(x => x.TeamId == awayTeamId && x.TeamPosition != "SUB" && x.TeamPosition != "RES").ToList();
            string homeTeam = _context.Team.Where(x => x.TeamId == homeTeamId).Select(x => x.Name).FirstOrDefault();
            string awayTeam = _context.Team.Where(x => x.TeamId == awayTeamId).Select(x => x.Name).FirstOrDefault();

            List<MatchPlayer> matchPlayers = new List<MatchPlayer>();

            foreach (var player in homePlayers)
            {
                matchPlayers.Add(new MatchPlayer
                {
                    MatchId = matchId,                          // mérközés id
                    PlayerId = player.PlayerId,                 // játékos id
                    TeamId = player.TeamId,                     // csapat id
                    InGame = true,                              // kezdőcsapatba van és éppen játszik
                    StartingTeamPlayer = true,                  // kezdőcsapat
                    Substitute = false,                         // nem csere, mert kezdő
                    Bench = false,                              // nem kispados, mert kezdő
                    PlayedMinutes = 90,                         // 90 percet játszik teljes mérközés
                    RedCard = 0,
                    YellowCard = 0,
                    Assist = 0,
                    Goal = 0,
                    PlayerName = player.NickName,
                    TeamName = homeTeam,
                    PlayerStatus = PlayerStatus.Home11,
                });
            }

            foreach (var player in awayPlayers)
            {
                matchPlayers.Add(new MatchPlayer
                {
                    MatchId = matchId,                          // mérközés id
                    PlayerId = player.PlayerId,                 // játékos id
                    TeamId = player.TeamId,                     // csapatid
                    Bench = false,                              // nem kispados
                    PlayerStatus = PlayerStatus.Away11,         // vendég kezdő játékos
                    InGame = true,                              // játékban van
                    StartingTeamPlayer = true,                  // kezdőcsapat játékos
                    Substitute = false,                         // nem csere
                    PlayedMinutes = 90,                         // ennyi percet játszik
                    RedCard = 0,
                    YellowCard = 0,
                    PlayerName = player.NickName,
                    TeamName = awayTeam,
                    Assist = 0,
                    Goal = 0
                });
            }

            _context.AddRange(matchPlayers);                        // hozzáadom
            _context.SaveChanges();                                 // elmentem
        }

        public void GenerateSubstitutes(int matchId, int homeTeamId, int awayTeamId)
        {
            var homePlayers = _context.MatchPlayer.Where(x => x.TeamId == homeTeamId).ToList();
            var awayPlayers = _context.MatchPlayer.Where(x => x.TeamId == awayTeamId).ToList();

            var homeSubstitutedPlayers = _context.Player.Where(x => x.TeamId == homeTeamId && x.TeamPosition == "SUB").ToList();
            var awaySubstitutedPlayers = _context.Player.Where(x => x.TeamId == awayTeamId && x.TeamPosition == "SUB").ToList();

            string homeTeam = _context.Team.Where(x => x.TeamId == homeTeamId).Select(x => x.Name).FirstOrDefault();
            string awayTeam = _context.Team.Where(x => x.TeamId == awayTeamId).Select(x => x.Name).FirstOrDefault();

            var rand = new Random();
            int minute = 0;

            var matchPlayer = new MatchPlayer();

            // home team players substitutes
            for (int i = 0; i < 3; i++)
            {
                minute = rand.Next(40, 90);
                matchPlayer = new MatchPlayer
                {
                    MatchId = matchId,                                  // mézközés id
                    PlayerId = homeSubstitutedPlayers[i].PlayerId,      // játékos id ezek a becsereléndő játékosok
                    TeamId = homeSubstitutedPlayers[i].TeamId,          // csapat id
                    InGame = true,                                      // játékban vannak
                    StartingTeamPlayer = false,                         // nem kezdők
                    Substitute = true,                                  // cserék
                    Bench = false,                                      // nem kispadosok
                    PlayedMinutes = 90 - minute,                             // ennyi percet játszanak
                    RedCard = 0,
                    YellowCard = 0,
                    Assist = 0,
                    PlayerName = homeSubstitutedPlayers[i].NickName,
                    TeamName = homeTeam,
                    Goal = 0,
                    SubPlayerId = homePlayers[i].PlayerId,
                    PlayerStatus = PlayerStatus.HomeSub                 // hazai becserélt játékos
                };

                _context.Add(matchPlayer);                              // hozzáadom a játékosok listájához, mert becserélték
                _context.SaveChanges();                                 // elmentem

                homePlayers[i].InGame = false;                          // kezdő 11 játékos már nem játszik, mert lecserélték
                homePlayers[i].Substitute = true;                       // lecserélték
                homePlayers[i].PlayedMinutes = minute;             // ennyi percet játszanak                  
                homePlayers[i].PlayerStatus = PlayerStatus.Home11Sub;   // hazai kezdő lecserélt játékos

                _context.Update(homePlayers[i]);                        // frissítem a játékosok listáját
                _context.SaveChanges();                                 // elmentem


                SubStituteComment(matchId, minute, rand.Next(1, 9), matchPlayer.PlayerName, homePlayers[i].PlayerName, matchPlayer.TeamName);
            }

            // away team players substitutes
            for (int i = 0; i < 3; i++)
            {
                minute = rand.Next(40, 90);
                matchPlayer = new MatchPlayer
                {
                    MatchId = matchId,                                  // mérközés id
                    PlayerId = awaySubstitutedPlayers[i].PlayerId,      // ezek a becserélendő játékosok vendég csapatnál
                    TeamId = awaySubstitutedPlayers[i].TeamId,          // csapat id
                    InGame = true,                                      // játékban van                        
                    StartingTeamPlayer = false,                         // nem kezdő
                    Substitute = true,                                  // csere
                    Bench = true,                                       // nem kispados
                    PlayedMinutes = 90 - minute,                             // ennyi percet játszik
                    RedCard = 0,
                    PlayerName = awaySubstitutedPlayers[i].NickName,
                    TeamName = awayTeam,
                    YellowCard = 0,
                    Assist = 0,
                    Goal = 0,
                    SubPlayerId = awayPlayers[i].PlayerId,
                    PlayerStatus = PlayerStatus.AwaySub                 // vendég csapat csere játékosa
                };

                _context.Add(matchPlayer);                              // hozzáadom a listához
                _context.SaveChanges();

                awayPlayers[i].InGame = false;                          // lecserélt játékos már nem játszik
                awayPlayers[i].Substitute = true;                       // lecserélték
                awayPlayers[i].PlayedMinutes = minute;                  // ennyi percet játszott                  
                awayPlayers[i].PlayerStatus = PlayerStatus.Away11Sub;   // lecserélt kezdő vendég játékos

                _context.Update(awayPlayers[i]);                        // frissítem a játékosok listáját
                _context.SaveChanges();                                 // elmentem

                SubStituteComment(matchId, minute, rand.Next(1, 9), matchPlayer.PlayerName, homePlayers[i].PlayerName, matchPlayer.TeamName);
            }

            // home team bench player
            for (int i = 3; i < 8; i++)                                 // ezek a maradék nem becserélt játékosok akik, kispadosok maradnak
            {
                matchPlayer = new MatchPlayer
                {
                    MatchId = matchId,                                  // mérközés id
                    PlayerId = homeSubstitutedPlayers[i].PlayerId,      // játékos id
                    TeamId = homeSubstitutedPlayers[i].TeamId,          // csapat id
                    InGame = false,                                     // nem lépnek pályára
                    StartingTeamPlayer = false,                         // nem kezdők
                    Substitute = false,                                 // nem cserék
                    Bench = true,                                       // kispadosok      
                    PlayedMinutes = 0,
                    RedCard = 0,
                    YellowCard = 0,
                    PlayerName = homeSubstitutedPlayers[i].NickName,
                    TeamName = homeTeam,
                    Assist = 0,
                    Goal = 0,
                    PlayerStatus = PlayerStatus.HomeBench
                };

                _context.Add(matchPlayer);                   // hozzáadom a listához, hogy ott voltak a mérközésen de nem játszottak
                _context.SaveChanges();                      // elmentem
            }

            // away team bench player
            for (int i = 3; i < 8; i++)
            {
                matchPlayer = new MatchPlayer
                {
                    MatchId = matchId,                                  // mérközés id
                    PlayerId = awaySubstitutedPlayers[i].PlayerId,      // játékos id
                    TeamId = awaySubstitutedPlayers[i].TeamId,          // csapat id
                    InGame = false,                                     // nem játszanak
                    StartingTeamPlayer = false,                         // nem kezdők
                    Substitute = false,                                 // nem cserék
                    Bench = true,                                       // kispadosak                      
                    PlayedMinutes = 0,
                    RedCard = 0,
                    YellowCard = 0,
                    PlayerName = awaySubstitutedPlayers[i].NickName,
                    TeamName = homeTeam,
                    Assist = 0,
                    Goal = 0,
                    PlayerStatus = PlayerStatus.AwayBench
                };

                _context.Add(matchPlayer);                // hozzáadom a listához, hogy ott voltak a mérközésen de nem játszottak
                _context.SaveChanges();                   // elmentem
            }
        }

        // Csere
        // kiemelt esemény

        public void SubStituteComment(int matchId, int minute, int category, string inPlayer, string outPlayer, string substituteTeam)
        {

            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"{inPlayer} lejön, {outPlayer} ({substituteTeam}) érkezik helyette.",
                    2 => $"Íme a csere. {outPlayer} adja át a helyét csapattársának, mostantól {inPlayer} ({substituteTeam}) a pályán.",
                    3 => $"Az edző cserélni kényszerül. {outPlayer} ({substituteTeam}) nem tudja folytatni, {inPlayer} lép helyette pályára.",
                    4 => $"Időközben csere történt, {inPlayer} ({substituteTeam}) érkezett {outPlayer} helyére.",
                    5 => $"Csere. {outPlayer} jön le, az edző úgy döntött, hogy {inPlayer} ({substituteTeam}) lép majd pályára.",
                    6 => $"Íme a csere. {outPlayer} megy le, helyére {inPlayer} ({substituteTeam}) érkezik.",
                    7 => $"Az edző lehívja a pályáról játékosát, {outPlayer} helyett {inPlayer} ({substituteTeam}) lép pályára.",
                    8 => $"Csere történt {outPlayer} lemegy, {inPlayer} ({substituteTeam}) lép pályára helyette.",
                    _ => $"{inPlayer} lejön, {outPlayer} ({substituteTeam}) érkezik helyette.",
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.SubStitute,
            };

            AddComment(Comment);
        }

        public void AddComment(Comment Comment)
        {
            _context.Add(Comment);
            _context.SaveChanges();
        }

        // Labda állapota
        // nem kiemelt esemény

        public void BallStatusComment(int matchId, string team)
        {
            var rand = new Random();

            for (int i = 0; i < 4; i++)
            {
                int minute = rand.Next(0, 90);
                int category = rand.Next(0, 8);

                Comment Comment = new Comment()
                {
                    Description = category switch
                    {
                        1 => $"{team} csapat játékosa szerezte meg a labdát az ötösön belül egy bedobásból.",
                        2 => $"A(z) {team} tartja a labdát, passzolgatnak, próbálják előkészíteni a támadást. Várnak a megfelelő pillanatra, hogy áttörjék a védelmet.",
                        3 => $"A beadás a tizenhatoson belül nem ért célba. {team} csapat játékosa ettől jobban is célozhatott volna. A labda kiment a pályáról, az ellenfél jöhet kirúgással.",
                        4 => $"A(z) {team} játékosai passzolgatnak, újabb támadás szerveződik.",
                        5 => $"A(z) {team} rövid passzokkal készül az újabb támadás felépítésére.",
                        6 => $"{team} csapat játékosa passza a senki földjére érkezett, a támadás így befejeződik.",
                        7 => $"{team} csapat játékosa rázza le a védőket a kapu előterében és kapura fejel. A labda messze kapu fölé megy. A labda kiment az alapvonalon, az ellenfél jöhet kirúgással.",
                        8 => $"Hosszú, de pontatlan labdát küldött előre {team} csapat játékosa, a játékszer nem jutott csapattárshoz.",
                        _ => $"{team} csapat játékosa szerezte meg a labdát az ötösön belül egy bedobásból.",
                    },
                    MatchId = matchId,
                    Minute = minute,
                    Type = CommentType.Ballstatus,
                };

                AddComment(Comment);
            }
        }

        // Labdabirtoklás
        // nem kiemelt esemény

        public void PossesionComment(int matchId, int hometeamId, int awayteamId)
        {
            var rand = new Random();

            for (int i = 5; i <= 90; i += 5)
            {
                int homePossesion = rand.Next(40, 60);
                int awayPossesion = 100 - homePossesion;

                Comment Comment = new Comment
                {
                    Description = $"{homePossesion}:{awayPossesion} % – ez a labdabirtoklás aránya.",
                    MatchId = matchId,
                    Minute = i,
                    Type = CommentType.Possesion,
                };

                AddComment(Comment);

                Possesion Possesion = new Possesion
                {
                    MatchId = matchId,
                    Minute = i,
                    HomeTeamId = hometeamId,
                    AwayTeamId = awayteamId,
                    HomePossesion = homePossesion,
                    AwayPossesion = awayPossesion,
                    HomeTeamPass = ((40 * homePossesion) / 100),
                    AwayTeamPass = ((40 * awayPossesion) / 100)
                };

                _context.Add(Possesion);
                _context.SaveChanges();
            }
        }

        // Első félidő vége, második félidő eleje kommentár

        public void SecondHalfBeginCommentar(int matchId, string referee)
        {
            Comment Comment = new Comment()
            {
                Description = $"{referee} sípol, vége az első félidőnek. A játékvezető sípszavára kezdetét veszi a második félidő.",
                MatchId = matchId,
                Minute = 45,
                Type = CommentType.SecondHalf,
            };

            AddComment(Comment);
        }

        // Játék hoszabítás mérközés kommentár

        public void GameExtensionComment(int matchId)
        {
            var rand = new Random();

            for (int i = 0; i < 2; i++)
            {
                int extension = rand.Next(0, 5);

                if (i == 0)
                {
                    Comment FirstHalf = new Comment()
                    {
                        Description = $"{extension} perc lesz a hosszabbítás.",
                        MatchId = matchId,
                        Minute = 45,
                        Type = CommentType.GameExtension,
                    };

                    AddComment(FirstHalf);
                }
                else
                {
                    Comment SecondHalf = new Comment()
                    {
                        Description = $"{extension} perc lesz a hosszabbítás.",
                        MatchId = matchId,
                        Minute = 90,
                        Type = CommentType.GameExtension,
                    };

                    AddComment(SecondHalf);
                }
            }
        }

        // Kapura lövések

        public void GenerateShot(int matchId, List<MatchPlayer> hometeam, List<MatchPlayer> awayteam)
        {
            int shot = _context.Shot.Where(x => x.MatchId == matchId).Count();

            if (shot == 0)
            {
                var match = _context.Match.FirstOrDefault(x => x.MatchId == matchId);
                var matchfact = _context.MatchFact.FirstOrDefault(x => x.MatchId == matchId);

                GenerateOffTargetShot(matchId, match.HomeTeamId, matchfact.HomeTotalShots - matchfact.HomeShotsOnTarget);                                   // kapu nem találó lövés
                GenerateOffTargetShot(matchId, match.AwayTeamId, matchfact.AwayTotalShots - matchfact.AwayShotsOnTarget);
                GenerateGoal(matchId, match.HomeTeamId, matchfact.HomeHalfTimeGoalCount, matchfact.HomeGoalCount - matchfact.HomeHalfTimeGoalCount, hometeam);        // gól
                GenerateGoal(matchId, match.AwayTeamId, matchfact.AwayHalfTimeGoalCount, matchfact.AwayGoalCount - matchfact.AwayHalfTimeGoalCount, awayteam);

                var rand = new Random();

                int homeSavedShot = rand.Next(0, matchfact.HomeShotsOnTarget - matchfact.HomeGoalCount);                                                    // védett random szám 0 és összes - gól között
                int awaySavedShot = rand.Next(0, matchfact.AwayShotsOnTarget - matchfact.AwayGoalCount);
                int homeBlockedShot = matchfact.HomeShotsOnTarget - matchfact.HomeGoalCount - homeSavedShot;                                                // blokkolt = összes - gól - védett
                int awayBlockedShot = matchfact.AwayShotsOnTarget - matchfact.AwayGoalCount - awaySavedShot;

                GenerateSavedShot(matchId, match.HomeTeamId, homeSavedShot, hometeam);                                                                                // védett lövés
                GenerateSavedShot(matchId, match.AwayTeamId, awaySavedShot, awayteam);
                GenerateBlockedShot(matchId, match.HomeTeamId, homeBlockedShot, hometeam);                                                                            // blokkolt lövés
                GenerateBlockedShot(matchId, match.AwayTeamId, awayBlockedShot, awayteam);
                GenerateCorner(matchId, match.HomeTeamId, matchfact.HomeCorners, hometeam);                                                                           // szöglet
                GenerateCorner(matchId, match.AwayTeamId, matchfact.AwayCorners, awayteam);
                GenerateFreeKick(matchId, match.HomeTeamId, rand.Next(0, 5), hometeam);                                                                                // szabadrugás
                GenerateFreeKick(matchId, match.AwayTeamId, rand.Next(0, 5), awayteam);
            }
        }

        // Gól

        public void GenerateGoal(int matchId, int teamId, int firsthalf, int secondhalf, List<MatchPlayer> players)
        {
            var rand = new Random();


            // Első félidő gólok

            for (int i = 0; i < firsthalf; i++)
            {
                int minute = rand.Next(0, 45);

                Shot Goal = new Shot()
                {
                    MatchId = matchId,
                    TeamId = teamId,
                    ShotPlayerId = players.ElementAt(rand.Next(players.Count)).PlayerId,
                    Minute = minute,
                    LeagueId = 1,
                    SeasonId = 1,
                    Goal = true,
                    Event = Event.Goal
                };

                _context.Add(Goal);
                _context.SaveChanges();

                GoalComment(rand.Next(1, 26), matchId, minute, players.ElementAt(rand.Next(players.Count)).PlayerName, players.ElementAt(rand.Next(players.Count)).TeamName);
            }

            // Második félidő gólok

            for (int i = 0; i < secondhalf; i++)
            {
                int minute = rand.Next(45, 90);

                Shot Goal = new Shot()
                {
                    MatchId = matchId,
                    TeamId = teamId,
                    ShotPlayerId = players.ElementAt(rand.Next(players.Count)).PlayerId,
                    Minute = minute,
                    LeagueId = 1,
                    SeasonId = 1,
                    Goal = true,
                    Event = Event.Goal
                };

                _context.Add(Goal);
                _context.SaveChanges();

                GoalComment(rand.Next(1, 26), matchId, minute, players.ElementAt(rand.Next(players.Count)).PlayerName, players.ElementAt(rand.Next(players.Count)).TeamName);
            }

        }

        public void GoalComment(int category, int matchId, int minute, string player, string team)
        {
            var rand = new Random();

            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"Micsoda befejezés! {player} ({team}) veszi át a labdát az üres területen és a léc alá lő.",
                    2 => $"Gól! {player} ({team}) kapott egy esélyt szinte ajándékba és nem is hibázott, a hálóba fejelte a labdát.",
                    3 => $"Gól! {player} ({team}) nyerte az idegek csatáját, védhetetlen lövése a kapus mellett vágódott a kapu jobb oldalába.",
                    4 => $"Gyönyörű jelenetnek lehettünk szemtanúi a tizenhatoson kívül! {player} ({team}) remek ütemben lőtt a kapu bal oldalába.",
                    5 => $"Gól! {player} ({team}) vágta be a kipattanót a bal alsó sarokba.",
                    6 => $"A játékvezető az eset visszanézése után jelzi, hogy a gól érvényes, a(z) {team} játékosai ünnepelhetnek!",
                    7 => $"Gól! {player} ({team}) kapott gólpasszt, a csapattárs labdája után csak annyi dolga volt, hogy az üres kapuba lőjjön.",
                    8 => $"{player} ({team}) remek labdát kap, áttör a védelmen és a léc alá tüzel.",
                    9 => $"Gól! {player} ({team}) szerezte meg a labdát majd ahogy látta, hogy kinn áll a kapus, átemelte fölötte a labdát.",
                    10 => $"Gól! {player} az alapvonalig, ahonnan beadta a labdát. A csapattársa ugrott fel legmagasabbra és közelről befejelte a labdát a kapu jobb alsó sarkába.",
                    11 => $"{player} ({team}) szerez gólt, miután jól érkezett a beadásra és remek lövést mutat be a tizenhatosról.A labda a kapu jobb alsó sarkába megy a kapus mellett.",
                    12 => $"A {team} csapat játékosa nézi, hogy kinek tudna passzolni. {player} ({team}) tökéletesen veszi át a labdát és csodálatos lövést mutat be közvetlenül a tizenhatoson kívülről. A labda elindul a kapu jobb oldalába.",
                    13 => $"GÓL! Remek játékot mutatott be {player}, jól vette ki a részét a támadásépítésből. A csapattáresa kapte  a labdát ({team}) aki pedig briliánsan lőtt a kapu bal oldalába.",
                    14 => $"Videóbiró döntött! A gól érvényes, a(z) {team} ünnepelhet!",
                    15 => $"{player} ({team}) egy kiváló keresztlabdára jól érkezett és a kapu jobb oldalába lőtt.",
                    16 => $"Varázslatos volt! {player} ({team}) vette át a pontos passzt és szép lövést küldött kapura a bal felső sarok irányába, a kapufáról pattan be. A kapus tehetetlen volt.",
                    17 => $"GÓL! {player} ({team}) érkezett remek ütemben a beadására a kontra végén, majd kapásból lőtt, közepes távolságból leadott lövése pedig meg sem állt a hálóig",
                    18 => $"Szólója végén {player} ({team}) betör a büntetőterületre.Ellövi a labdát pontosan a kapu bal oldalába. A kapus tehetetlen volt.",
                    19 => $"Gól! {player} ({team}) kapásból lőtte el a labdát a tizenhatoson belülről, a játékszer a kapu bal alsó sarkába megy.",
                    20 => $"Öngól! Kínos percek ezek a játékos számára, akiről az ötösön belülről a gyenge fejest követően saját hálójába pattan a labda.",
                    21 => $"A labda bent van a hálóban! {player} küld szép magas labdát csapattársa irányába ({team}) lecsap rá és a tizenegyespont magasságából és az ellenfél kapujába, a bal alsó sarokba fejel. Nagyon gólérzékeny játékos és kiváló befejezés.",
                    22 => $"{player} ({team}) érkezett a beadásra és ragyogó fejest küldött a kapu bal alsó sarkába.",
                    23 => $"Gól! {player} jól látja meg, hogy a csapattársa ({team}) elindul.Meg is kapja a labdát és ellövi a kapuba.",
                    24 => $"Micsoda gól! {player} ({team}) hatalmas önbizalomról tett tanúbizonyságot, a tizenhatoson nem átvette, hanem azonnal kapura lőtte a labdát. A kapáslövés a jobb felső sarokba ment.",
                    25 => $"A gólszerző {player} ({team})! Átveszi a labdát a kapu torkában a szabadrúgást követően és azonnal lő, a labda a kapu jobb oldalába megy.",
                    26 => $"{player} ({team}) szépen tört előre, majd a labdát remekül emelte át a kapus fölött.",
                    _ => $"Micsoda befejezés! {player} ({team}) veszi át a labdát az üres területen és a léc alá lő.",
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.Goal,
            };

            AddComment(Comment);
        }

        // Kaput nem találó lövések

        public void GenerateOffTargetShot(int matchId, int teamId, int offTarget)
        {
            var rand = new Random();

            for (int i = 0; i < offTarget; i++)
            {
                Shot Offtarget = new Shot()
                {
                    MatchId = matchId,
                    TeamId = teamId,
                    Minute = rand.Next(0, 90),
                    Event = Event.OffTarget
                };

                _context.Add(Offtarget);
                _context.SaveChanges();
            }
        }

        // Blokkolt lövés

        public void GenerateBlockedShot(int matchId, int teamId, int blockedShot, List<MatchPlayer> matchPlayers)
        {
            var rand = new Random();

            for (int i = 0; i < blockedShot; i++)
            {
                int index = rand.Next(matchPlayers.Count);
                int minute = rand.Next(0, 90);

                Shot BlockedShot = new Shot()
                {
                    SeasonId = 1,
                    LeagueId = 1,
                    Event = Event.BlockedShot,
                    MatchId = matchId,
                    Minute = minute,
                    TeamId = teamId,
                    ShotPlayerId = matchPlayers[index].PlayerId,
                };

                _context.Add(BlockedShot);
                _context.SaveChanges();

                BlockedShotComment(matchId, minute, rand.Next(1, 13), matchPlayers[i].PlayerName, matchPlayers[i].TeamName);
            }
        }

        public void BlockedShotComment(int matchId, int minute, int category, string shotPlayer, string attackingTeam)
        {
            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"Újabb remek beadást mutat be {shotPlayer} ({attackingTeam}). A védőknek nem okoz gondot, kivágják a labdát. A(z) {attackingTeam} szögletével folytatódik a játék.",
                    2 => $"{shotPlayer} ({attackingTeam}) adja be a labdát, de a védők fel tudnak szabadítani.",
                    3 => $"{shotPlayer} ({attackingTeam}) lőtte be oldalról a labdát, de nem úgy, ahogy szerette volna, a jól szervezett védelem tisztázni tudott.",
                    4 => $"{shotPlayer} ({attackingTeam}) adja be a labdát. A védőknek sikerül kivágni a játékszert. Az asszisztens a szögletzászlóra mutat, Magyarország-szöglet következik.",
                    5 => $"{shotPlayer} ({attackingTeam}) előrelőtt labdáját a védők kivágták az ötösön belülről.",
                    6 => $"{shotPlayer} ({attackingTeam}) került ígéretes helyzetbe, jó labdát kapott. Kapura is próbált lőni, de az egyik védő közbelépett. A játékvezető a(z) {attackingTeam} javára ítélt szögletet.",
                    7 => $"{shotPlayer} ({attackingTeam}) lövi be a labdát a büntetőterületre. A védők közbelépnek, tisztázni tudnak.",
                    8 => $"Ez aztán pech! {shotPlayer} ({attackingTeam}) kiválóan vette át a labdát, majd szépen lőtt kapura, de a labda elakadt egy védőben. Az asszisztens jól döntött, Magyarország-szöglettel folytatódik a játék.",
                    9 => $"{shotPlayer} ({attackingTeam}) próbálta beadni a labdát, de az a csapattársai helyett csak a védőkig jutott, akik felszabadítottak. {attackingTeam} szöglet következik. Újra veszélyben forog a kapu.",
                    10 => $"{shotPlayer} ({attackingTeam}) vette át a labdát, de tizenhatosról leadott lövését blokkolták.",
                    11 => $"{shotPlayer} ({attackingTeam}) passzát blokkolták.",
                    12 => $"{shotPlayer} ({attackingTeam}) érkezik az előrelőtt labdára, de a védelem tisztáz.",
                    13 => $"{shotPlayer} ({attackingTeam}) bejut a tizenhatoson belülre, de labdáját az egyik védő blokkolni tudja.",
                    _ => $"{shotPlayer} ({attackingTeam}) előrelőtt labdáját a védők kivágták az ötösön belülről.",
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.Blockedshot,
            };

            AddComment(Comment);
        }

        // Védet lövés

        public void GenerateSavedShot(int matchId, int teamId, int savedShot, List<MatchPlayer> matchPlayers)
        {
            var rand = new Random();

            string referee = _context.Match.Where(x => x.MatchId == matchId).Select(x => x.Referee).FirstOrDefault();

            for (int i = 0; i < savedShot; i++)
            {
                int index = rand.Next(matchPlayers.Count);
                int minute = rand.Next(0, 90);

                Shot SavedShot = new Shot()
                {
                    MatchId = matchId,
                    TeamId = teamId,
                    LeagueId = 1,
                    SeasonId = 1,
                    ShotPlayerId = matchPlayers[index].PlayerId,
                    Minute = minute,
                    Event = Event.SavedShot
                };

                _context.Add(SavedShot);
                _context.SaveChanges();

                SavedShotComment(matchId, minute, rand.Next(1, 13), matchPlayers[i].PlayerName, matchPlayers[i].TeamName, referee);
            }
        }

        public void SavedShotComment(int matchId, int minute, int category, string shotPlayer, string shotTeam, string referee)
        {
            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"A kapus ugrik fel és húzza le {shotPlayer} ({shotTeam}) szögletből beívelt labdáját.",
                    2 => $"{shotPlayer} ({shotTeam}) helyet csinál magának a tizenhatoson és veszélyes lövést ereszt meg. A kapusnak jó eséllyel gondot okozott volna a lövés, de az egyik védő közbe tud avatkozni és menteni tud.",
                    3 => $"{shotPlayer} ({shotTeam}) kapja jó ütemben a labdát és lövéssel próbálkozik! A kapus résen volt, a távoli lövést sikerül a bal kapufára ütnie.",
                    4 => $"{shotPlayer} ({shotTeam}) lábára érkezett a beadás, aki már csak a kapussal állt szemben. A kapu közepébe tartó lövése viszont nem mondható erőteljesnek, a kapus könnyedén védeni tudta.",
                    5 => $"{shotPlayer} ({shotTeam}) beadása túl erős volt, így a kapus kezébe kerül a labda, az akciónak vége.",
                    6 => $"{shotPlayer} ({shotTeam}) cselezte át magát a védőkön, majd remek lövőhelyzetben találta magát. Lövése a tizenhatoson kívülről ment a kapu jobb oldala felé, a próbálkozás nem volt veszélyes, és a kapus könnyedén védeni tudott.",
                    7 => $"Mint a lavina. {shotPlayer} ({shotTeam}) kapott figyelmeztetést támadóként elkövetett szabálytalanságáért. {referee} látta az esetet és meg is állította a játékot. Az ellenfél játékosa reklamál, de ez nem változtatja meg a játékvezető döntését.",
                    8 => $"{shotPlayer} ({shotTeam}) lő kapura laposan a tizenhatosról briliáns szólója végén. A labda a kapu jobb oldalába tart, de a kapus fantasztikus mozdulattal védeni tud.",
                    9 => $"{shotPlayer} ({shotTeam}) ugrik fel és fejel kapura a tizenegyespont magasságában, elfejeli a labdát a kapu bal oldala felé. a kapus remek reflexről tesz tanúbizonyságot, így a labda nem jut a hálóba.",
                    10 => $"{shotPlayer} ({shotTeam}) hagyott ki helyzetet a szögletet követően. Fejese nem volt rossz, de mivel laposan a kapu közepébe ment, így a kapus gond nélkül védeni tudott.",
                    11 => $"Egy kiváló passz érkezik {shotPlayer} ({shotTeam}) lába elé a kapu előterében, de a kapu közepébe tartó lapos lövést a kapus könnyedén hatástalanítja.",
                    12 => $"{shotPlayer} ({shotTeam}) erőből lőtt gólt, lövése a kapu közepét találta el.",
                    13 => $"{shotPlayer} ({shotTeam}) érkezett a szögletre és fejesét a kapu közepébe próbálta irányítani, de a kapus elképesztő mozdulattal védeni tudott.",
                    _ => $"A kapus ugrik fel és húzza le {shotPlayer} ({shotTeam}) szögletből beívelt labdáját.",
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.Savedshot,
            };

            AddComment(Comment);
        }

        // Szabadrugás

        public void GenerateFreeKick(int matchId, int teamId, int freekick, List<MatchPlayer> matchPlayers)
        {
            var rand = new Random();
            string referee = _context.Match.Where(x => x.MatchId == matchId).Select(x => x.Referee).FirstOrDefault();

            for (int i = 0; i < freekick; i++)
            {
                int minute = rand.Next(0, 90);

                Shot Freekick = new Shot()
                {
                    MatchId = matchId,
                    TeamId = teamId,
                    Minute = minute,
                    Event = Event.Freekick
                };

                _context.AddAsync(Freekick);
                _context.SaveChanges();

                FreekickComment(matchId, minute, rand.Next(1, 7), matchPlayers[i].PlayerName, matchPlayers[i].TeamName, referee);
            }
        }

        public void FreekickComment(int matchId, int minute, int category, string freeKickPlayer, string freeKickTeam, string referee)
        {
            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"A szabadrúgást {freeKickPlayer} ({freeKickTeam}) végezte el, de labdáját kivágták.",
                    2 => $"{freeKickPlayer} ({freeKickTeam}) tekerte be a labdát szabadrúgásból messziről a kapu elé, ám a kapus jól lépett ki a kapuból és véget vetett a támadásnak.",
                    3 => $"A kapus időben reagált {freeKickTeam} ({freeKickTeam}) távoli szabadrúgására. A labda a bal alsó sarokba tartott, de ő védeni tudta.",
                    4 => $"Az ígéretes helyről megítélt szabadrúgást {freeKickPlayer} ({freeKickTeam}) gyengén végezte el, nem talált kaput. Mekkora lehetőség volt! A labda kiment a pályáról, az ellenfél jöhet kirúgással.",
                    5 => $"{freeKickPlayer} ({freeKickTeam}) szabadrúgása száll be a kapu elé, de a védelem tisztázni tud. Szöglet. A(z) {freeKickTeam} előtt nyílik lehetőség a gólszerzésre.",
                    6 => $"{referee} fújt a sípjába. {freeKickPlayer} ({freeKickTeam}) veszélyes játéka miatt volt szükség erre. Szabadrúgás. A(z) {freeKickTeam} lehetőségével folytatódik a mérkőzés.",
                    7 => $"{freeKickPlayer} ({freeKickTeam}) lövi be a labdát a büntetőterületen belülre a szabadrúgásból. A védelem azonban felkészült és hatástalanítja a próbálkozást.",
                    _ => $"A szabadrúgást {freeKickPlayer} ({freeKickTeam}) végezte el, de labdáját kivágták.",
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.Freekick,
            };

            AddComment(Comment);
        }

        // Szöglet

        public void GenerateCorner(int matchId, int teamId, int corner, List<MatchPlayer> matchPlayers)
        {
            var rand = new Random();

            for (int i = 0; i < corner; i++)
            {
                int minute = rand.Next(0, 90);
                int index = rand.Next(matchPlayers.Count);

                Shot Corner = new Shot()
                {
                    MatchId = matchId,
                    TeamId = teamId,
                    Minute = minute,
                    LeagueId = 1,
                    SeasonId = 1,
                    ShotPlayerId = matchPlayers[index].PlayerId,
                    Event = Event.Corner
                };

                _context.Add(Corner);
                _context.SaveChanges();

                CornerComment(matchId, minute, rand.Next(1, 10), matchPlayers[i].PlayerName, matchPlayers[i].TeamName);
            }
        }

        // Szöglet

        public void CornerComment(int matchId, int minute, int category, string cornerKicker, string cornerTeam)
        {
            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"{cornerKicker} ({cornerTeam}) végzi el a szögletet, a labdát belövi a büntetőterületre, de a tömegből végül a védők végül fel tudnak szabadítani.",
                    2 => $"{cornerKicker} ({cornerTeam}) végzi el a szögletet és szépen lövi be a labdát a kapu elé, de a védelem készen áll és kitessékeli a labdát a kapu elől.",
                    3 => $"{cornerKicker} ({cornerTeam}) szögletet végez el, a védelem pedig tisztáz.",
                    4 => $"A szögletet {cornerKicker} ({cornerTeam}) ívelte be, de a védők kivágták a labdát.",
                    5 => $"{cornerKicker} ({cornerTeam}) íveli be a szögletet a kapu elé, ám a labda túl közel kerül a kapushoz, a kapus le is húzza a labdát.",
                    6 => $"{cornerKicker} ({cornerTeam}) ívelte be a szögletet, de Péter Gulácsi közbelépett és elhárította a veszélyt.",
                    7 => $"{cornerKicker} ({cornerTeam}) próbálta meg egyik csapattársa elé belőni a labdát a tizenhatoson belülre, de a védők még időben közbelépnek. Az asszisztens és a bíró is szögletnek látta az esetet, így a(z) {cornerTeam} sarokrúgása következik.",
                    8 => $"{cornerKicker} ({cornerTeam}) viszi a labdát a szögletzászlóhoz, ő ívelheti majd be a labdát.",
                    9 => $"{cornerKicker} ({cornerTeam}) tart a sarok felé, ő végzi majd el a szögletet. {cornerKicker} ({cornerTeam}) kis szögletet végzett el.",
                    10 => $"A bíró a szögletzászló irányába mutat. {cornerKicker} ({cornerTeam}) végzi el a sarokrúgást.",
                    _ => $"{cornerKicker} ({cornerTeam}) végzi el a szögletet, a labdát belövi a büntetőterületre, de a tömegből végül a védők végül fel tudnak szabadítani.",
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.Corner,
            };

            AddComment(Comment);
        }

        // Les

        public void GenerateOffside(int matchId, int hometeamId, int hometeamOffside, int awayteamId, int awayteamoffside)
        {
            var rand = new Random();

            string offsideTeam = _context.Team.Where(x => x.TeamId == hometeamId).Select(x => x.Name).FirstOrDefault();

            for (int i = 0; i < hometeamOffside; i++)
            {
                OffsideComment(matchId, rand.Next(0, 90), 1, offsideTeam);
            }

            offsideTeam = _context.Team.Where(x => x.TeamId == awayteamId).Select(x => x.Name).FirstOrDefault();

            for (int i = 0; i < awayteamoffside; i++)
            {
                OffsideComment(matchId, rand.Next(0, 90), 1, offsideTeam);
            }
        }

        // Les
        // nem kiemelt esemény

        public void OffsideComment(int matchId, int minute, int category, string offsideTeam)
        {
            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"A(z) {offsideTeam} egyik játékosa feledkezett lesen, szól is a bíró sípja.",
                    _ => $"A(z) {offsideTeam} egyik játékosa feledkezett lesen, szól is a bíró sípja."
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.Offside,
            };

            AddComment(Comment);
        }

        // Szabálytalanság

        public void GenerateFault(int matchId, List<MatchPlayer> hometeam, List<MatchPlayer> awayteam)
        {
                var match = _context.Match.FirstOrDefault(x => x.MatchId == matchId);
                var matchFact = _context.MatchFact.FirstOrDefault(x => x.MatchId == matchId);
                var homePlayers = _context.MatchPlayer.Where(x => x.MatchId == matchId && x.TeamId == match.HomeTeamId && x.InGame == true).ToList();
                var awayPlayers = _context.MatchPlayer.Where(x => x.MatchId == matchId && x.TeamId == match.AwayTeamId && x.InGame == true).ToList();

                var rand = new Random();

                int homeFauls = matchFact.HomeFouls - matchFact.HomeYellowCards - matchFact.HomeRedCards;                                                  // összes - sárga - piros
                int awayFouls = matchFact.AwayFouls - matchFact.AwayYellowCards - matchFact.AwayRedCards;

                for (int i = 0; i < homeFauls; i++)
                {
                    int minute = rand.Next(0, 90);
                    int category = rand.Next(0, 15);
                    int playerId = homePlayers.ElementAt(rand.Next(0, homePlayers.Count)).PlayerId;
                    string player = _context.Player.Where(x => x.PlayerId == playerId).Select(x => x.NickName).FirstOrDefault();
           
                    Fault Fault = new Fault()
                    {
                        MatchId = matchId,
                        TeamId = match.HomeTeamId,
                        Cardtype = Cardtype.None,
                        Minute = minute,
                        PlayerId = playerId
                    };

                    _context.Add(Fault);
                    _context.SaveChanges();

                    FaultComment(matchId, minute, category, player);
                }

                for (int i = 0; i < awayFouls; i++)
                {
                    int minute = rand.Next(0, 90);
                    int category = rand.Next(0, 15);
                    int playerId = awayPlayers.ElementAt(rand.Next(0, homePlayers.Count)).PlayerId;
                    string player = _context.Player.Where(x => x.PlayerId == playerId).Select(x => x.NickName).FirstOrDefault();

                    Fault Fault = new Fault()
                    {
                        MatchId = matchId,
                        TeamId = match.AwayTeamId,
                        Cardtype = Cardtype.None,
                        Minute = minute,
                        PlayerId = playerId,
                    };

                    _context.Add(Fault);
                    _context.SaveChanges();

                    FaultComment(matchId, minute, category, player);
                }

                GenerateYellowCard(matchId, match.HomeTeamId, matchFact.HomeYellowCards, homePlayers);                                                                   // sárgalap
                GenerateYellowCard(matchId, match.AwayTeamId, matchFact.AwayYellowCards, awayPlayers);
                GenerateRedCard(matchId, match.HomeTeamId, matchFact.HomeRedCards, homePlayers);                                                                         // piroslap
                GenerateRedCard(matchId, match.AwayTeamId, matchFact.AwayRedCards, awayPlayers);
        }

        // Szabálytalanság

        public void FaultComment(int matchId, int minute, int category, string abusePlayer)
        {
            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"{abusePlayer}  túlzottan igyekezett, sietsége támadó szabálytalanságot eredményezett.",
                    2 => $"A bíró megállította meg a játékot. {abusePlayer}  szabálytalanságát kellett lefújnia, aki túl agresszíven próbálta megszerezni a labdát.",
                    3 => $"{abusePlayer}  kapja a figyelmeztetést ellenfele lerántásáért. Szabadrúgással jöhet a(z) ellenfél.",
                    4 => $"A bíró fújt a sípjába, {abusePlayer}  követett el támadószabálytalanságot. Jó döntés volt.",
                    5 => $"{abusePlayer}  volt szabálytalan a labda megszerzésénél. A bíró látta mindezt és a sípjába fújt.",
                    6 => $"{abusePlayer}  a labda helyett az ellenfél lábát rúgta el. A bíró lefújta a szabálytalanságot.",
                    7 => $"A bíró fújt a sípjába azt követően, hogy {abusePlayer}  lerántotta ellenfelét.",
                    8 => $"A bíró a sípjába fújt. {abusePlayer}  rántotta le ellenfelét igen durván, az ellenfél szabadrúgással folytatódik a játék. Ígéretes helyzet.",
                    9 => $"{abusePlayer}  rossz ütemben lépett oda ellenfelének, a játékvezető észlelte a szabálytalanságot.",
                    10 => $"{abusePlayer}  meg kellett volna, hogy őrizze hidegvérét, de nem tette! Durván lépett oda ellenfelének, a bíró azonnal sípolt is.",
                    11 => $"{abusePlayer}  húzta vissza ellenfelét mezénél fogva, de ezt jól látta a bíró is, meg is állítja a játékot.",
                    12 => $"{abusePlayer}  volt durva, a bíró le is fújta a szabálytalanságot.",
                    13 => $"Veszélyes játék volt, {abusePlayer}  követte el, a bíró fújt a sípjába. Újabb szabadrúgáshoz jutott a(z) ellenfél.",
                    14 => $"Támadó-szabálytalanságot követett el {abusePlayer} , a bíró mindent látott, a sípjába is fújt.",
                    15 => $"{abusePlayer}  támadó szabálytalanságot követ el.",
                    _ => $"{abusePlayer}  túlzottan igyekezett, sietsége támadó szabálytalanságot eredményezett."
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.Fault,
            };

            AddComment(Comment);
        }

        // Sárgalap

        public void GenerateYellowCard(int matchId, int teamId, int yellowCard, List<MatchPlayer> players)
        {
            var rand = new Random();

            for (int i = 0; i < yellowCard; i++)
            {
                int minute = rand.Next(0, 90);
                int playerId = players.ElementAt(rand.Next(0, players.Count)).PlayerId;
                string player = _context.Player.Where(x => x.PlayerId == playerId).Select(x => x.NickName).FirstOrDefault();
                int category = rand.Next(0, 2);

                Fault YellowCard = new Fault()
                {
                    MatchId = matchId,
                    PlayerId = playerId,
                    Minute = minute,
                    TeamId = teamId,
                    Cardtype = Cardtype.Yellow,
                };

                _context.Add(YellowCard);
                _context.SaveChanges();

                YellowCardComment(matchId, minute, category, player);
            }
        }

        // Sárga lap

        public void YellowCardComment(int matchId, int minute, int category, string abusePlayer)
        {
            Comment Comment = new Comment()
            {
                Description = category switch
                {
                    1 => $"Egyértelmű sárga lap. {abusePlayer}  neve bekerül a mérkőzés jegyzőkönyvébe.",
                    2 => $"{abusePlayer}  kap sárga lapot és ezért haragosan néz a játékvezetőre. Valószínűleg túl szigorúnak találja az ítéletet.",
                    _ => $"Egyértelmű sárga lap. {abusePlayer}  neve bekerül a mérkőzés jegyzőkönyvébe.",
                },
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.YellowCard,
            };

            AddComment(Comment);
        }

        // Piroslap

        public void GenerateRedCard(int matchId, int teamId, int redCard, List<MatchPlayer> players)
        {
            var rand = new Random();

            for (int i = 0; i < redCard; i++)
            {
                int minute = rand.Next(0, 90);
                int playerId = players.ElementAt(rand.Next(0, players.Count)).PlayerId;
                string player = _context.Player.Where(x => x.PlayerId == playerId).Select(x => x.NickName).FirstOrDefault();

                Fault RedCard = new Fault()
                {
                    MatchId = matchId,
                    PlayerId = playerId,
                    Minute = minute,
                    TeamId = teamId,
                    Cardtype = Cardtype.Red,
                };

                _context.Add(RedCard);
                _context.SaveChanges();

                RedCardComment(matchId, minute, player);
            }
        }

        // Piros lap
        // kiemelt esemény

        public void RedCardComment(int matchId, int minute, string abusePlayer)
        {
            Comment Comment = new Comment()
            {
                Description = $"Egyértelmű piros lap. {abusePlayer}  neve bekerül a mérkőzés jegyzőkönyvébe.",
                MatchId = matchId,
                Minute = minute,
                Type = CommentType.RedCard,
            };

            AddComment(Comment);
        }

        public async Task<SimulationHeadLineDto> SimulationHeadLineResponse(CreateMatchSimulationRequest createMatchSimulationRequest)
        {
            return await (from t in _context.Team
                          from m in _context.Match
                          where m.MatchId == createMatchSimulationRequest.MatchId
                          select new SimulationHeadLineDto
                          {
                              MatchId = m.MatchId,
                              Away = _context.Team.Where(x => x.TeamId == m.HomeTeamId).Select(x => x.Name).FirstOrDefault(),
                              Home = _context.Team.Where(x => x.TeamId == m.AwayTeamId).Select(x => x.Name).FirstOrDefault(),
                              AwayLogo = $"assets/logo/{_context.Team.Where(x => x.TeamId == m.HomeTeamId).Select(x => x.Name).FirstOrDefault()}",
                              HomeLogo = $"assets/logo/{_context.Team.Where(x => x.TeamId == m.HomeTeamId).Select(x => x.Name).FirstOrDefault()}",
                          }).FirstOrDefaultAsync();
        }

        public async Task GenerateSeason(int leagueId)
        {
            var league = _context.League.FirstOrDefault(x => x.LeagueId == leagueId);
            var season = _context.Season.Where(x => x.LeagueId == league.LeagueId).FirstOrDefault();
            var rounds = _context.Round.Where(x => x.SeasonId == season.SeasonId).ToList();

            foreach (var round in rounds)
            {
                var match_list = _context.Match.Where(x => x.RoundId == round.RoundId).Select(x => x.MatchId).ToList();

                foreach (var match in match_list)
                {
                    var createMatchSimulationRequest = new CreateMatchSimulationRequest()
                    {
                        MatchId = match
                    };

                    await BeginMatchSimulationRequest(createMatchSimulationRequest);
                }

            }

        }
    }
}
