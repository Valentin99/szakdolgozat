﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.TeamDto.HeadLineDto
{
    public class TeamHeadlineDto
    {
        public string Title { get; set; }
        public string Logo { get; set; }
    }
}
