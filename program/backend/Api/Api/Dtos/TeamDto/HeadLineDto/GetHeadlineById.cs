﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.TeamDto.HeadLineDto
{
    public class GetHeadlineById
    {
        public int TeamId { get; set; }
    }
}
