﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.TeamDto.TransferDto
{
    public class GetTeamTransfersById
    {
        public int TeamId { get; set; }
    }
}
