﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.TeamDto.TransferDto
{
    public class TeamTransferDto
    {
        public int PlayerId { get; set; }           // elsődleges kulcs
        public string Name { get; set; }            // játékos neve
        public string Movement { get; set; }        // átigazolás
        public string From { get; set; }            // honnan
        public string To { get; set; }              // hová
    }
}
