﻿namespace Api.Dtos.TeamDto.PlayerDto
{
    public class GetTeamPlayersByIdRequest
    {
        public int TeamId { get; set; }
    }
}
