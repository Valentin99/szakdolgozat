﻿namespace Api.Dtos.TeamDto.PlayerDto
{
    public class TeamPlayerDto
    {
        public int PlayerId { get; set; }       
        public int TeamId { get; set; }
        public string Name { get; set; }            // játékos neve
        public int YerseyNumber { get; set; }       // mez szám
        public int Age { get; set; }                // kor
        public int Apps { get; set; }               // mérközések száma
        public string Position { get; set; }        // pozició
        public int Goal { get; set; }               // gólok számának összege
        public int YellowCard { get; set; }         // sárga lapok számának összege
        public int RedCard { get; set; }            // piros lapok számának összege
    }
}
