﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.TeamDto.TeamResultDto
{
    public class TeamResultDto
    {
        public int TeamId { get; set; }             // elsődleges kulcs
        public int MatchId { get; set; }            // mérközés id
        public string HomeTeam { get; set; }        // hazai csapat
        public string AwayTeam { get; set; }        // vendég csapat
        public string Fulltime { get; set; }        // végeredmény
    }
}
