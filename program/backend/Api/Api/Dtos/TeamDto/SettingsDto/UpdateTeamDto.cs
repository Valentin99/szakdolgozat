﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.TeamDto.SettingsDto
{
    public class UpdateTeamDto
    {
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Stadium { get; set; }
        public string Formation { get; set; }
    }
}
