﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.MatchDto.MatchStatistics
{
    public class GetMatchStatisticsByIdRequest
    {
        public int MatchId { get; set; }
        public int Minute { get; set; }
    }
}
