﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.MatchDto.MatchStatistics
{

    //const ELEMENT_DATA: MatchStatistics[] = [
    //  { homeTeam: 38, type: 'Labdabirtoklás', awayTeam: 62 },
    //  { homeTeam: 4, type: 'Kapura lövés', awayTeam: 7 },
    //  { homeTeam: 2, type: 'Kaput eltaláló lövés', awayTeam: 5 },
    //  { homeTeam: 2, type: 'Blokkolt lövés', awayTeam: 2 },
    //  { homeTeam: 5, type: 'Szabadrugás', awayTeam: 3 },
    //  { homeTeam: 1, type: 'Szöglet', awayTeam: 9 },
    //  { homeTeam: 1, type: 'Les', awayTeam: 0 },
    //  { homeTeam: 5, type: 'Védés', awayTeam: 3 },
    //  { homeTeam: 6, type: 'Szabálytalanság', awayTeam: 3 },
    //  { homeTeam: 3, type: 'Sárga lap', awayTeam: 1 },
    //  { homeTeam: 0, type: 'Piros lap', awayTeam: 0 },
    //  { homeTeam: 382, type: 'Összes passz', awayTeam: 411 },
    //];

    public class MatchStatisticsDto
    {
        public int HomeTeam { get; set; }
        public int AwayTeam { get; set; }
        public string Type { get; set; }
    }
}
