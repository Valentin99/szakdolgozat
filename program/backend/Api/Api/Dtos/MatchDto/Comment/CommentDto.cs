﻿namespace Api.Dtos.MatchDto.Comment
{
    /// <summary>
    /// Cachelt mérközés kommentár response
    /// </summary>
    public class CommentDto
    {
        public string Description { get; set; }         // leírás
        public int Minute { get; set; }                 // perc
    }
}
