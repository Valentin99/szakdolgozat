﻿namespace Api.Dtos.MatchDto.Comment
{
    /// <summary>
    /// Request mérközés kommentárjainak lekérése egy adott intervallumban
    /// </summary>
    public class GetCommentsByMatchIdRequest
    {
        public int MatchId { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
    }
}
