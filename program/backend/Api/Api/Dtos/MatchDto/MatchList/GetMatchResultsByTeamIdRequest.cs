﻿namespace Api.Dtos.MatchDto.MatchList
{
    /// <summary>
    /// Get match results by team id request
    /// </summary>
    public class GetMatchResultsByTeamIdRequest
    {
        public int TeamId { get; set; }
    }
}
