﻿namespace Api.Dtos.MatchDto.MatchList
{
    /// <summary>
    /// get match results by team id response
    /// </summary>
    public class MatchDto
    {
        public int MatchId { get; set; }
        public string HomeTeam { get; set; }                // hazai csapat
        public string AwayTeam { get; set; }                // vendég csapat
        public string FulTimeResult { get; set; }           // végeredmény
        public string HalfTimeResult { get; set; }          // félidő
    }
}
