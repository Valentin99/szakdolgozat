﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.MatchDto.MatchReport
{
    public class GetMatchReportByIdRequest
    {
        public int MatchId { get; set; }
        public int Minute { get; set; }
    }
}
