﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.MatchDto.MatchReport
{
    /// <summary>
    /// Mérközés összegző felületen lévő object
    /// </summary>
    public class MatchReportDto
    {
        public int PlayerId { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int Minute { get; set; }
    }
}
