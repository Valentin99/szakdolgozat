﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.MatchDto.LineUp
{
    /// <summary>
    /// 2 csapat formációja egy adott mérközésen
    /// </summary>
    public class FormationDto
    {
        public string HomeTeamFormation { get; set; }
        public string AwayTeamFormation { get; set; }
    }
}
