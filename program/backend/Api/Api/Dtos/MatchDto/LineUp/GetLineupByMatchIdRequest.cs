﻿namespace Api.Dtos.MatchDto.LineUp
{
    /// <summary>
    /// Mérközés id alapján request kezdőcsapat lekérdezésére
    /// </summary>
    public class GetLineupByMatchIdRequest
    {
        public int MatchId { get; set; }
    }
}
