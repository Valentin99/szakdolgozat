﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.MatchDto.LineUp
{
    public class LineupDto
    {
        public int PlayerId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }        // home11, away11, homesub, awaysub
    }
}
