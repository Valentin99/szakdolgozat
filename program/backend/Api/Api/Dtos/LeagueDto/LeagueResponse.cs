﻿namespace Api.Dtos.LeagueDto
{
    /// <summary>
    /// Example
    /// </summary>
    public class LeagueResponse
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
