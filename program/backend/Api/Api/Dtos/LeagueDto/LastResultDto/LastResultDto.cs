﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.LeagueDto.LastResultDto
{
    /// <summary>
    /// Utolsó szimulált forduló kiértékelése
    /// </summary>
    public class LastResultDto
    {
        public int MatchId { get; set; }            // elsődleges kulcs
        public string HomeTeam { get; set; }        // hazai csapat
        public string AwayTeam { get; set; }        // vendég csapat
        public string Fulltime { get; set; }        // végeredmény
    }
}
