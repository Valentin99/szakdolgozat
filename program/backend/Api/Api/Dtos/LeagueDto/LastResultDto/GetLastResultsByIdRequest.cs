﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.LeagueDto.LastResultDto
{
    public class GetLastResultsByIdRequest
    {
        public int RoundId { get; set; }
    }
}
