﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.LeagueDto.CleansheetDto
{
    /// <summary>
    /// Cleansheet lista
    /// </summary>
    public class CleansheetDto
    {
        public int PlayerId { get; set; }               // elsődleges kulcs
        public int Position { get; set; }               // helyezés
        public string PlayerName { get; set; }          // játékos neve
        public int Cleansheet { get; set; }             // gól nélküli mérközések száma
    }
}
