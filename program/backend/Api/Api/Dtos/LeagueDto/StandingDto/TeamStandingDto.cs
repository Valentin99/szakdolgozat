﻿namespace Api.Dtos.LeagueDto.StandingDto
{
    /// <summary>
    /// Bajnokság tabella
    /// </summary>
    public class TeamStandingDto
    {
        public int TeamId { get; set; }                 // elsődleges kulcs
        public int Position { get; set; }               // helyezés
        public string TeamName { get; set; }            // csapat neve
        public string Logo { get; set; }                // csapat logo
        public int PlayedGames { get; set; }            // lejátszott mérközések száma
        public int Win { get; set; }                    // győzelem
        public int Draw { get; set; }                   // döntetlen
        public int Defeat { get; set; }                 // vereség
        public int Point { get; set; }                  // pontszám
    }
}
