﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.LeagueDto.CardDto
{
    /// <summary>
    /// Piros lap lista
    /// </summary>
    public class RedcardDto
    {
        public int PlayerId { get; set; }               // elsődleges kulcs
        public int Position { get; set; }               // helyezés
        public string PlayerName { get; set; }          // játékos neve
        public int RedCard { get; set; }                // piros lapok száma
    }
}
