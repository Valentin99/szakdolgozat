﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.LeagueDto.CardDto
{
    /// <summary>
    /// Sárga lap lista
    /// </summary>
    public class YellowcardDto
    {
        public int PlayerId { get; set; }               // elsődleges kulcs
        public int Position { get; set; }               // helyezés
        public string PlayerName { get; set; }          // játékos neve
        public int YellowCard { get; set; }             // sárga lapok száma
    }
}
