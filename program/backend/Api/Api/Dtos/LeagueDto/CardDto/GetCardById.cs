﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.LeagueDto.CardDto
{
    public class GetCardById
    {
        public int LeagueId { get; set; }
        public int SeasonId { get; set; }
        public int RoundId { get; set; }
    }
}
