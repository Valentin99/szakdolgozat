﻿namespace Api.Dtos.LeagueDto
{
    /// <summary>
    /// Example
    /// </summary>
    public class GetLeagueByIdRequest
    {
        public int Id { get; set; }
    }
}
