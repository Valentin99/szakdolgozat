﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.LeagueDto.GoalscorerlistDto
{
    /// <summary>
    /// Góllövőlista
    /// </summary>
    public class GoalscorerDto
    {
        public int PlayerId { get; set; }               // elsődleges kulcs
        public int Position { get; set; }               // helyezés
        public string PlayerName { get; set; }          // játékos neve
        public int Goal { get; set; }                   // gólok száma
    }
}
