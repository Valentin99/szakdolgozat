﻿namespace Api.Dtos.SimulationDto.RoundDto
{
    public class CreateRoundSimulationRequest
    {
        public int RoundId { get; set; }
    }
}
