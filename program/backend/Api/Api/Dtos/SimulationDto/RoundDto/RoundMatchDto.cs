﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.SimulationDto.RoundDto
{
    public class RoundMatchDto
    {
        public int MatchId { get; set; }
        public string Status { get; set; }
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public string Halftime { get; set; }
        public string Fulltime { get; set; }
    }
}
