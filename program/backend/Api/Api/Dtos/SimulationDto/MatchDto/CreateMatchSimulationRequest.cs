﻿namespace Api.Dtos.SimulationDto.MatchDto
{
    public class CreateMatchSimulationRequest
    {
        public int MatchId { get; set; }
    }
}
