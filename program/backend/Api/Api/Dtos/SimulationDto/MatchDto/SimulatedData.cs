﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.SimulationDto.MatchDto
{
    public class SimulatedData
    {
        public int MatchId { get; set; }
        public int BlockedShot { get; set; }
        public int BlockedShotComment { get; set; }
        public int SavedShot { get; set; }
        public int SavedShotComment { get; set; }
        public int Goal { get; set; }
        public int AssistedGoal { get; set; }
        public int GoalComment { get; set; }
        public int Freekick { get; set; }
        public int FreekickComment { get; set; }
        public int Corner { get; set; }
        public int CornerComment { get; set; }
        public int Fault { get; set; }
        public int YellowCard { get; set; }
        public int RedCard { get; set; }
        public int Sub { get; set; }
        public int SubComment { get; set; }
        public int Comment { get; set; }
    }
}
