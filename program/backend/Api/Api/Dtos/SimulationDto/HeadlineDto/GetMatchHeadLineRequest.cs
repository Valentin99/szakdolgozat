﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.SimulationDto.HeadlineDto
{
    public class GetMatchHeadLineRequest
    {
        public int MatchId { get; set; }
    }
}
