﻿namespace Api.Dtos.SimulationDto.HeadlineDto
{
    public class SimulationPageHeadlineDto
    {
        public string Title { get; set; }
    }
}
