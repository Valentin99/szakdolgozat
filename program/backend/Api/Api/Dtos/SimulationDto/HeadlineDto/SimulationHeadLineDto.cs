﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.SimulationDto.HeadlineDto
{
    public class SimulationHeadLineDto
    {
        public int MatchId { get; set; }                // mérközés id
        public string Home { get; set; }                // hazai csapat neve
        public string Away { get; set; }                // vendég csapat neve
        public string HomeLogo { get; set; }            // hazai csapat logo képe
        public string AwayLogo { get; set; }            // vendég csapat logo képe
    }
}
