﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.SettingsDto
{
    public class PlayerDto
    {
        public int PlayerId { get; set; }
        public int TeamId { get; set; }                                             // idegen kulcs
        public string Name { get; set; }                                            // név
        public string NickName { get; set; }                                        // becenév
        public int Age { get; set; }                                                // kor
        public string Birthday { get; set; }                                        // születésnap
        public string Nationality { get; set; }                                     // nemzetiség
        public string CurrentTeam { get; set; }                                     // jelenlegi csapat
        public int MarketValue { get; set; }                                        // piaci érték
        public string TeamPosition { get; set; }                                    // pozíció
        public int TeamYerseyNumber { get; set; }                                   // mez szám
    }
}
