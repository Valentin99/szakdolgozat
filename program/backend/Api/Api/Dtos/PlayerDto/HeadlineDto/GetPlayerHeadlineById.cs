﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.HeadlineDto
{
    public class GetPlayerHeadlineById
    {
        public int PlayerId { get; set; }
    }
}
