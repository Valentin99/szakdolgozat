﻿namespace Api.Dtos.PlayerDto.HeadlineDto
{
    /// <summary>
    /// ez marad
    /// </summary>
    public class PlayerHeadLineDto
    {
        public string Name { get; set; }            // Játékos neve
        public string Position { get; set; }        // Pozició (csapat neve)
        public string Age { get; set; }             // Kor: xy (yyyy.MM.dd)
        public string teamLogo { get; set; }            // csapat logo
    }
}
