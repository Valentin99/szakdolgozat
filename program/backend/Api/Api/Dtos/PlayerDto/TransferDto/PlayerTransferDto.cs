﻿namespace Api.Dtos.PlayerDto.TransferDto
{
    /// <summary>
    /// ez marad
    /// </summary>
    public class PlayerTransferDto
    {
        public int PlayerId { get; set; }           // elsődleges kulcs
        public string Name { get; set; }            // játékos neve
        public string Season { get; set; }          // szezon
        public string From { get; set; }            // honnan
        public string To { get; set; }              // hová
        public string Type { get; set; }            // hogyan
    }
}
