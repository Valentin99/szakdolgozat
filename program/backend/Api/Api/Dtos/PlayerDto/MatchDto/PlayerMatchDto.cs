﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.MatchDto
{
    public class PlayerMatchDto
    {
        public int MatchId { get; set; }                
        public int PlayerId { get; set; }
        public int PlayedMinutes { get; set; }          // lejátszott percek száma
        public string GameWeek { get; set; }            // mérközés dátuma
        public string League { get; set; }              // bajnokság
        public string HomeTeam { get; set; }            // hazai csapat
        public string AwayTeam { get; set; }            // vendég csasat
        public string Fulltime { get; set; }            // végeredmény
        public int Goal { get; set; }                   // játékos góljainak a száma
        public int YellowCard { get; set; }             // játékos sárga lapjainak a száma
        public int RedCard { get; set; }                // játékos piros lapjainak a száma
    }
}
