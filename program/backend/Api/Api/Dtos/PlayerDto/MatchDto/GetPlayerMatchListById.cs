﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.MatchDto
{
    public class GetPlayerMatchListById
    {
        public int PlayerId { get; set; }
    }
}
