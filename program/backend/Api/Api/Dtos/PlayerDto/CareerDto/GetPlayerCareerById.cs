﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.CareerDto
{
    public class GetPlayerCareerById
    {
        public int PlayerId { get; set; }
    }
}
