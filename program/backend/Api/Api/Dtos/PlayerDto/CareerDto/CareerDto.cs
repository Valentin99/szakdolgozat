﻿namespace Api.Dtos.PlayerDto.CareerDto
{
    public class CareerDto
    {
        public int PlayerId { get; set; }           // elsődleges kulcs
        public string Season { get; set; }          // szezon
        public string League { get; set; }          // bajnokság
        public string Team { get; set; }            // csapat
        public int Apps { get; set; }               // mérközések száma
        public int Goal { get; set; }               // gólok számának összege
        public int YellowCard { get; set; }         // sárga lapok számának összege
        public int RedCard { get; set; }            // piros lapok számának összege
    }
}
