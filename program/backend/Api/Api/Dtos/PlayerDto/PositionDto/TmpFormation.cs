﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.PositionDto
{
    public class TmpFormation
    {
        public string Team { get; set; }
        public int Goalkeeper { get; set; }
        public int Defender { get; set; }
        public int Midfielder { get; set; }
        public int Attacker { get; set; }
        public int Striker { get; set; }
    }
}
