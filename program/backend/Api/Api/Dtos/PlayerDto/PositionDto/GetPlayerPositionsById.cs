﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.PositionDto
{
    public class GetPlayerPositionsById
    {
        public int MatchId { get; set; }
    }
}
