﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Dtos.PlayerDto.PositionDto
{
    public class PositionDto
    {
        public string Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
    }
}
