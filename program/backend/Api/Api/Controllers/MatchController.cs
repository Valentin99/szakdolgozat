﻿using Api.Dtos.MatchDto.Comment;
using Api.Dtos.MatchDto.LineUp;
using Api.Dtos.MatchDto.MatchReport;
using Api.Dtos.MatchDto.MatchStatistics;
using Api.Dtos.PlayerDto.PositionDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using Api.Repository.MatchRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{

    /// <summary>
    /// Mérközés kontroller, a leszimulált mérközéssel kapcsolatos adatokat adja vissza
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly IMatchService _service;

        public MatchController(IMatchService service)
        {
            _service = service;
        }

        /// <summary>
        /// Játékosok pozicióinak a koordinátáit kérdezi le ez a metódus mérközés id alapján, hogy hol szerepelnek a felállásban
        /// </summary>
        /// <param name="getPlayerPositionsById"></param>
        /// <returns></returns>

        [HttpPost("position")]
        public async Task<List<PositionDto>> GetPlayerPositions(GetPlayerPositionsById getPlayerPositionsById)
        {
            return await _service.PlayerPosition(getPlayerPositionsById);
        }

        /// <summary>
        /// Mérközés címsorát adja vissza, 2 csapat logoja és neve
        /// </summary>
        /// <param name="getMatchHeadLineRequest"></param>
        /// <returns></returns>

        [HttpPost("headline")]
        public async Task<SimulationHeadLineDto> SimulationHeadLine(GetMatchHeadLineRequest getMatchHeadLineRequest)
        {
            return await _service.SimulationHeadLine(getMatchHeadLineRequest);
        }

        /// <summary>
        /// Csapatok felállás listáját adja vissza, akik részt vettek a mérközésen
        /// </summary>
        /// <param name="getLineupRequest"></param>
        /// <returns></returns>

        [HttpPost("lineup")]
        public async Task<List<LineupDto>> Lineup(GetLineupByMatchIdRequest getLineupRequest)
        {
            return await _service.Lineup(getLineupRequest);
        }

        /// <summary>
        /// Mérközés aktuális eredmény állapotát adja vissza egy adott percben
        /// </summary>
        /// <param name="getMatchResultRequest"></param>
        /// <returns></returns>

        [HttpPost("result")]
        public async Task<MatchResultDto> MatchResultResponse(GetMatchResultRequest getMatchResultRequest)
        {
            return await _service.MatchResultResponse(getMatchResultRequest);
        }

        /// <summary>
        /// Mérközés akutális kommentárját kérdezi le gyorsítótárazva a korábbi intervallumban szereplő kommenteket a gyorsítótárból szedi ki és fűzi hozzá
        /// </summary>
        /// <param name="getCommentsByMatchIdRequest"></param>
        /// <returns></returns>

        [HttpPost("comment")]
        public async Task<List<CommentDto>> Comment(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest)
        {
            return await _service.GetCachedComments(getCommentsByMatchIdRequest);
        }

        /// <summary>
        /// Mérközés kommentárjait adja vissza egy aktuális percben gyorsítótárazás nélkül
        /// </summary>
        /// <param name="getCommentsByMatchIdRequest"></param>
        /// <returns></returns>

        [HttpPost("comment/normal")]
        public List<CommentDto> NormalComment(GetCommentsByMatchIdRequest getCommentsByMatchIdRequest)
        {
            return _service.GetNormalComments(getCommentsByMatchIdRequest);
        }

        /// <summary>
        /// Mérközés aktuális statisztikáját adja vissza egy adottt percben
        /// </summary>
        /// <param name="getMatchStatisticsByIdRequest"></param>
        /// <returns></returns>

        [HttpPost("statistics")]
        public List<MatchStatisticsDto> GetMatchStatistics(GetMatchStatisticsByIdRequest getMatchStatisticsByIdRequest)
        {
            return _service.GetMatchStatistics(getMatchStatisticsByIdRequest);
        }

        /// <summary>
        /// A mérközés report a legfontosabb mérközésen történő események listáját adja vissza egy adott percben sárga lap, piros lap, csere, gól
        /// </summary>
        /// <param name="getMatchReportByIdRequest"></param>
        /// <returns></returns>

        [HttpPost("report")]
        public List<MatchReportDto> MatchReport(GetMatchReportByIdRequest getMatchReportByIdRequest)
        {
            return _service.MatchReport(getMatchReportByIdRequest);
        }

    }
}
