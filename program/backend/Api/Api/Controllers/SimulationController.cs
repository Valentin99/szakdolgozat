﻿using Api.Dtos.MatchDto.LineUp;
using Api.Dtos.SimulationDto;
using Api.Dtos.SimulationDto.FaultDto;
using Api.Dtos.SimulationDto.HeadlineDto;
using Api.Dtos.SimulationDto.MatchDto;
using Api.Dtos.SimulationDto.RoundDto;
using Api.Dtos.SimulationDto.ShotDto;
using Api.Repository.SimulationRepository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Controllers
{

    /// <summary>
    /// Szimuláció kontroller, az adatgeneráláshoz felhasznált kontroller, amit ezzel a kontroller segitsével kitudtam generáltatni
    /// utána archiváltam egy .csv formátumban és betöltöttem az adatbázisba
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    public class SimulationController : ControllerBase
    {
        private readonly ISimulationService _service;

        public SimulationController(ISimulationService service)
        {
            _service = service;
        }

        [HttpPost("round")]
        public async Task<List<RoundMatchDto>> CreateRoundSimulationRequest(CreateRoundSimulationRequest createSimulationRequest)
        {
            return await _service.CreateRoundSimulationRequest(createSimulationRequest);
        }

        [HttpPost("page")]
        public async Task<SimulationPageHeadlineDto> SimulationPageHeadlineResponse(CreateRoundSimulationRequest createSimulationRequest)
        {
            return await _service.SimulationPageHeadLineRespone(createSimulationRequest);
        }

        [HttpPost("headline")]
        public async Task<SimulationHeadLineDto> SimulationHeadLineResponse(CreateMatchSimulationRequest createMatchSimulationRequest)
        {
            return await _service.SimulationHeadLineResponse(createMatchSimulationRequest);
        }

        [HttpPost("match")]
        public async Task BeginMatchSimulationRequest(CreateMatchSimulationRequest createMatchSimulationRequest)
        {
            await _service.BeginMatchSimulationRequest(createMatchSimulationRequest);
        }

        [HttpPost("season")]
        public async Task GenerateSeason()
        {
            await _service.GenerateSeason(1);
        }

    }
}
