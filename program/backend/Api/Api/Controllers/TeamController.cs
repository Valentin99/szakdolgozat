﻿using Api.Dtos.MatchDto.MatchList;
using Api.Dtos.TeamDto.HeadLineDto;
using Api.Dtos.TeamDto.PlayerDto;
using Api.Dtos.TeamDto.SettingsDto;
using Api.Dtos.TeamDto.TransferDto;
using Api.Repository.TeamRepository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Controllers
{

    /// <summary>
    /// Csapat kontroller a csapatokkal kapcsolatos adatokat adja vissza elsődleges kulcs alapján
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _service;

        public TeamController(ITeamService service)
        {
            _service = service;
        }

        /// <summary>
        /// Csapat összegző felület címsora
        /// </summary>
        /// <param name="getHeadlineById"></param>
        /// <returns></returns>

        [HttpPost("headline")]
        public async Task<IActionResult> GetTeamHeadlineById(GetHeadlineById getHeadlineById)
        {
            if (getHeadlineById == null)
            {
                return BadRequest();
            }

            var headline = await _service.TeamHeadline(getHeadlineById);

            if (headline == null)
            {
                return NotFound();
            }

            return Ok(headline);
        }

        /// <summary>
        /// Csapat legutobbi eredményeit adja vissza, amin a csapat részt vett
        /// </summary>
        /// <param name="getMatchResultsByTeamIdRequest"></param>
        /// <returns></returns>

        [HttpPost("result")]
        public async Task<IActionResult> GetMatchResultsByTeamId(GetMatchResultsByTeamIdRequest getMatchResultsByTeamIdRequest)
        {
            if (getMatchResultsByTeamIdRequest == null)
            {
                return BadRequest();
            }

            var results = await _service.GetMatchResults(getMatchResultsByTeamIdRequest);

            if (results == null)
            {
                return NotFound();
            }

            return Ok(results);
        }

        /// <summary>
        /// Csapat átigazolási listája egy adott szezonban
        /// </summary>
        /// <param name="getTeamTransfersById"></param>
        /// <returns></returns>

        [HttpPost("transfer")]
        public async Task<IActionResult> GeTransfersByTeamId(GetTeamTransfersById getTeamTransfersById)
        {
            if (getTeamTransfersById == null)
            {
                return BadRequest();
            }

            var transfers = await _service.TeamTransfers(getTeamTransfersById);

            if (transfers == null)
            {
                return NotFound();
            }

            return Ok(transfers);
        }

        /// <summary>
        /// Csapat játékosainak a listája
        /// </summary>
        /// <param name="getTeamPlayersByIdRequest"></param>
        /// <returns></returns>

        [HttpPost("player")]
        public async Task<IActionResult> GetTeamPlayersByTeamId(GetTeamPlayersByIdRequest getTeamPlayersByIdRequest)
        {
            if (getTeamPlayersByIdRequest == null)
            {
                return BadRequest();
            }

            var players = await _service.GetTeamPlayersByTeamId(getTeamPlayersByIdRequest);

            if (players == null)
            {
                return NotFound();
            }

            return Ok(players);
        }


        [HttpPost]
        public void CreateTeam(TeamDto teamDto)
        {
            _service.CreateTeam(teamDto);
        }

        [HttpPut("{id}")]
        public string UpdaTeam(TeamDto teamDto, int id)
        {
            return _service.UpdateTeam(teamDto, id);
        }

        [HttpDelete("{id}")]
        public void DeleteTeam(int id)
        {
            _service.DeleteTeam(id);
        }

        [HttpGet]
        public List<TeamDto> GetTeams()
        {
            return _service.GetTeams();
        }

    }
}
