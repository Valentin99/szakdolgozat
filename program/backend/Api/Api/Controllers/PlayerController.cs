﻿using Api.Dtos.PlayerDto.CareerDto;
using Api.Dtos.PlayerDto.HeadlineDto;
using Api.Dtos.PlayerDto.MatchDto;
using Api.Dtos.PlayerDto.PositionDto;
using Api.Dtos.TeamDto.SettingsDto;
using Api.Models;
using Api.Repository.PlayerRepository;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Api.Dtos.PlayerDto.SettingsDto;

namespace Api.Controllers
{
    /// <summary>
    /// Játékos kontroller a játékossal kapcsolatos adatokat adja vissza elsődleges kulcs alapján
    /// </summary>

    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        private readonly IPlayerService _service;

        public PlayerController(IPlayerService service)
        {
            _service = service;
        }

        /// <summary>
        /// Játékos összegző felület címsorát adja vissza
        /// </summary>
        /// <param name="getPlayerHeadlineById"></param>
        /// <returns></returns>

        [HttpPost("headline")]
        public async Task<PlayerHeadLineDto> GetPlayerHeadlineById(GetPlayerHeadlineById getPlayerHeadlineById)
        {
            return await _service.GetPlayerHeadlineById(getPlayerHeadlineById);
        }

        /// <summary>
        /// Játékos karrierjének összegző felületét adja vissza, hogy milyen teljesítményt nyujtott szezononkénti bontásban
        /// </summary>
        /// <param name="getPlayerCareerById"></param>
        /// <returns></returns>

        [HttpPost("career")]
        public async Task<List<CareerDto>> GetPlayerCareerById(GetPlayerCareerById getPlayerCareerById)
        {
            return await _service.GetPlayerCareerById(getPlayerCareerById);
        }

        /// <summary>
        /// Azoknak a mérközéseknek a listáját adja vissza, amin a játékos részt vett
        /// </summary>
        /// <param name="getPlayerMatchListById"></param>
        /// <returns></returns>

        [HttpPost("list")]
        public async Task<List<PlayerMatchDto>> GetPlayerMatch(GetPlayerMatchListById getPlayerMatchListById)
        {
            return await _service.PlayerMatch(getPlayerMatchListById);
        }

        [HttpPost]
        public void CreatePlayer(PlayerDto playerDto)
        {
            _service.CreatePlayer(playerDto);
        }

        [HttpPut("{id}")]
        public void UpdatePlayer(PlayerDto playerDto, int id)
        {
            _service.UpdatePlayer(playerDto, id);
        }

        [HttpDelete("{id}")]
        public void DeletePlayer(int id)
        {
            _service.DeletePlayer(id);
        }

        [HttpGet]
        public List<PlayerDto> GetPlayers()
        {
            return _service.GetPlayers();
        }

    }
}
