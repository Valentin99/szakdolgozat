﻿using Api.Dtos.LeagueDto;
using Api.Dtos.LeagueDto.CardDto;
using Api.Dtos.LeagueDto.CleansheetDto;
using Api.Dtos.LeagueDto.GoalscorerlistDto;
using Api.Dtos.LeagueDto.LastResultDto;
using Api.Dtos.LeagueDto.StandingDto;
using Api.Dtos.SimulationDto.RoundDto;
using Api.Models;
using Api.Repository.LeagueRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Controllers
{

    /// <summary>
    /// Bajnokság kontroller, a bajnokság összegző felületet kezeli.
    /// </summary>
  
    [Route("api/[controller]")]
    [ApiController]
    public class LeagueController : ControllerBase
    {
        private readonly ILeagueService _service;

        public LeagueController(ILeagueService service)
        {
            _service = service;
        }

        [HttpGet]
        public string Server()
        {
            return "A backend elindult!";
        }

        /// <summary>
        /// Bajnokság tabella
        /// </summary>
        /// <param name="getLeagueStandingsRequest"></param>
        /// <returns></returns>


        [HttpPost("standing")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LeagueStanding(GetLeagueStandingsRequest getLeagueStandingsRequest)
        {
            if (getLeagueStandingsRequest == null)
            {
                return BadRequest();
            }

            var standings = await _service.Standing(getLeagueStandingsRequest);

            return Ok(standings);
        }

        /// <summary>
        /// Bajnokság összegző felület címsorát adja vissza
        /// </summary>
        /// <param name="createRoundSimulationRequest"></param>
        /// <returns></returns>

        [HttpPost("headline")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> HeadLine(CreateRoundSimulationRequest createRoundSimulationRequest)
        {
             var headline = await _service.SimulationPageHeadLineRespone(createRoundSimulationRequest);

            return Ok(headline);
        }   

        /// <summary>
        /// Az éppen aktuális lekérdezett bajnokság forduló szimulált mérközéseinek a listáját adja vissza
        /// </summary>
        /// <param name="getLastResultsByIdRequest"></param>
        /// <returns></returns>

        [HttpPost("result")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<LastResultDto>> Result(GetLastResultsByIdRequest getLastResultsByIdRequest)
        {
            return await _service.Result(getLastResultsByIdRequest);
        }

        /// <summary>
        /// Góllövő lista
        /// </summary>
        /// <param name="getGoalscorerListById"></param>
        /// <returns></returns>

        [HttpPost("goalscorer")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<GoalscorerDto>> Goalscorer(GetGoalscorerListById getGoalscorerListById)
        {
            return await _service.GoalscorerList(getGoalscorerListById);
        }

        /// <summary>
        /// Sárga lap lista
        /// </summary>
        /// <param name="getCardById"></param>
        /// <returns></returns>

        [HttpPost("yellowcard")]
        public async Task<List<YellowcardDto>> YellowCard(GetCardById getCardById)
        {
            return await _service.YellowCard(getCardById);
        }

        /// <summary>
        /// piros lap lista
        /// </summary>
        /// <param name="getCardById"></param>
        /// <returns></returns>

        [HttpPost("redcard")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<List<RedcardDto>> RedCard(GetCardById getCardById)
        {
            return await _service.RedCard(getCardById);
        }

        // Unit test example

        [HttpGet("leagues")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetLeagues()
        {
            var leagues = await _service.GetLeagues();

            if (leagues == null)
            {
                return NotFound();
            }

            return Ok(leagues);
        }

        [HttpPost("getbyid")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetLeagueById(GetLeagueByIdRequest getLeagueByIdRequest)
        {

            if (getLeagueByIdRequest == null)
            {
                return BadRequest();
            }

            var league = await _service.GetLeagueById(getLeagueByIdRequest);

            if (league == null)
            {
                return NotFound();
            }

            return Ok(league);
        }

    }
}
