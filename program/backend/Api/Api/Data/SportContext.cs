﻿using Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Api.Data
{
    public class SportContext : DbContext
    {
        public DbSet<Comment> Comment { get; set; }
        public DbSet<League> League { get; set; }
        public DbSet<Match> Match { get; set; }
        public DbSet<MatchFact> MatchFact { get; set; }
        public DbSet<MatchPlayer> MatchPlayer { get; set; }
        public DbSet<Player> Player { get; set; }
        public DbSet<Season> Season { get; set; }
        public DbSet<Round> Round { get; set; }
        public DbSet<Shot> Shot { get; set; }
        public DbSet<Fault> Fault { get; set; }
        public DbSet<Team> Team { get; set; }
        public DbSet<Possesion> Possesion { get; set; }
        public DbSet<Transfer> Transfer { get; set; }

        public SportContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //Database Relations

            modelBuilder.Entity<Season>()
            .HasOne(x => x.League)
            .WithMany(x => x.Season);

            modelBuilder.Entity<Transfer>()
            .HasOne(x => x.Season)
            .WithMany(x => x.Transfer);

            modelBuilder.Entity<Round>()
            .HasOne(x => x.Season)
            .WithMany(x => x.Round);

            modelBuilder.Entity<Team>()
            .HasOne(x => x.Season)
            .WithMany(x => x.Team);

            modelBuilder.Entity<Player>()
            .HasOne(x => x.Team)
            .WithMany(x => x.Player);

            modelBuilder.Entity<Match>()
            .HasOne(x => x.Round)
            .WithMany(x => x.Match);

            modelBuilder.Entity<MatchPlayer>()
            .HasOne(x => x.Match)
            .WithMany(x => x.MatchPlayer)
            .HasForeignKey(x => x.MatchId)
            .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<MatchPlayer>()
            .HasOne(x => x.Player)
            .WithMany(x => x.MatchPlayer)
            .HasForeignKey(x => x.PlayerId)
            .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<MatchFact>()
            .HasOne(x => x.Match)
            .WithOne(x => x.MatchFact);

            modelBuilder.Entity<Comment>()
            .HasOne(x => x.Match)
            .WithMany(x => x.Comment);

            modelBuilder.Entity<Fault>()
            .HasOne(x => x.Match)
            .WithMany(x => x.Fault);

            modelBuilder.Entity<Possesion>()
            .HasOne(x => x.Match)
            .WithMany(x => x.Possesion);

            modelBuilder.Entity<Shot>()
            .HasOne(x => x.Match)
            .WithMany(x => x.Shot);

            //Configuration

            //modelBuilder.ApplyConfiguration(new LeagueConfiguration());
            //modelBuilder.ApplyConfiguration(new SeasonConfiguration());
            //modelBuilder.ApplyConfiguration(new TeamConfiguration());
            //modelBuilder.ApplyConfiguration(new PlayerConfiguration());
            //modelBuilder.ApplyConfiguration(new TransferConfiguration());
            //modelBuilder.ApplyConfiguration(new RoundConfiguration());
            //modelBuilder.ApplyConfiguration(new MatchConfiguration());
            //modelBuilder.ApplyConfiguration(new MatchFactConfiguration());
            //modelBuilder.ApplyConfiguration(new MatchPlayerConfiguration());
            //modelBuilder.ApplyConfiguration(new CommentConfiguration());
            //modelBuilder.ApplyConfiguration(new ShotConfiguration());
            //modelBuilder.ApplyConfiguration(new FaultConfiguration());
            //modelBuilder.ApplyConfiguration(new PossessionConfiguration());

        }

    }
}
