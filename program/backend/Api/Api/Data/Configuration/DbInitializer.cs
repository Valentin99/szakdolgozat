﻿using Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Data
{
    public class DbInitializer : IDbInitializer
    {

        private readonly SportContext _context;

        public DbInitializer(SportContext context)
        {
            _context = context;
        }

        public void Initialize()
        {

            if (!_context.League.Any())
            {
                var leagues = new List<League>()
                {
                    new League
                    {
                        LeagueId = 1,
                        Name = "Premier League"
                    },
                    new League
                    {
                        LeagueId = 2,
                        Name = "Other"
                    }
                };

                _context.AddRange(leagues);
                _context.SaveChanges();
            }

            if (!_context.Season.Any())
            {
                var seasons = new List<Season>()
                {
                    new Season
                    {
                        SeasonId = 1,
                        Value = "2018/2019",
                        LeagueId = 1
                    }
                };

                _context.AddRange(seasons);
                _context.SaveChanges();
            }

            string CurrentDirectory = $"{Directory.GetCurrentDirectory()}/Data/Configuration/StaticData";

            if (!_context.Transfer.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Transfer/Transfer.csv");

                var transfers = new List<Transfer>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    transfers.Add(new Transfer
                    {
                        TransferId = Convert.ToInt32(Attributes[0]),
                        TeamId = Convert.ToInt32(Attributes[1]),
                        TeamName = Attributes[2],
                        PlayerName = Attributes[3],
                        InvolvedTeamName = Attributes[6],
                        Fee = Attributes[7],
                        TransferMovement = Attributes[8],
                        CleanedFee = Attributes[9],
                        LeagueId = Convert.ToInt32(Attributes[10]),
                        LeagueName = Attributes[11],
                        SeasonId = 1,
                        TransferSeason = Attributes[13]
                    });
                }

                _context.AddRange(transfers);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Round.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Round/Round.csv");

                var rounds = new List<Round>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    rounds.Add(new Round
                    {
                        RoundId = Convert.ToInt32(Attributes[0]),
                        GameWeek = Convert.ToInt32(Attributes[1]),
                        SeasonId = Convert.ToInt32(Attributes[2])
                    });
                }

                _context.AddRange(rounds);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Team.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Team/Team.csv");

                var teams = new List<Team>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(";");

                    teams.Add(new Team
                    {
                        TeamId = Convert.ToInt32(Attributes[0]),
                        Name = Attributes[1],
                        Logo = Attributes[2],
                        NickName = Attributes[3],
                        Formation = Attributes[4],
                        Stadium = Attributes[5],
                        LeagueId = 1,
                        SeasonId = 1,
                    });
                }

                _context.AddRange(teams);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Player.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Player/Player.csv");

                var players = new List<Player>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    players.Add(new Player
                    {
                        PlayerId = Convert.ToInt32(Attributes[0]),
                        Name = Attributes[1],
                        NickName = Attributes[2],
                        Age = Convert.ToInt32(Attributes[3]),
                        Birthday = Attributes[4],
                        Nationality = Attributes[5],
                        CurrentTeam = Attributes[6],
                        TeamId = Convert.ToInt32(Attributes[7]),
                        MarketValue = Convert.ToInt32(Attributes[8]),
                        TeamPosition = Attributes[9],
                        TeamYerseyNumber = Convert.ToInt32(Attributes[10]),
                    });
                }

                _context.AddRange(players);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Match.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Match/Match.csv");

                var match = new List<Match>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    bool simulated = Attributes[9] == "1" ? true : false;

                    match.Add(new Match
                    {
                        MatchId = Convert.ToInt32(Attributes[0]),
                        RoundId = Convert.ToInt32(Attributes[3]),
                        HomeTeamId = Convert.ToInt32(Attributes[5]),
                        AwayTeamId = Convert.ToInt32(Attributes[6]),
                        Simulated = simulated
                    });
                }

                _context.AddRange(match);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.MatchPlayer.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Match/MatchPlayer.csv");

                var matchPlayers = new List<MatchPlayer>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    bool startingteamPlayer = Attributes[12] == "1" ? true : false;
                    bool ingame = Attributes[13] == "1" ? true : false;
                    bool bench = Attributes[14] == "1" ? true : false;
                    bool substitute = Attributes[15] == "1" ? true : false;

                    matchPlayers.Add(new MatchPlayer
                    {
                        MatchPlayerId = Convert.ToInt32(Attributes[0]),
                        PlayerId = Convert.ToInt32(Attributes[1]),
                        MatchId = Convert.ToInt32(Attributes[2]),
                        TeamId = Convert.ToInt32(Attributes[3]),
                        PlayerName = Attributes[4],
                        TeamName = Attributes[5],
                        PlayerStatus = (PlayerStatus)Enum.Parse(typeof(PlayerStatus), Attributes[6], true),
                        PlayedMinutes = Convert.ToInt32(Attributes[7]),
                        Goal = Convert.ToInt32(Attributes[8]),
                        Assist = Convert.ToInt32(Attributes[9]),
                        YellowCard = Convert.ToInt32(Attributes[10]),
                        RedCard = Convert.ToInt32(Attributes[11]),
                        StartingTeamPlayer = startingteamPlayer,
                        InGame = ingame,
                        Bench = bench,
                        Substitute = substitute,
                        SubPlayerId = Convert.ToInt32(Attributes[16])
                    });
                }

                _context.AddRange(matchPlayers);
                _context.SaveChanges();
                sr.Close();
            }
 
            if (!_context.MatchFact.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Match/MatchFact.csv");

                var matchFact = new List<MatchFact>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    matchFact.Add(new MatchFact
                    {
                        MatchFactId = Convert.ToInt32(Attributes[0]),
                        HomeGoalCount = Convert.ToInt32(Attributes[1]),
                        AwayGoalCount = Convert.ToInt32(Attributes[2]),
                        HomeHalfTimeGoalCount = Convert.ToInt32(Attributes[4]),
                        AwayHalfTimeGoalCount = Convert.ToInt32(Attributes[5]),
                        HomeTotalShots = Convert.ToInt32(Attributes[7]),
                        AwayTotalShots = Convert.ToInt32(Attributes[8]),
                        HomeShotsOnTarget = Convert.ToInt32(Attributes[9]),
                        AwayShotsOnTarget = Convert.ToInt32(Attributes[10]),
                        HomeFouls = Convert.ToInt32(Attributes[11]),
                        AwayFouls = Convert.ToInt32(Attributes[12]),
                        HomeCorners = Convert.ToInt32(Attributes[13]),
                        AwayCorners = Convert.ToInt32(Attributes[14]),
                        HomeYellowCards = Convert.ToInt32(Attributes[15]),
                        AwayYellowCards = Convert.ToInt32(Attributes[16]),
                        HomeRedCards = Convert.ToInt32(Attributes[17]),
                        AwayRedCards = Convert.ToInt32(Attributes[18]),
                        MatchId = Convert.ToInt32(Attributes[19]),
                        FullTime = Attributes[20],
                        HalfTime = Attributes[21],
                        HomeOffsides = Convert.ToInt32(Attributes[22]),
                        AwayOffsides = Convert.ToInt32(Attributes[23])
                    });
                }

                _context.AddRange(matchFact);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Comment.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Comment/Comment.csv");

                var comments = new List<Comment>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    comments.Add(new Comment
                    {
                        CommentId = Convert.ToInt32(Attributes[0]),
                        Type = (CommentType)Enum.Parse(typeof(CommentType), Attributes[1], true),
                        MatchId = Convert.ToInt32(Attributes[2]),
                        Minute = Convert.ToInt32(Attributes[3]),
                        Description = Attributes[4]
                    });
                }

                _context.AddRange(comments);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Fault.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Fault/Fault.csv");

                var faults = new List<Fault>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    faults.Add(new Fault
                    {
                        FaultId = Convert.ToInt32(Attributes[0]),
                        MatchId = Convert.ToInt32(Attributes[1]),
                        TeamId = Convert.ToInt32(Attributes[2]),
                        PlayerId = Convert.ToInt32(Attributes[3]),
                        Minute = Convert.ToInt32(Attributes[4]),
                        Cardtype = (Cardtype)Enum.Parse(typeof(Cardtype), Attributes[5], true),
                    });
                }

                _context.AddRange(faults);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Possesion.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Possession/Possession.csv");

                var possesions = new List<Possesion>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    possesions.Add(new Possesion
                    {
                        PossesionId = Convert.ToInt32(Attributes[0]),
                        HomeTeamId = Convert.ToInt32(Attributes[1]),
                        AwayTeamId = Convert.ToInt32(Attributes[2]),
                        Minute = Convert.ToInt32(Attributes[3]),
                        HomeTeamPass = Convert.ToInt32(Attributes[4]),
                        AwayTeamPass = Convert.ToInt32(Attributes[5]),
                        HomePossesion = Convert.ToInt32(Attributes[6]),
                        AwayPossesion = Convert.ToInt32(Attributes[7]),
                        MatchId = Convert.ToInt32(Attributes[8])
                    });
                }

                _context.AddRange(possesions);
                _context.SaveChanges();
                sr.Close();
            }

            if (!_context.Shot.Any())
            {
                StreamReader sr = new StreamReader($"{CurrentDirectory}/Shot/Shot.csv");

                var shots = new List<Shot>();

                while (!sr.EndOfStream)
                {
                    string[] Attributes = sr.ReadLine().Split(",");

                    bool goal = Attributes[9] == "1" ? true : false;

                    shots.Add(new Shot
                    {
                        ShotId = Convert.ToInt32(Attributes[0]),
                        Minute = Convert.ToInt32(Attributes[1]),
                        Event = (Event)Enum.Parse(typeof(Event), Attributes[2], true),
                        LeagueId = Convert.ToInt32(Attributes[3]),
                        SeasonId = Convert.ToInt32(Attributes[4]),
                        TeamId = Convert.ToInt32(Attributes[5]),
                        ShotPlayerId = Convert.ToInt32(Attributes[6]),
                        AssistPlayerId = Convert.ToInt32(Attributes[7]),
                        MatchId = Convert.ToInt32(Attributes[8]),
                        Goal = goal
                    });
                }

                _context.AddRange(shots);
                _context.SaveChanges();
                sr.Close();
            }

        }
    }
}