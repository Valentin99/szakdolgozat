﻿CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
    "MigrationId" TEXT NOT NULL CONSTRAINT "PK___EFMigrationsHistory" PRIMARY KEY,
    "ProductVersion" TEXT NOT NULL
);

BEGIN TRANSACTION;

CREATE TABLE "League" (
    "LeagueId" INTEGER NOT NULL CONSTRAINT "PK_League" PRIMARY KEY AUTOINCREMENT,
    "Name" TEXT NULL
);

CREATE TABLE "Season" (
    "SeasonId" INTEGER NOT NULL CONSTRAINT "PK_Season" PRIMARY KEY AUTOINCREMENT,
    "Value" TEXT NULL,
    "LeagueId" INTEGER NOT NULL,
    CONSTRAINT "FK_Season_League_LeagueId" FOREIGN KEY ("LeagueId") REFERENCES "League" ("LeagueId") ON DELETE CASCADE
);

CREATE TABLE "Round" (
    "RoundId" INTEGER NOT NULL CONSTRAINT "PK_Round" PRIMARY KEY AUTOINCREMENT,
    "GameWeek" INTEGER NOT NULL,
    "Simulated" INTEGER NOT NULL,
    "SeasonId" INTEGER NOT NULL,
    CONSTRAINT "FK_Round_Season_SeasonId" FOREIGN KEY ("SeasonId") REFERENCES "Season" ("SeasonId") ON DELETE CASCADE
);

CREATE TABLE "Team" (
    "TeamId" INTEGER NOT NULL CONSTRAINT "PK_Team" PRIMARY KEY AUTOINCREMENT,
    "Name" TEXT NULL,
    "Logo" TEXT NULL,
    "NickName" TEXT NULL,
    "Formation" TEXT NULL,
    "Stadium" TEXT NULL,
    "SeasonId" INTEGER NOT NULL,
    "LeagueId" INTEGER NOT NULL,
    CONSTRAINT "FK_Team_Season_SeasonId" FOREIGN KEY ("SeasonId") REFERENCES "Season" ("SeasonId") ON DELETE CASCADE
);

CREATE TABLE "Transfer" (
    "TransferId" INTEGER NOT NULL CONSTRAINT "PK_Transfer" PRIMARY KEY AUTOINCREMENT,
    "SeasonId" INTEGER NOT NULL,
    "TeamId" INTEGER NOT NULL,
    "InvolvedTeamId" INTEGER NOT NULL,
    "PlayerId" INTEGER NOT NULL,
    "LeagueId" INTEGER NOT NULL,
    "LeagueName" TEXT NULL,
    "TransferSeason" TEXT NULL,
    "TeamName" TEXT NULL,
    "PlayerName" TEXT NULL,
    "InvolvedTeamName" TEXT NULL,
    "Fee" TEXT NULL,
    "CleanedFee" TEXT NULL,
    "TransferMovement" TEXT NULL,
    CONSTRAINT "FK_Transfer_Season_SeasonId" FOREIGN KEY ("SeasonId") REFERENCES "Season" ("SeasonId") ON DELETE CASCADE
);

CREATE TABLE "Match" (
    "MatchId" INTEGER NOT NULL CONSTRAINT "PK_Match" PRIMARY KEY AUTOINCREMENT,
    "RoundId" INTEGER NOT NULL,
    "HomeTeamId" INTEGER NOT NULL,
    "AwayTeamId" INTEGER NOT NULL,
    "Stadium" TEXT NULL,
    "Referee" TEXT NULL,
    "Simulated" INTEGER NOT NULL,
    CONSTRAINT "FK_Match_Round_RoundId" FOREIGN KEY ("RoundId") REFERENCES "Round" ("RoundId") ON DELETE CASCADE
);

CREATE TABLE "Player" (
    "PlayerId" INTEGER NOT NULL CONSTRAINT "PK_Player" PRIMARY KEY AUTOINCREMENT,
    "TeamId" INTEGER NOT NULL,
    "Name" TEXT NULL,
    "NickName" TEXT NULL,
    "Age" INTEGER NOT NULL,
    "Birthday" TEXT NULL,
    "Nationality" TEXT NULL,
    "CurrentTeam" TEXT NULL,
    "MarketValue" INTEGER NOT NULL,
    "TeamPosition" TEXT NULL,
    "TeamYerseyNumber" INTEGER NOT NULL,
    CONSTRAINT "FK_Player_Team_TeamId" FOREIGN KEY ("TeamId") REFERENCES "Team" ("TeamId") ON DELETE CASCADE
);

CREATE TABLE "Comment" (
    "CommentId" INTEGER NOT NULL CONSTRAINT "PK_Comment" PRIMARY KEY AUTOINCREMENT,
    "Type" INTEGER NOT NULL,
    "MatchId" INTEGER NOT NULL,
    "Minute" INTEGER NOT NULL,
    "Description" TEXT NULL,
    CONSTRAINT "FK_Comment_Match_MatchId" FOREIGN KEY ("MatchId") REFERENCES "Match" ("MatchId") ON DELETE CASCADE
);

CREATE TABLE "Fault" (
    "FaultId" INTEGER NOT NULL CONSTRAINT "PK_Fault" PRIMARY KEY AUTOINCREMENT,
    "MatchId" INTEGER NOT NULL,
    "TeamId" INTEGER NOT NULL,
    "PlayerId" INTEGER NOT NULL,
    "Minute" INTEGER NOT NULL,
    "Cardtype" INTEGER NOT NULL,
    CONSTRAINT "FK_Fault_Match_MatchId" FOREIGN KEY ("MatchId") REFERENCES "Match" ("MatchId") ON DELETE CASCADE
);

CREATE TABLE "MatchFact" (
    "MatchFactId" INTEGER NOT NULL CONSTRAINT "PK_MatchFact" PRIMARY KEY AUTOINCREMENT,
    "MatchId" INTEGER NOT NULL,
    "Simulated" INTEGER NOT NULL,
    "HalfTime" TEXT NULL,
    "FullTime" TEXT NULL,
    "HomeTotalShots" INTEGER NOT NULL,
    "AwayTotalShots" INTEGER NOT NULL,
    "HomeShotsOnTarget" INTEGER NOT NULL,
    "AwayShotsOnTarget" INTEGER NOT NULL,
    "HomeGoalCount" INTEGER NOT NULL,
    "AwayGoalCount" INTEGER NOT NULL,
    "HomeHalfTimeGoalCount" INTEGER NOT NULL,
    "AwayHalfTimeGoalCount" INTEGER NOT NULL,
    "HomeOffsides" INTEGER NOT NULL,
    "AwayOffsides" INTEGER NOT NULL,
    "HomeCorners" INTEGER NOT NULL,
    "AwayCorners" INTEGER NOT NULL,
    "HomeYellowCards" INTEGER NOT NULL,
    "AwayYellowCards" INTEGER NOT NULL,
    "HomeRedCards" INTEGER NOT NULL,
    "AwayRedCards" INTEGER NOT NULL,
    "HomeFouls" INTEGER NOT NULL,
    "AwayFouls" INTEGER NOT NULL,
    CONSTRAINT "FK_MatchFact_Match_MatchId" FOREIGN KEY ("MatchId") REFERENCES "Match" ("MatchId") ON DELETE CASCADE
);

CREATE TABLE "Possesion" (
    "PossesionId" INTEGER NOT NULL CONSTRAINT "PK_Possesion" PRIMARY KEY AUTOINCREMENT,
    "HomeTeamId" INTEGER NOT NULL,
    "AwayTeamId" INTEGER NOT NULL,
    "Minute" INTEGER NOT NULL,
    "HomeTeamPass" INTEGER NOT NULL,
    "AwayTeamPass" INTEGER NOT NULL,
    "HomePossesion" INTEGER NOT NULL,
    "AwayPossesion" INTEGER NOT NULL,
    "MatchId" INTEGER NOT NULL,
    CONSTRAINT "FK_Possesion_Match_MatchId" FOREIGN KEY ("MatchId") REFERENCES "Match" ("MatchId") ON DELETE CASCADE
);

CREATE TABLE "Shot" (
    "ShotId" INTEGER NOT NULL CONSTRAINT "PK_Shot" PRIMARY KEY AUTOINCREMENT,
    "Minute" INTEGER NOT NULL,
    "Event" INTEGER NOT NULL,
    "LeagueId" INTEGER NOT NULL,
    "SeasonId" INTEGER NOT NULL,
    "TeamId" INTEGER NOT NULL,
    "ShotPlayerId" INTEGER NOT NULL,
    "AssistPlayerId" INTEGER NOT NULL,
    "MatchId" INTEGER NOT NULL,
    "Goal" INTEGER NOT NULL,
    CONSTRAINT "FK_Shot_Match_MatchId" FOREIGN KEY ("MatchId") REFERENCES "Match" ("MatchId") ON DELETE CASCADE
);

CREATE TABLE "MatchPlayer" (
    "MatchPlayerId" INTEGER NOT NULL CONSTRAINT "PK_MatchPlayer" PRIMARY KEY AUTOINCREMENT,
    "PlayerId" INTEGER NOT NULL,
    "MatchId" INTEGER NOT NULL,
    "TeamId" INTEGER NOT NULL,
    "PlayerName" TEXT NULL,
    "TeamName" TEXT NULL,
    "PlayerStatus" INTEGER NOT NULL,
    "PlayedMinutes" INTEGER NOT NULL,
    "Goal" INTEGER NOT NULL,
    "Assist" INTEGER NOT NULL,
    "YellowCard" INTEGER NOT NULL,
    "RedCard" INTEGER NOT NULL,
    "StartingTeamPlayer" INTEGER NOT NULL,
    "InGame" INTEGER NOT NULL,
    "Bench" INTEGER NOT NULL,
    "Substitute" INTEGER NOT NULL,
    "SubPlayerId" INTEGER NOT NULL,
    CONSTRAINT "FK_MatchPlayer_Match_MatchId" FOREIGN KEY ("MatchId") REFERENCES "Match" ("MatchId"),
    CONSTRAINT "FK_MatchPlayer_Player_PlayerId" FOREIGN KEY ("PlayerId") REFERENCES "Player" ("PlayerId")
);

CREATE INDEX "IX_Comment_MatchId" ON "Comment" ("MatchId");

CREATE INDEX "IX_Fault_MatchId" ON "Fault" ("MatchId");

CREATE INDEX "IX_Match_RoundId" ON "Match" ("RoundId");

CREATE UNIQUE INDEX "IX_MatchFact_MatchId" ON "MatchFact" ("MatchId");

CREATE INDEX "IX_MatchPlayer_MatchId" ON "MatchPlayer" ("MatchId");

CREATE INDEX "IX_MatchPlayer_PlayerId" ON "MatchPlayer" ("PlayerId");

CREATE INDEX "IX_Player_TeamId" ON "Player" ("TeamId");

CREATE INDEX "IX_Possesion_MatchId" ON "Possesion" ("MatchId");

CREATE INDEX "IX_Round_SeasonId" ON "Round" ("SeasonId");

CREATE INDEX "IX_Season_LeagueId" ON "Season" ("LeagueId");

CREATE INDEX "IX_Shot_MatchId" ON "Shot" ("MatchId");

CREATE INDEX "IX_Team_SeasonId" ON "Team" ("SeasonId");

CREATE INDEX "IX_Transfer_SeasonId" ON "Transfer" ("SeasonId");

INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20210318161104_initialcreate', '5.0.3');

COMMIT;

