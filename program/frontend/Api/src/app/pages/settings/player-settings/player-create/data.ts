export interface Team {
    teamId: number,
    name: string
}

export interface Position {
    name: string
}

export const TEAMS: Team[] = [
    { teamId: 1, name: 'Arsenal' },
    { teamId: 2, name: 'Bournemouth' },
    { teamId: 3, name: 'Brighton & Hove Albion' },
    { teamId: 4, name: 'Burnley' },
    { teamId: 5, name: 'Cardiff City' },
    { teamId: 6, name: 'Chelsea' },
    { teamId: 7, name: 'Crystal Palace' },
    { teamId: 8, name: 'Everton' },
    { teamId: 9, name: 'Fulham' },
    { teamId: 10, name: 'Huddersfield Town' },
    { teamId: 11, name: 'Leicester City' },
    { teamId: 12, name: 'Liverpool' },
    { teamId: 13, name: 'Newcastle United' },
    { teamId: 14, name: 'Manchester City' },
    { teamId: 15, name: 'Manchester United' },
    { teamId: 16, name: 'Southampton' },
    { teamId: 17, name: 'Tottenham Hotspur' },
    { teamId: 18, name: 'Watford' },
    { teamId: 19, name: 'West Ham United' },
    { teamId: 20, name: 'Wolverhampton Wanderers' }
];

export const POSITIONS: Position[] = [
    {
        name: 'CAM',
    },
    {
        name: 'CB',
    },
    {
        name: 'CDM',
    },
    {
        name: 'CF',
    },
    {
        name: 'CM',
    },
    {
        name: 'GK',
    },
    {
        name: 'LB',
    },
    {
        name: 'LCB',
    },
    {
        name: 'LDM',
    },
    {
        name: 'LM',
    },
    {
        name: 'LW',
    },
    {
        name: 'RB',
    },
    {
        name: 'RWB',
    },
    {
        name: 'RCM',
    },
    {
        name: 'RDM',
    },
    {
        name: 'RM',
    },
    {
        name: 'RS',
    },
    {
        name: 'RW'
    },
    {
        name: 'RWB'
    },
    {
        name: 'ST'
    },
    {
        name: 'SUB'
    }
];