import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { PlayerSettingsService } from 'src/app/services/domain/settings/player.settings.service';
import { Player } from 'src/app/services/domain/settings/player/player-settings.model';
import { PlayerCreateComponent } from './player-create/player-create.component';
import { PlayerEditComponent } from './player-edit/player-edit.component';

@Component({
  selector: 'app-player-settings',
  templateUrl: './player-settings.component.html',
  styleUrls: ['./player-settings.component.css']
})
export class PlayerSettingsComponent {

  players: Player[];
  displayedColumns: string[];
  dataSource: MatTableDataSource<Player>;

  constructor(private router: Router,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private service: PlayerSettingsService) { }

  ngAfterViewInit() {
    this.getPlayers();
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPlayers() {
    this.service.getPlayers().subscribe(val => {
        this.players = val;
        this.displayedColumns = ['name', 'nickName', 'age', 'birthday', 'nationality', 'currentTeam', 'marketValue', 'teamPosition', 'teamYerseyNumber', 'edit', 'delete'];
        this.dataSource = new MatTableDataSource(this.players),
        this.dataSource.sort = this.sort,
        this.dataSource.paginator = this.paginator
    });
  }

  onEdit(player: Player): void {

    const dialogRef = this.dialog.open(PlayerEditComponent, {
      width: '500px',
      data: player
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onCreate(): void {
    const dialogRef = this.dialog.open(PlayerCreateComponent, {
      width: '500px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onDelete(player: Player): void {
    this.service.deletePlayer(player.playerId).subscribe(val => 
      console.log(val),
      this.succes()
    );
  }

  succes() {
    this.snackBar.open('Sikeres törlés', 'Játékos', {
      duration: 1000,
    });
    this.getPlayers();
  }

}
