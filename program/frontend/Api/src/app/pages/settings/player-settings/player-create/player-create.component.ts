import { validateHorizontalPosition } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PlayerSettingsService } from 'src/app/services/domain/settings/player.settings.service';
import { CreatePlayerDTO } from 'src/app/services/domain/settings/player/player-settings';
import { Position, POSITIONS, Team, TEAMS } from './data';

@Component({
  selector: 'app-player-create',
  templateUrl: './player-create.component.html',
  styleUrls: ['./player-create.component.css']
})
export class PlayerCreateComponent implements OnInit {

  teams: Team[];
  positions: Position[];

  constructor(private snackBar: MatSnackBar, private service: PlayerSettingsService) { }

  ngOnInit(): void {
    this.teams = TEAMS;
    this.positions = POSITIONS;
  }

  succes() {
    this.snackBar.open('Sikeres létrehozás', 'Új játékos', {
      duration: 1000,
    });
  }

  playerFormGroup = new FormGroup({
    team: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
    nickName: new FormControl('', [Validators.required]),
    age: new FormControl('', [Validators.required, Validators.min(0), Validators.max(99)]),
    birthday: new FormControl('', [Validators.required]),
    nationality: new FormControl('', [Validators.required]),
    teamPosition: new FormControl('', [Validators.required]),
    marketValue: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    teamYerseyNumber: new FormControl('', [Validators.required])
  });
  player: CreatePlayerDTO;

  create() {
    this.player = {
      teamId: Number(this.playerFormGroup.get('team').value),
      name: this.playerFormGroup.get('name').value,
      nickName: this.playerFormGroup.get('nickName').value,
      age: this.playerFormGroup.get('age').value,
      birthday: this.playerFormGroup.get('birthday').value,
      nationality: this.playerFormGroup.get('nationality').value,
      teamPosition: this.playerFormGroup.get('teamPosition').value,
      marketValue: this.playerFormGroup.get('marketValue').value,
      teamYerseyNumber: Number(this.playerFormGroup.get('teamYerseyNumber').value)
    }

    this.service.createPlayer(this.player.teamId,
                              this.player.name,
                              this.player.nickName,
                              this.player.age,
                              this.player.birthday,
                              this.player.nationality,
                              this.player.marketValue,
                              this.player.teamPosition,
                              this.player.teamYerseyNumber).subscribe();
    this.succes();
  }

  getError(el) {
    switch (el) {
      case 'name':
        if (this.playerFormGroup.get('name').hasError('required')) {
          return 'A név mező kitöltése kötelező!';
        }
        break;
      case 'nickName':
        if (this.playerFormGroup.get('nickName').hasError('required')) {
          return 'A becenév mező kitöltése kötelező!';
        }
        break;
      case 'team':
        if (this.playerFormGroup.get('team').hasError('required')) {
          return 'A csapat mező kitöltése kötelező!';
        }
        break;
      case 'age':
        if (this.playerFormGroup.get('age').hasError('required')) {
          return 'Az életkor mező kitöltése kötelező!';
        }
        break;
      case 'birthday':
        if (this.playerFormGroup.get('birthday').hasError('required')) {
          return 'A születésnap mező kitöltése kötelező!';
        }
        break;
      case 'nationality':
        if (this.playerFormGroup.get('nationality').hasError('required')) {
          return 'A nemzetiség mező kitöltése kötelező!';
        }
        break;
      case 'marketValue':
        if (this.playerFormGroup.get('marketValue').hasError('required')) {
          return 'A piaci érték mező kitöltése kötelező!';
        }
        break;
      case 'teamPosition':
        if (this.playerFormGroup.get('teamPosition').hasError('required')) {
          return 'A pozíció mező kitöltése kötelező!';
        }
        break;
      case 'teamYerseyNumber':
        if (this.playerFormGroup.get('teamYerseyNumber').hasError('required')) {
          return 'A mez szám mező kitöltése kötelező!';
        }
        break;
      default:
        return '';
    }
  }

}
