import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { SettingsRoutingModule } from './settings-routing.module';
import { TeamCreateComponent } from './team-settings/team-create/team-create.component';
import { PlayerSettingsComponent } from './player-settings/player-settings.component';
import { TeamSettingsComponent } from './team-settings/team-settings.component';
import { TeamEditComponent } from './team-settings/team-edit/team-edit.component';
import { PlayerCreateComponent } from './player-settings/player-create/player-create.component';
import { PlayerEditComponent } from './player-settings/player-edit/player-edit.component';

@NgModule({
  declarations: [
    TeamSettingsComponent,
    TeamCreateComponent,
    PlayerSettingsComponent,
    TeamEditComponent,
    PlayerCreateComponent,
    PlayerEditComponent,
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SettingsModule { }
