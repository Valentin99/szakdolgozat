import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayerSettingsComponent } from './player-settings/player-settings.component';
import { TeamSettingsComponent } from './team-settings/team-settings.component';

const routes: Routes = [
  { path: 'team', component: TeamSettingsComponent },
  { path: 'player', component: PlayerSettingsComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
