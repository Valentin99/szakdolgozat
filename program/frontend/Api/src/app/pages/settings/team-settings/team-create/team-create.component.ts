import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { TeamSettingsService } from 'src/app/services/domain/settings/team.settings.service';
import { CreateTeamDTO, Formation, League } from 'src/app/services/domain/settings/team/team-settings';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit {

  formations: Formation[] = [
    { value: '4-2-3-1', text: '4-2-3-1' },
    { value: '4-3-3', text: '4-3-3' },
    { value: '5-3-2', text: '5-3-2' },
    { value: '4-4-2', text: '4-4-2' },
    { value: '3-4-1-2', text: '3-4-1-2' },
    { value: '4-1-4-1', text: '4-1-4-1'}
  ];

  leagues: League[] = [
    { value: '1', text: 'Egyéb' }
  ];

  constructor(private snackBar: MatSnackBar, private service: TeamSettingsService) { }

  ngOnInit(): void {

  }

  succes() {
    this.snackBar.open('Sikeres létrehozás', 'Új csapat', {
      duration: 1000,
    });
  }

  teamFormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    nickName: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    stadion: new FormControl('', [Validators.required, Validators.maxLength(20)]),
    formation: new FormControl('', [Validators.required])
  });
  team: CreateTeamDTO;

  create() {
    this.team = {
      name: this.teamFormGroup.get('name').value,
      nickName: this.teamFormGroup.get('nickName').value,
      stadium: this.teamFormGroup.get('stadion').value,
      formation: this.teamFormGroup.get('formation').value,
    }
    this.service.createTeam(this.team.name, this.team.nickName, this.team.stadium, this.team.formation).subscribe();
    this.succes();
  }

  getError(el) {
    switch (el) {
      case 'name':
        if (this.teamFormGroup.get('name').hasError('required')) {
          return 'A név mező kitöltése kötelező!';
        }
        break;
      case 'nickName':
        if (this.teamFormGroup.get('nickName').hasError('required')) {
          return 'A becenév mező kitöltése kötelező!';
        }
        break;
      case 'stadion':
        if (this.teamFormGroup.get('stadion').hasError('required')) {
          return 'A stadion mező kitöltése kötelező!';
        }
        break;
      case 'formation':
        if (this.teamFormGroup.get('formation').hasError('required')) {
          return 'A stadion mező kitöltése kötelező!';
        }
        break;
      default:
        return '';
    }
  }

}
