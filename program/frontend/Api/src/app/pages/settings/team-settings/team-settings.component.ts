import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TeamSettingsService } from 'src/app/services/domain/settings/team.settings.service';
import { Team } from 'src/app/services/domain/settings/team/team-setting.model';
import { TeamCreateComponent } from './team-create/team-create.component';
import { TeamEditComponent } from './team-edit/team-edit.component';

@Component({
  selector: 'app-team-settings',
  templateUrl: './team-settings.component.html',
  styleUrls: ['./team-settings.component.css']
})
export class TeamSettingsComponent {

  teams: Team[];
  displayedColumns: string[];
  dataSource: MatTableDataSource<Team>;

  constructor(public dialog: MatDialog, private snackBar: MatSnackBar, private service: TeamSettingsService) { }

  ngAfterViewInit() {
    this.getPlayers();
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getPlayers() {
    this.service.getTeams().subscribe(val => {
      this.teams = val;
      this.displayedColumns = ['logo', 'name', 'nickName', 'stadium', 'formation', 'edit', 'delete'];
      this.dataSource = new MatTableDataSource(this.teams);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  onEdit(team: Team) {
    const dialogRef = this.dialog.open(TeamEditComponent, {
      width: '500px',
      data: team
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onCreate(): void {
    const dialogRef = this.dialog.open(TeamCreateComponent, {
      width: '500px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onDelete(team: Team): void {
    this.service.deleteTeam(team.teamId).subscribe(
      (val: JSON) => console.log(val),
      this.succes(),
      this.getPlayers()
    );
  }

  succes() {
    this.snackBar.open('Sikeres törlés', 'csapat', {
      duration: 1000,
    });
  }

}
