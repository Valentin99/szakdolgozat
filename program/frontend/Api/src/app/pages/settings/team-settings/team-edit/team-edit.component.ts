import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TeamSettingsService } from 'src/app/services/domain/settings/team.settings.service';
import { Team } from 'src/app/services/domain/settings/team/team-setting.model';
import { League, Formation, UpdateTeamDTO } from 'src/app/services/domain/settings/team/team-settings';

@Component({
  selector: 'app-team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.css']
})
export class TeamEditComponent implements OnInit {

  teamFormGroup: FormGroup;
  team: UpdateTeamDTO;

  formations: Formation[] = [
    { value: '4-2-3-1', text: '4-2-3-1' },
    { value: '4-3-3', text: '4-3-3' },
    { value: '5-3-2', text: '5-3-2' },
    { value: '4-4-2', text: '4-4-2' },
    { value: '3-4-1-2', text: '3-4-1-2' },
    { value: '4-1-4-1', text: '4-1-4-1' }
  ];

  leagues: League[] = [
    { value: '1', text: 'Egyéb' }
  ];

  constructor(private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<TeamEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Team,
    private service: TeamSettingsService) { }

  ngOnInit(): void {
    this.teamFormGroup = new FormGroup({
      name: new FormControl(this.data.name, [Validators.required, Validators.maxLength(20)]),
      nickName: new FormControl(this.data.nickName, [Validators.required, Validators.maxLength(20)]),
      stadion: new FormControl(this.data.stadium, [Validators.required, Validators.maxLength(20)]),
      formation: new FormControl(this.data.formation, [Validators.required])
    });
  }

  succes() {
    this.snackBar.open('Sikeres módosítás', 'Csapat', {
      duration: 1000,
    });
  }

  edit() {
    this.team = {
      teamId: this.data.teamId,
      logo: this.data.logo,
      formation: this.teamFormGroup.get('formation').value,
      name: this.teamFormGroup.get('name').value,
      nickName: this.teamFormGroup.get('nickName').value,
      stadium: this.teamFormGroup.get('stadion').value,
    }

    this.service.updateTeam(this.team.teamId, this.team.logo, this.team.name, this.team.nickName, this.team.stadium, this.team.formation).subscribe(val =>
      console.log(val)  
    );

    this.succes();
  }

  getError(el) {
    switch (el) {
      case 'name':
        if (this.teamFormGroup.get('name').hasError('required')) {
          return 'A név mező kitöltése kötelező!';
        }
        break;
      case 'nickName':
        if (this.teamFormGroup.get('nickName').hasError('required')) {
          return 'A becenév mező kitöltése kötelező!';
        }
        break;
      case 'stadion':
        if (this.teamFormGroup.get('stadion').hasError('required')) {
          return 'A stadion mező kitöltése kötelező!';
        }
        break;
      case 'formation':
        if (this.teamFormGroup.get('formation').hasError('required')) {
          return 'A stadion mező kitöltése kötelező!';
        }
        break;
      default:
        return '';
    }
  }

}
