import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { LeagueService } from 'src/app/services/domain/league/league.service';
import { LastResult } from 'src/app/services/domain/league/result/lastResult.model';

@Component({
  selector: 'app-round',
  templateUrl: './round.component.html',
  styleUrls: ['./round.component.css']
})
export class RoundComponent implements AfterViewInit {

  displayedColumns: string[];
  results: LastResult[];
  dataSource: any;

  roundId = Number(this.route.snapshot.paramMap.get('id'));

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private router: Router, private route: ActivatedRoute, private service: LeagueService) { }
  ngAfterViewInit(): void {
    this.leagueResults(this.roundId);
  }

  leagueResults(roundId: number) {
    this.service.getLastResultstById(roundId).subscribe(val => {
      this.results = val;
      this.displayedColumns = ['homeTeam', 'awayTeam', 'fulltime', 'actions'];
      this.dataSource = new MatTableDataSource<LastResult>(this.results);
      this.dataSource.paginator = this.paginator;
    });
  }

  onDetailsClick(result: LastResult) {
    this.router.navigate(['summary', 'matchdetails', result.matchId]);
  }

}
