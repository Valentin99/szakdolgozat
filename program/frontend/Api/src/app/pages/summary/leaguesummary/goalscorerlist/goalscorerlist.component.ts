import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { GoalscorerList } from 'src/app/services/domain/league/goalscorerlist/goalscorerList.model';
import { LeagueService } from 'src/app/services/domain/league/league.service';

@Component({
  selector: 'app-goalscorerlist',
  templateUrl: './goalscorerlist.component.html',
  styleUrls: ['./goalscorerlist.component.css']
})
export class GoalscorerlistComponent implements AfterViewInit {

  constructor(private router: Router, private route: ActivatedRoute, private service: LeagueService) { }

  displayedColumns: string[];
  goalscorer: GoalscorerList[];
  dataSource: any;

  roundId = Number(this.route.snapshot.paramMap.get('id'));

  @ViewChild(MatPaginator) paginator: MatPaginator;

  leagueGoalscorer(leagueId: number, seasonId: number, roundId: number) {
    this.service.getGoalscorerListById(leagueId,seasonId,roundId).subscribe(val => {
      this.goalscorer = val;
      this.displayedColumns = ['position', 'playerName', 'goal', 'actions'];
      this.dataSource =  new MatTableDataSource<GoalscorerList>(this.goalscorer);
      this.dataSource.paginator = this.paginator;
    });
  }

  ngAfterViewInit() {
    this.leagueGoalscorer(1,1,this.roundId);
  }

  onDetailsClick(player: GoalscorerList) {
    this.router.navigate(['summary', 'player', player.playerId]);
  }

}
