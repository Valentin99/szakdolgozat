import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { RedCard } from 'src/app/services/domain/league/card/card.model';
import { LeagueService } from 'src/app/services/domain/league/league.service';

const ELEMENT_DATA: RedCard[] = [
  {playerId: 1, position: 1, playerName: 'Son Heung-Min', redCard: 4 },
  {playerId: 2, position: 2, playerName: 'Calvert-Lewin D', redCard: 3 },
  {playerId: 3, position: 3, playerName: 'Bamford P.', redCard: 2 },
  {playerId: 4, position: 4, playerName: 'Salah M.', redCard: 1 },
  {playerId: 5, position: 5, playerName: 'Vardy J.', redCard: 1 },
  {playerId: 6, position: 6, playerName: 'Carbon', redCard: 1 },
];

@Component({
  selector: 'app-redcard',
  templateUrl: './redcard.component.html',
  styleUrls: ['./redcard.component.css']
})
export class RedcardComponent implements AfterViewInit {

  displayedColumns: string[];
  redcards: RedCard[];
  dataSource = new MatTableDataSource<RedCard>(ELEMENT_DATA);

  constructor(private router: Router, private route: ActivatedRoute, private service: LeagueService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  roundId = Number(this.route.snapshot.paramMap.get('id'));

  ngAfterViewInit() {
    this.leagueRedCard(1,1,this.roundId);
  }

  leagueRedCard(leagueId: number, seasonId: number, roundId: number) {
    this.service.getRedCardsById(leagueId,seasonId,roundId).subscribe(val => {
      this.redcards = val;
      this.displayedColumns = ['position', 'playerName', 'redCard', 'actions'];
      this.dataSource =  new MatTableDataSource<RedCard>(this.redcards);
      this.dataSource.paginator = this.paginator;
    });
  }

  onDetailsClick(player: RedCard) {
    this.router.navigate(['summary', 'player', player.playerId]);
  }

}
