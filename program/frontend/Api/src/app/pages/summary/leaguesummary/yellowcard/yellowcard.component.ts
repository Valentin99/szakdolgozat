import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { YellowCard } from 'src/app/services/domain/league/card/card.model';
import { LeagueService } from 'src/app/services/domain/league/league.service';

const ELEMENT_DATA: YellowCard[] = [
  {playerId: 1, position: 1, playerName: 'Son Heung-Min', yellowCard: 5 },
  {playerId: 2, position: 2, playerName: 'Calvert-Lewin D', yellowCard: 4 },
  {playerId: 3, position: 3, playerName: 'Bamford P.', yellowCard: 3 },
  {playerId: 4, position: 4, playerName: 'Salah M.', yellowCard: 3 },
  {playerId: 5, position: 5, playerName: 'Vardy J.', yellowCard: 3 },
  {playerId: 6, position: 6, playerName: 'Carbon', yellowCard: 2 },
  {playerId: 7, position: 7, playerName: 'Kane H.', yellowCard: 2 },
  {playerId: 8, position: 8, playerName: 'Zaha W.', yellowCard: 2 },
  {playerId: 9, position: 9, playerName: 'Ings D.', yellowCard: 1 },
  {playerId: 10, position: 10, playerName: 'Mane S.', yellowCard: 1 },
  {playerId: 11, position: 11, playerName: 'Maupay N.', yellowCard: 1 },
  {playerId: 12, position: 12, playerName: 'Wilson C.', yellowCard: 1 }
];

@Component({
  selector: 'app-yellowcard',
  templateUrl: './yellowcard.component.html',
  styleUrls: ['./yellowcard.component.css']
})
export class YellowcardComponent implements AfterViewInit {

  displayedColumns: string[];
  yellowCards: YellowCard[];
  dataSource = new MatTableDataSource<YellowCard>(ELEMENT_DATA);

  constructor(private router: Router, private route: ActivatedRoute, private service: LeagueService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  roundId = Number(this.route.snapshot.paramMap.get('id'));

  ngAfterViewInit() {
    this.leagueYellowCard(1,1,this.roundId);
  }

  leagueYellowCard(leagueId: number, seasonId: number, roundId: number) {
    this.service.getYellowCardsById(leagueId,seasonId,roundId).subscribe(val => {
      this.yellowCards = val;
      this.displayedColumns =['position', 'playerName', 'yellowCard', 'actions'];
      this.dataSource =  new MatTableDataSource<YellowCard>(this.yellowCards);
      this.dataSource.paginator = this.paginator;
    });
  }

  onDetailsClick(player: YellowCard) {
    this.router.navigate(['summary', 'player', player.playerId]);
  }

}
