import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalscorerlistComponent } from './goalscorerlist.component';

describe('GoalscorerlistComponent', () => {
  let component: GoalscorerlistComponent;
  let fixture: ComponentFixture<GoalscorerlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoalscorerlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoalscorerlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
