import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YellowcardComponent } from './yellowcard.component';

describe('YellowcardComponent', () => {
  let component: YellowcardComponent;
  let fixture: ComponentFixture<YellowcardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ YellowcardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(YellowcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
