import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedcardComponent } from './redcard.component';

describe('RedcardComponent', () => {
  let component: RedcardComponent;
  let fixture: ComponentFixture<RedcardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedcardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RedcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
