import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeamStanding } from 'src/app/services/domain/league/standing/standing.model';

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.css']
})
export class LeagueComponent implements OnInit {

  title = '';

  constructor(private router: Router, private route: ActivatedRoute) { }

  roundId = Number(this.route.snapshot.paramMap.get('id'));

  ngOnInit(): void {
    this.title = 'Premier League 2018/2019';
  }

  onStandingDetailsClick(TeamStandingsDto: TeamStanding) {
    this.router.navigate(['summary', 'team', TeamStandingsDto.teamId]);
  }

}

