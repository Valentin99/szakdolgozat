import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LeagueService } from 'src/app/services/domain/league/league.service';
import { TeamStanding } from 'src/app/services/domain/league/standing/standing.model';

@Component({
  selector: 'app-standings',
  templateUrl: './standings.component.html',
  styleUrls: ['./standings.component.css']
})
export class StandingsComponent implements OnInit {

  displayedColumns: string[];
  standings: TeamStanding[];
  dataSource: any;

  constructor(private router: Router, private route: ActivatedRoute, private service: LeagueService) { }

  roundId = Number(this.route.snapshot.paramMap.get('id'));

  ngOnInit(): void {
    this.leagueStandings(1,1,this.roundId);
  }

  leagueStandings(leagueId: number, seasonId: number, roundId: number) {
    this.service.getLeagueStandingsById(leagueId,seasonId,roundId).subscribe(val => {
      this.dataSource = val;
      this.displayedColumns = ['position', 'logo', 'teamName', 'playedGames', 'win', 'draw', 'defeat', 'point', 'actions'];
    });
  }

  onStandingDetailsClick(team: TeamStanding) {
    this.router.navigate(['summary', 'team', team.teamId]);
  }

}
