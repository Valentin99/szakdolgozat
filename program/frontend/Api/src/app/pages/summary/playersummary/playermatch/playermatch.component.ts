import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { PlayerMatch } from 'src/app/services/domain/player/match/match.model';
import { PlayerService } from 'src/app/services/domain/player/player.service';

@Component({
  selector: 'app-playermatch',
  templateUrl: './playermatch.component.html',
  styleUrls: ['./playermatch.component.css']
})
export class PlayermatchComponent implements AfterViewInit {

  displayedColumns: string[];
  ELEMENT_DATA: PlayerMatch[];
  dataSource: any;

  constructor(private route: ActivatedRoute, private service: PlayerService) { }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  playerId = Number(this.route.snapshot.paramMap.get('id'));

  ngAfterViewInit() {
    this.service.getPlayerMatchList(this.playerId).subscribe(val => {
      this.ELEMENT_DATA = val;
      this.displayedColumns = ['league', 'gameWeek', 'playedMinutes', 'homeTeam', 'awayTeam', 'fulltime', 'goal', 'yellowCard', 'redCard' ];
      this.dataSource =new MatTableDataSource<PlayerMatch>(this.ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
    });
  }

}
