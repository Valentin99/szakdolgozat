import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayercareerComponent } from './playercareer.component';

describe('PlayercareerComponent', () => {
  let component: PlayercareerComponent;
  let fixture: ComponentFixture<PlayercareerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayercareerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayercareerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
