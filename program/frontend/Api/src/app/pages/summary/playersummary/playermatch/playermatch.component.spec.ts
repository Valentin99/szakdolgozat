import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayermatchComponent } from './playermatch.component';

describe('PlayermatchComponent', () => {
  let component: PlayermatchComponent;
  let fixture: ComponentFixture<PlayermatchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlayermatchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayermatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
