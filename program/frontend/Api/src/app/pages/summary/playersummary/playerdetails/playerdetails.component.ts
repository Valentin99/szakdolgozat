import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayerHeadLine } from 'src/app/services/domain/player/headline/headline.model';
import { PlayerService } from 'src/app/services/domain/player/player.service';

@Component({
  selector: 'app-playerdetails',
  templateUrl: './playerdetails.component.html',
  styleUrls: ['./playerdetails.component.css']
})
export class PlayerdetailsComponent implements OnInit {

  headline: PlayerHeadLine;

  constructor(private route: ActivatedRoute, private service: PlayerService) { }

  playerId = Number(this.route.snapshot.paramMap.get('id'));

  ngOnInit(): void {
    this.service.getPlayerHeadline(this.playerId).subscribe(val => {
      this.headline = val;
    });
  }

}
