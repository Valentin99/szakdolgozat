import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Career } from 'src/app/services/domain/player/career/career.model';
import { PlayerService } from 'src/app/services/domain/player/player.service';

@Component({
  selector: 'app-playercareer',
  templateUrl: './playercareer.component.html',
  styleUrls: ['./playercareer.component.css']
})
export class PlayercareerComponent implements AfterViewInit {

  constructor(private route: ActivatedRoute, private service: PlayerService) { }

  ELEMENT_DATA: Career[];
  dataSource: any;

  displayedColumns: string[] = ['season', 'team', 'league', 'team', 'apps', 'goal', 'yellowCard', 'redCard'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  playerId = Number(this.route.snapshot.paramMap.get('id'));

  ngAfterViewInit() {
      this.service.getPlayerCareer(this.playerId).subscribe(val => {
        this.ELEMENT_DATA = val;
        this.dataSource = new MatTableDataSource<Career>(this.ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      });
   }

}
