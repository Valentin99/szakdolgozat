import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeagueComponent } from './leaguesummary/league/league.component';
import { MatchDetailsComponent } from './matchsummary/matchdetails/matchdetails.component';
import { PlayerdetailsComponent } from './playersummary/playerdetails/playerdetails.component';
import { TeamComponent } from './teamsummary/team/team.component';

const routes: Routes = [
  { path: 'league/:id', component: LeagueComponent },
  { path: 'matchdetails/:id', component: MatchDetailsComponent },
  { path: 'player/:id', component: PlayerdetailsComponent },
  { path: 'team/:id', component: TeamComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SummaryRoutingModule {
}
