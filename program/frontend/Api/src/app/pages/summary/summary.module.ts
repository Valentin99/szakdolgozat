import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryRoutingModule } from './summary-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { GoalscorerlistComponent } from './leaguesummary/goalscorerlist/goalscorerlist.component';
import { LeagueComponent } from './leaguesummary/league/league.component';
import { RedcardComponent } from './leaguesummary/redcard/redcard.component';
import { RoundComponent } from './leaguesummary/round/round.component';
import { MatchDetailsComponent } from './matchsummary/matchdetails/matchdetails.component';
import { MatchlineupComponent } from './matchsummary/matchlineup/matchlineup.component';
import { PlayercareerComponent } from './playersummary/playercareer/playercareer.component';
import { PlayerdetailsComponent } from './playersummary/playerdetails/playerdetails.component';
import { TeamComponent } from './teamsummary/team/team.component';
import { TeamtransferComponent } from './teamsummary/teamtransfer/teamtransfer.component';
import { TeamplayerComponent } from './teamsummary/teamplayer/teamplayer.component';
import { TeammatchComponent } from './teamsummary/teammatch/teammatch.component';
import { YellowcardComponent } from './leaguesummary/yellowcard/yellowcard.component';
import { PlayermatchComponent } from './playersummary/playermatch/playermatch.component';
import { StandingsComponent } from './leaguesummary/standings/standings.component';

@NgModule({
  declarations: [
    LeagueComponent,
    GoalscorerlistComponent,
    RedcardComponent,
    YellowcardComponent,
    MatchDetailsComponent,
    MatchlineupComponent,
    TeamComponent,
    RoundComponent,
    PlayerdetailsComponent,
    PlayercareerComponent,
    TeamtransferComponent,
    TeamplayerComponent,
    TeammatchComponent,
    PlayermatchComponent,
    StandingsComponent,
  ],
  imports: [
    CommonModule,
    SummaryRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ]
})
export class SummaryModule { }
