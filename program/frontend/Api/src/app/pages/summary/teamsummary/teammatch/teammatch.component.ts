import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Match } from 'src/app/services/domain/match/list/match.model';
import { TeamService } from 'src/app/services/domain/team/team.service';

@Component({
  selector: 'app-teammatch',
  templateUrl: './teammatch.component.html',
  styleUrls: ['./teammatch.component.css']
})
export class TeammatchComponent implements OnInit {

  displayedColumns: string[];
  dataSource: any;

  constructor(private router: Router, private route: ActivatedRoute, private service: TeamService) { }

  teamId = Number(this.route.snapshot.paramMap.get('id'));

  ngOnInit(): void {
    this.service.getTeamResultsById(this.teamId).subscribe(val => {
      this.dataSource = val;
      this.displayedColumns = ['homeTeam', 'fulTimeResult', 'awayTeam', 'halfTimeResult', 'actions'];
    });
  }

  onDetailsClick(match: Match) {
    this.router.navigate(['summary', 'matchdetails', match.matchId]);
  }

}
