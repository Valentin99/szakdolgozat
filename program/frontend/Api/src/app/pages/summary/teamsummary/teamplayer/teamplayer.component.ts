import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamPlayer } from 'src/app/services/domain/team/player/player.model';
import { TeamService } from 'src/app/services/domain/team/team.service';

@Component({
  selector: 'app-teamplayer',
  templateUrl: './teamplayer.component.html',
  styleUrls: ['./teamplayer.component.css']
})
export class TeamplayerComponent implements AfterViewInit {
  displayedColumns: string[] = ['yerseyNumber', 'name', 'age', 'apps', 'position', 'goal', 'yellowCard', 'redCard', 'actions'];
  ELEMENT_DATA: TeamPlayer[];
  dataSource: MatTableDataSource<TeamPlayer>;

  constructor(private router: Router, private route: ActivatedRoute, private service: TeamService) { }
  @ViewChild(MatPaginator) paginator: MatPaginator;

  teamId = Number(this.route.snapshot.paramMap.get('id'));

  ngAfterViewInit() {
    this.service.getTeamPlayersById(this.teamId).subscribe(val => {
      this.ELEMENT_DATA = val;
      this.dataSource = new MatTableDataSource<TeamPlayer>(this.ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
    });
  }

  onDetailsClick(player: TeamPlayer) {
    this.router.navigate(['summary', 'player', player.playerId]);
  }

}
