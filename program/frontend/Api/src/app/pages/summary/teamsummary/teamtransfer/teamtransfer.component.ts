import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { TeamService } from 'src/app/services/domain/team/team.service';
import { TeamTransfer } from 'src/app/services/domain/team/transfer/transfer.model';

@Component({
  selector: 'app-teamtransfer',
  templateUrl: './teamtransfer.component.html',
  styleUrls: ['./teamtransfer.component.css']
})
export class TeamtransferComponent implements AfterViewInit {
  ELEMENT_DATA: TeamTransfer[];
  dataSource: any;

  constructor(private route: ActivatedRoute, private service: TeamService) { }

  displayedColumns: string[] = ['name', 'movement', 'from', 'to'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  teamId = Number(this.route.snapshot.paramMap.get('id'));

  ngAfterViewInit() {
    this.service.getTeamTransfersById(this.teamId).subscribe(val => {
      console.log(val);
      this.ELEMENT_DATA = val;
      this.dataSource = new MatTableDataSource<TeamTransfer>(this.ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
    });
  }

}
