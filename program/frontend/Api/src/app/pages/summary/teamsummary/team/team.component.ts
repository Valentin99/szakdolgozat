import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeamHeadline } from 'src/app/services/domain/team/headline/headline.model';
import { TeamService } from 'src/app/services/domain/team/team.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  headline: TeamHeadline;

  constructor(private route: ActivatedRoute, private service: TeamService) { }

  teamId = Number(this.route.snapshot.paramMap.get('id'));

  ngOnInit(): void {
    this.service.getTeamHeadlineById(this.teamId).subscribe(val => {
      this.headline = val;
    });
  }

}
