import { Position } from 'src/app/services/domain/match/position/position.model';

export class Simulation {

    private readonly positions: Position[];

    constructor(private ctx: CanvasRenderingContext2D, private team: Position[]) {
        this.positions = team;
    }

    draw() {

        // this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

        this.drawPitch();
        this.drawPlayers(this.positions);
    }

    private drawPlayers(positions: Position[]) {

        for (const player of positions) {

            this.ctx.beginPath();
            this.ctx.arc(player.x, player.y, 10, 0, 2 * Math.PI, false);
            this.ctx.lineWidth = 4;

            if (player.type === 'home') {
                this.ctx.fillStyle = 'blue';
            } else if (player.type === 'away') {
                this.ctx.fillStyle = 'red';
            }

            this.ctx.fill();
            this.ctx.stroke();
            this.ctx.closePath();

        }

    }

    private drawPitch() {

        const height = 500;
        const width = 800;

        // Outer lines
        this.ctx.beginPath();
        this.ctx.rect(0, 0, width, height);
        this.ctx.fillStyle = '#060';
        this.ctx.fill();
        this.ctx.lineWidth = 1;
        this.ctx.strokeStyle = '#FFF';
        this.ctx.stroke();
        this.ctx.closePath();

        this.ctx.fillStyle = '#FFF';

        // Mid line
        this.ctx.beginPath();
        this.ctx.moveTo(width / 2, 0);
        this.ctx.lineTo(width / 2, height);
        this.ctx.stroke();
        this.ctx.closePath();

        // Mid circle
        this.ctx.beginPath();
        this.ctx.arc(width / 2, height / 2, 73, 0, 2 * Math.PI, false);
        this.ctx.stroke();
        this.ctx.closePath();
        // Mid point
        this.ctx.beginPath();
        this.ctx.arc(width / 2, height / 2, 2, 0, 2 * Math.PI, false);
        this.ctx.fill();
        this.ctx.closePath();

        // Home penalty box
        this.ctx.beginPath();
        this.ctx.rect(0, (height - 322) / 2, 132, 322);
        this.ctx.stroke();
        this.ctx.closePath();
        // Home goal box
        this.ctx.beginPath();
        this.ctx.rect(0, (height - 146) / 2, 44, 146);
        this.ctx.stroke();
        this.ctx.closePath();
        // Home goal
        this.ctx.beginPath();
        this.ctx.moveTo(1, (height / 2) - 22);
        this.ctx.lineTo(1, (height / 2) + 22);
        this.ctx.lineWidth = 2;
        this.ctx.stroke();
        this.ctx.closePath();
        this.ctx.lineWidth = 1;

        // Home penalty point
        this.ctx.beginPath();
        this.ctx.arc(88, height / 2, 1, 0, 2 * Math.PI, true);
        this.ctx.fill();
        this.ctx.closePath();
        // Home half circle
        this.ctx.beginPath();
        this.ctx.arc(88, height / 2, 73, 0.29 * Math.PI, 1.71 * Math.PI, true);
        this.ctx.stroke();
        this.ctx.closePath();

        // Away penalty box
        this.ctx.beginPath();
        this.ctx.rect(width - 132, (height - 322) / 2, 132, 322);
        this.ctx.stroke();
        this.ctx.closePath();
        // Away goal box
        this.ctx.beginPath();
        this.ctx.rect(width - 44, (height - 146) / 2, 44, 146);
        this.ctx.stroke();
        this.ctx.closePath();
        // Away goal
        this.ctx.beginPath();
        this.ctx.moveTo(width - 1, (height / 2) - 22);
        this.ctx.lineTo(width - 1, (height / 2) + 22);
        this.ctx.lineWidth = 2;
        this.ctx.stroke();
        this.ctx.closePath();
        this.ctx.lineWidth = 1;
        // Away penalty point
        this.ctx.beginPath();
        this.ctx.arc(width - 88, height / 2, 1, 0, 2 * Math.PI, true);
        this.ctx.fill();
        this.ctx.closePath();
        // Away half circle
        this.ctx.beginPath();
        this.ctx.arc(width - 88, height / 2, 73, 0.71 * Math.PI, 1.29 * Math.PI, false);
        this.ctx.stroke();
        this.ctx.closePath();

        // Home L corner
        this.ctx.beginPath();
        this.ctx.arc(0, 0, 8, 0, 0.5 * Math.PI, false);
        this.ctx.stroke();
        this.ctx.closePath();
        // Home R corner
        this.ctx.beginPath();
        this.ctx.arc(0, height, 8, 0, 2 * Math.PI, true);
        this.ctx.stroke();
        this.ctx.closePath();
        // Away R corner
        this.ctx.beginPath();
        this.ctx.arc(width, 0, 8, 0.5 * Math.PI, 1 * Math.PI, false);
        this.ctx.stroke();
        this.ctx.closePath();
        // Away L corner
        this.ctx.beginPath();
        this.ctx.arc(width, height, 8, 1 * Math.PI, 1.5 * Math.PI, false);
        this.ctx.stroke();
        this.ctx.closePath();

    }

}
