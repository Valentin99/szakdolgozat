import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatchService } from 'src/app/services/domain/match/match.service';
import { SimulationHeadLine } from 'src/app/services/domain/simulation/headline/headline.model';
import { Comment } from 'src/app/services/domain/match/comment/comment.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatchReport } from 'src/app/services/domain/match/report/report.model';
import { MatchStatistics } from 'src/app/services/domain/match/statistics/statistics.model';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-matchdetails',
  templateUrl: './matchdetails.component.html',
  styleUrls: ['./matchdetails.component.css']
})
export class MatchDetailsComponent implements OnInit {

  matchId: number;
  minute: number;
  second: number;
  destroy = new Subject();
  headline: SimulationHeadLine;
  time: string;
  result: string;

  report: MatchReport[]
  home1: MatchReport[];
  away1: MatchReport[];
  home2: MatchReport[];
  away2: MatchReport[];

  dataSource: any;
  displayedColumns: string[];

  statistics: MatchStatistics[];
  statisticsHeadline: string[];

  comment: Comment[];

  simulation: boolean;

  rxjsTimer = timer(10, 20);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private router: Router, private route: ActivatedRoute, private matchService: MatchService) {
    this.minute = 0;
    this.second = 0;
  }

  ngOnInit() {
    this.matchId = Number(this.route.snapshot.paramMap.get('id'));
    this.result = '0 : 0';
    this.simulation = true;
    this.matchheadline();
    this.clock();
  }

  commentar(matchId: number, start: number, end: number) {
    this.matchService.getCommentsById(matchId, start, end).subscribe(val => {
      this.comment = val;
      this.displayedColumns = ['minute', 'description'];
      this.dataSource = new MatTableDataSource<Comment>(this.comment);
      this.dataSource.paginator = this.paginator;
    });
  }

  matchReport() {
    this.matchService.getMatchReportById(this.matchId, this.minute).subscribe(val => {
      this.report = val;
      this.home1 = this.report.filter(x => x.type === 'home1');
      this.away1 = this.report.filter(x => x.type === 'away1');
      this.home2 = this.report.filter(x => x.type === 'home2');
      this.away2 = this.report.filter(x => x.type === 'away2');
    });
  }

  matchStatistics() {
    this.matchService.getMatchStatisticsById(this.matchId, this.minute).subscribe(val => {
      this.statistics = val;
      this.statisticsHeadline = ['homeTeam', 'type', 'awayTeam'];
    });
  }

  onDetailsClick(player: MatchReport) {
    this.router.navigate(['summary', 'player', player.playerId]);
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  matchResult() {
    this.matchService.getMatchResult(this.matchId, this.minute).subscribe(val => {
      this.result = val.result;
    });
  }

  matchheadline() {
    this.matchService.getMatchHeadline(this.matchId).subscribe(val => {
      this.headline = val;
    });
  }

  clock() {
      this.rxjsTimer.pipe(takeUntil(this.destroy)).subscribe(val => {

        if (this.simulation === true) {

          if (val % 60 === 0) {
            val -= 60;
            this.second = 0;
            this.minute++;
          } else {
            this.second++;
          }
    
          if (this.minute % 5 === 0 && this.second === 0) {
            this.matchResult();
            this.matchReport();
            this.matchStatistics();
            this.commentar(this.matchId, this.minute - 5, this.minute);
          }
          
          if (this.minute !== 90) {
            this.time = this.minute + ':' + this.second;
            if (this.minute === 45) {
              this.time = 'Félidő';
              this.delay(1000);
            }
          } else {
            this.time = 'Vége';
            this.simulation = false;
            this.destroy.next();
            this.destroy.complete();
          }

        }

      });
  }

}
