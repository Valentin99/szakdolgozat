import { AfterViewInit, Component, ElementRef, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Lineup } from 'src/app/services/domain/match/lineup/lineup.model';
import { MatchService } from 'src/app/services/domain/match/match.service';
import { Position } from 'src/app/services/domain/match/position/position.model';
import { Simulation } from './simulation';

@Component({
  selector: 'app-matchlineup',
  templateUrl: './matchlineup.component.html',
  styleUrls: ['./matchlineup.component.css']
})
export class MatchlineupComponent implements OnInit, AfterViewInit {

  team: Lineup[];
  home11: Lineup[];
  away11: Lineup[];
  homesub: Lineup[];
  awaysub: Lineup[];

  positions: Position[];

  @ViewChild('canvas') canvas: ElementRef;

  ctx: CanvasRenderingContext2D;
  simulation: Simulation;

  constructor(private route: ActivatedRoute, private router: Router, private service: MatchService ) { }
  ngOnInit(): void {
    this.lineup()
  }

  matchId = Number(this.route.snapshot.paramMap.get('id'));

  homeFormation = '4-4-2';
  awayFormation = '4-4-2';

  onDetailsClick(player: Lineup) {
    this.router.navigate(['summary', 'player', player.playerId]);
  }

  ngAfterViewInit() {
    this.service.getPlayerPositionList(this.matchId).subscribe(val => {
      this.ctx = this.canvas.nativeElement.getContext('2d');
      this.positions = val;
      this.simulation = new Simulation(this.ctx, this.positions);
      this.simulation.draw();
    });
  }

  lineup() {
    this.service.getLineupById(this.matchId).subscribe(val => {
      this.team = val;
      this.home11 = this.team.filter(x => x.type === 'home11');
      this.away11 = this.team.filter(x => x.type === 'away11');
      this.homesub = this.team.filter(x => x.type === 'homesub');
      this.awaysub = this.team.filter(x => x.type === 'awaysub');
    });

  }

}
