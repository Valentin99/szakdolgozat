import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchlineupComponent } from './matchlineup.component';

describe('MatchlineupComponent', () => {
  let component: MatchlineupComponent;
  let fixture: ComponentFixture<MatchlineupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchlineupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchlineupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
