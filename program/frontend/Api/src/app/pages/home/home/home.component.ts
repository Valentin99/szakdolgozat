import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SimulationData, SimulationDTO } from 'src/app/services/domain/home/simulation';
import { Match } from 'src/app/services/domain/match/list/match.model';
import { RoundMatch } from 'src/app/services/domain/simulation/round/round.model';
import { SimulationService } from 'src/app/services/domain/simulation/simulation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  ELEMENT_DATA: RoundMatch[];
  title: string;
  form: boolean;
  isLoading: boolean;

  seasons: SimulationData[] = [
    { id: 1, value: 1, text: '2018/2019' },
  ];

  leagues: SimulationData[] = [
    { id: 1, value: 1, text: 'Premier League' },
  ];

  rounds: SimulationData[] = [
    { id: 1, value: 1, text: '' },
    { id: 2, value: 2, text: '' },
    { id: 3, value: 3, text: '' },
    { id: 4, value: 4, text: '' },
    { id: 5, value: 5, text: '' },
    { id: 6, value: 6, text: '' },
    { id: 7, value: 7, text: '' },
    { id: 8, value: 8, text: '' },
    { id: 9, value: 9, text: '' },
  ];

  simulationFormGroup = new FormGroup({
    seasonId: new FormControl('', Validators.required),
    leagueId: new FormControl('', Validators.required),
    roundId: new FormControl('', Validators.required),
  });
  simulation: SimulationDTO;

  displayedColumns: string[] = ['status', 'homeTeam', 'fulltime', 'awayTeam', 'halftime', 'actions'];
  dataSource: any;

  constructor(private router: Router, private service: SimulationService) { }

  ngOnInit(): void {
    this.form = true;
    this.title = 'Szimuláció';
    this.isLoading = false;
  }

  onDetailsClick(match: Match) {
    this.redirect(match);
  }

  check = () => {
    return new Promise((resolve, reject) => {
      this.isLoading = true;
      setTimeout(() => {
        resolve(true);
      }, 1000);
    });
  };

  createEvents = (match: Match) => {
    return new Promise((resolve, reject) => {
      this.isLoading = true;
      setTimeout(() => {
        //this.service.CreateMatchSimulation(match.matchId).subscribe();
        resolve(true);
      }, 500);
    });
  };

  redirect = (match: Match) => {
    this.check()
    .then(val => {
      if (val) {
        return this.createEvents(match);
      }
    })
    .then(
      val => {
        this.router.navigate(['summary', 'matchdetails', match.matchId]);
      }
    )
  }

  createSimulation = () => {
    this.simulation = {
      seasonId: this.simulationFormGroup.get('seasonId').value,
      leagueId: this.simulationFormGroup.get('leagueId').value,
      roundId: this.simulationFormGroup.get('roundId').value
    };

    this.service.CreateRoundSimulation(this.simulation.roundId).subscribe(val => {
      this.ELEMENT_DATA = val;
      this.dataSource = this.ELEMENT_DATA;
      this.form = false;
    });

    this.service.GetSimulationPageHeadline(this.simulation.roundId).subscribe(val => {
      this.title = val.title;
    });


    this.isLoading = true;

    setTimeout(() => {
      this.isLoading = false;
    }, 1000);
  }

  back = () => {
    this.form = true;
    this.title = 'Szimuláció';
  }

  leagueInfo = () => {
    this.router.navigate(['summary', 'league', this.simulation.roundId]);
  }

}

