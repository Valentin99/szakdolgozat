export interface SimulationPageHeadlineDTO {
    title: string;
}

export interface GetSimulationPageHeadlineRequest {
    roundId: number;
}

export interface SimulationHeadLineDTO {
    matchId: number;
    hometeamId: number;
    awayteamId: number;
    home: string;
    away: string;
    homeLogo: string;
    awayLogo: string;
}

export interface GetMatchHeadlineRequest {
    matchId: number;
}

export interface MatchResultDTO {
    result: string;
}

export interface GetMatchResultRequest {
    matchId: number;
    minute: number;
}
