import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GetMatchHeadlineRequest, GetMatchResultRequest, MatchResultDTO, SimulationHeadLineDTO, SimulationPageHeadlineDTO } from './headline/headline';
import { toMatchResult, toSimulationHeadLine, toSimulationPageHeadLine } from './headline/headline.model';
import { RoundMatchDTO } from './round/round';
import { toRoundMatchList } from './round/round.model';
import { SimulationController } from './simulation-controller.service';

@Injectable({
    providedIn: 'root',
})
export class SimulationService {

    constructor(protected controller: SimulationController) { }

    CreateRoundSimulation(roundId: number): Observable<RoundMatchDTO[]> {
        return this.controller.createRoundSimulation({roundId}).pipe(map((response: RoundMatchDTO[]) => {
            return response ? toRoundMatchList(response) : null;
        }));
    }

    GetSimulationPageHeadline(roundId: number): Observable<SimulationPageHeadlineDTO> {
        return this.controller.getSimulationPageHeadline({ roundId }).pipe(map((response: SimulationPageHeadlineDTO) => {
            return response ? toSimulationPageHeadLine(response) : null;
        }));
    }

    GenerateFault(matchId: number) {
        return this.controller.generateFault({ matchId }).pipe(map(response => {
            return response;
        }));
    }

    GenerateShot(matchId: number) {
        return this.controller.generateShot({matchId}).pipe(map(response => {
            return response;
        }));
    }

    CreateMatchSimulation(matchId: number): Observable<{}> {
        return this.controller.createMatchSimulation({matchId}).pipe(map((response: {}) => {
            return response ? response : null;
        }));
    }

    GetMatchHeadline(param: GetMatchHeadlineRequest): Observable<SimulationHeadLineDTO> {
        return this.controller.getMatchHeadline(param).pipe(map((response: SimulationHeadLineDTO) => {
            return response ? toSimulationHeadLine(response) : null;
        }));
    }

    GetMatchResult(param: GetMatchResultRequest): Observable<MatchResultDTO> {
        return this.controller.getMatchResult(param).pipe(map((response: MatchResultDTO) => {
            return response ? toMatchResult(response) : null;
        }));
    }

}
