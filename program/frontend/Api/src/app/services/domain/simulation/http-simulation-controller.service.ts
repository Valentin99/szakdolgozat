import { HttpHeaders, HttpParameterCodec, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GenereateFaulsRequest } from './fault/fault';
import { GetSimulationPageHeadlineRequest, SimulationPageHeadlineDTO, GetMatchHeadlineRequest, SimulationHeadLineDTO, GetMatchResultRequest, MatchResultDTO } from './headline/headline';
import { CreateMatchSimulationRequest } from './match/match';
import { CreateRoundSimulationRequest, RoundMatchDTO } from './round/round';
import { GenerateShotRequest } from './shot/shot';
import { SimulationController } from './simulation-controller.service';

@Injectable()
export class HttpSimulationController implements SimulationController {
    private readonly BASE_URL = `https://localhost:5001/api/simulation`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }
    public createRoundSimulation(request: CreateRoundSimulationRequest): Observable<RoundMatchDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/round`, request).pipe(
            map((res: RoundMatchDTO[]) => res)
        );
    }
    public getSimulationPageHeadline(request: GetSimulationPageHeadlineRequest): Observable<SimulationPageHeadlineDTO> {
        return this.httpClient.post(`${this.BASE_URL}/page`, request).pipe(
            map((res: SimulationPageHeadlineDTO) => res)
        );
    }
    public generateFault(request: GenereateFaulsRequest): Observable<number> {
        return this.httpClient.post(`${this.BASE_URL}/fault`, request).pipe(
            map((res: number) => res)
        );
    }
    public generateShot(request: GenerateShotRequest) {
        return this.httpClient.post(`${this.BASE_URL}/shot`, request).pipe(
            map((res: number) => res)
        );
    }
    public createMatchSimulation(request: CreateMatchSimulationRequest): Observable<boolean> {
        return this.httpClient.post(`${this.BASE_URL}/match`, request).pipe(
            map((res: boolean) => res)
        );
    }
    public getMatchHeadline(request: GetMatchHeadlineRequest): Observable<SimulationHeadLineDTO> {
        return this.httpClient.post(`${this.BASE_URL}/headline`, request).pipe(
            map((res: SimulationHeadLineDTO) => res)
        );
    }
    public getMatchResult(request: GetMatchResultRequest): Observable<MatchResultDTO> {
        return this.httpClient.post(`${this.BASE_URL}/result`, request).pipe(
            map((res: MatchResultDTO) => res)
        );
    }
}
