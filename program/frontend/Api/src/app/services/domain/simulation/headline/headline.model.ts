import { SimulationHeadLineDTO, SimulationPageHeadlineDTO, MatchResultDTO } from './headline';

export interface MatchResult {
    result: string;
}

export interface SimulationPageHeadline {
    title: string;
}

export interface SimulationHeadLine {
    matchId: number;
    hometeamId: number;
    awayteamId: number;
    home: string;
    away: string;
    homeLogo: string;
    awayLogo: string;
}

export function toMatchResult(matchResultDTO: MatchResultDTO): MatchResult {
    return {
        result: matchResultDTO.result
    };
}

export function toSimulationPageHeadLine(simulationPageHeadlineDTO: SimulationPageHeadlineDTO): SimulationPageHeadline {
    return {
        title: simulationPageHeadlineDTO.title
    };
}

export function toSimulationHeadLine(simulationHeadLieDTO: SimulationHeadLineDTO): SimulationHeadLine {
    return {
        matchId: simulationHeadLieDTO.matchId,
        hometeamId: simulationHeadLieDTO.hometeamId,
        awayteamId: simulationHeadLieDTO.awayteamId,
        home: simulationHeadLieDTO.home,
        away: simulationHeadLieDTO.away,
        homeLogo: simulationHeadLieDTO.homeLogo,
        awayLogo: simulationHeadLieDTO.awayLogo
    };
}
