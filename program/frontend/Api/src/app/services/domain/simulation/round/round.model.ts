import { RoundMatchDTO } from './round';

export interface RoundMatch {
    matchId: number;
    status: string;
    homeTeam: string;
    awayTeam: string;
    halftime: string;
    fulltime: string;
}

export function toRoundMatchList(roundMatchResponse: RoundMatchDTO[]): RoundMatch[] {
    return roundMatchResponse.map(dto => toRoundMatch(dto));
}

export function toRoundMatch(roundMatchDTO: RoundMatchDTO): RoundMatch {
    return {
        matchId: roundMatchDTO.matchId,
        status: roundMatchDTO.status,
        homeTeam: roundMatchDTO.homeTeam,
        awayTeam: roundMatchDTO.awayTeam,
        halftime: roundMatchDTO.halftime,
        fulltime: roundMatchDTO.fulltime
    };
}
