import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Fault, GenereateFaulsRequest } from './fault/fault';
import { GetMatchHeadlineRequest, GetMatchResultRequest, GetSimulationPageHeadlineRequest, MatchResultDTO, SimulationHeadLineDTO, SimulationPageHeadlineDTO } from './headline/headline';
import { CreateMatchSimulationRequest } from './match/match';
import { CreateRoundSimulationRequest, RoundMatchDTO } from './round/round';
import { GenerateShotRequest, Shot } from './shot/shot';

@Injectable()
export abstract class SimulationController {
    public abstract generateFault(request: GenereateFaulsRequest): Observable<number>;
    public abstract generateShot(request: GenerateShotRequest): Observable<number>;
    public abstract createMatchSimulation(request: CreateMatchSimulationRequest): Observable<boolean>;
    public abstract createRoundSimulation(request: CreateRoundSimulationRequest): Observable<RoundMatchDTO[]>;
    public abstract getSimulationPageHeadline(request: GetSimulationPageHeadlineRequest): Observable<SimulationPageHeadlineDTO>;
    public abstract getMatchHeadline(request: GetMatchHeadlineRequest): Observable<SimulationHeadLineDTO>;
    public abstract getMatchResult(request: GetMatchResultRequest): Observable<MatchResultDTO>;
}
