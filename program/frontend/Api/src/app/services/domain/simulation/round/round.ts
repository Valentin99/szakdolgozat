export interface RoundMatchDTO {
    matchId: number;
    status: string;
    homeTeam: string;
    awayTeam: string;
    halftime: string;
    fulltime: string;
}

export interface CreateRoundSimulationRequest {
    roundId: number;
}
