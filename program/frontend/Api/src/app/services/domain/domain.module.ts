import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LeagueController } from './league/league-controller.service';
import { Optional } from '@angular/core';
import { SkipSelf } from '@angular/core';
import { MatchController } from './match/match-controller.service';
import { HttpMatchController } from './match/http-match-controller.service';
import { PlayerController } from './player/player-controller.service';
import { TeamController } from './team/team-controller.service';
import { HttpTeamController } from './team/http-team-controller.service';
import { SimulationController } from './simulation/simulation-controller.service';
import { HttpSimulationController } from './simulation/http-simulation-controller.service';
import { HttpLeagueController } from './league/http-league-controller.service';
import { HttpPlayerController } from './player/http-player-controller.service';
import { TeamSettingsController } from './settings/team-settings-controller.service';
import { TeamHttpSettingsController } from './settings/team-htttp-simulation-controller.service';
import { PlayerHttpSettingsController } from './settings/player-http-simulation-controller.service';
import { PlayerSettingsController } from './settings/player-settings-controller.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers:
    [
      { provide: LeagueController, useClass: HttpLeagueController },
      { provide: MatchController, useClass: HttpMatchController },
      { provide: PlayerController, useClass: HttpPlayerController },
      { provide: TeamController, useClass: HttpTeamController },
      { provide: SimulationController, useClass: HttpSimulationController },
      { provide: TeamSettingsController, useClass: TeamHttpSettingsController },
      { provide: PlayerSettingsController, useClass: PlayerHttpSettingsController }
    ]
})
export class DomainModule {
  constructor(@Optional() @SkipSelf() parentModule: DomainModule) {
    if (parentModule) {
      throw new Error(
        'DomainModule is already loaded. Import it in the AppModule only');
    }
  }
}
