import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CareerDTO, GetPlayerCareerByIdRequest } from './career/career';
import { GetPlayerHeadlineByIdRequest } from './headline/headline';
import { PlayerHeadLine } from './headline/headline.model';
import { GetPlayerMatchListBydRequest, PlayerMatchDTO } from './match/match';
import { GetPlayerTransfersByIdRequest, PlayerTransferDTO } from './transfer/transfer';

@Injectable()
export abstract class PlayerController {
    public abstract getPlayerCareerById(request: GetPlayerCareerByIdRequest): Observable<CareerDTO[]>;
    public abstract getPlayerHeadlineById(request: GetPlayerHeadlineByIdRequest): Observable<PlayerHeadLine>;
    public abstract getPlayerMatchListById(request: GetPlayerMatchListBydRequest): Observable<PlayerMatchDTO[]>;
    public abstract getPlayerTransfersById(request: GetPlayerTransfersByIdRequest): Observable<PlayerTransferDTO[]>;
}
