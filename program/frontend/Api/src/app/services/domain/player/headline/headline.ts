export interface PlayerHeadLineDTO {
    name: string;
    position: string;
    age: string;
    teamLogo: string;
}

export interface GetPlayerHeadlineByIdRequest {
    playerId: number;
}


