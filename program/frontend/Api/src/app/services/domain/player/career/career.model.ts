import { CareerDTO } from './career';

export interface Career {
    playerId: number;
    season: string;
    league: string;
    team: string;
    apps: number;
    goal: number;
    yellowCard: number;
    redCard: number;
}

export function toCareerList(careerResponse: CareerDTO[]): Career[] {
    return careerResponse.map(dto => toCareer(dto));
}

export function toCareer(careerDTO: CareerDTO): Career {
    return {
        playerId: careerDTO.playerId,
        season: careerDTO.season,
        league: careerDTO.league,
        team: careerDTO.team,
        apps: careerDTO.apps,
        goal: careerDTO.goal,
        yellowCard: careerDTO.yellowCard,
        redCard: careerDTO.redCard
    };
}
