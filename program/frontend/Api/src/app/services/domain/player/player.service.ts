import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CareerDTO, GetPlayerCareerByIdRequest } from './career/career';
import { toCareerList } from './career/career.model';
import { GetPlayerHeadlineByIdRequest, PlayerHeadLineDTO } from './headline/headline';
import { PlayerHeadLine, toHeadline } from './headline/headline.model';
import { GetPlayerMatchListBydRequest, PlayerMatchDTO } from './match/match';
import { toPlayerMatchList } from './match/match.model';
import { PlayerController } from './player-controller.service';
import { GetPlayerTransfersByIdRequest, PlayerTransferDTO } from './transfer/transfer';
import { toPlayerTransferList } from './transfer/transfer.model';

@Injectable({
    providedIn: 'root',
})
export class PlayerService {

    constructor(protected controller: PlayerController) { }

    getPlayerCareer(playerId: number): Observable<CareerDTO[]> {
        return this.controller.getPlayerCareerById({playerId}).pipe(map((response: CareerDTO[]) => {
            return response ? toCareerList(response) : null;
        }));
    }

    getPlayerHeadline(playerId: number): Observable<PlayerHeadLine> {
        return this.controller.getPlayerHeadlineById({playerId}).pipe(map((response: PlayerHeadLineDTO) => {
            return response ? toHeadline(response) : null;
        }));
    }

    getPlayerMatchList(playerId: number): Observable<PlayerMatchDTO[]> {
        return this.controller.getPlayerMatchListById({playerId}).pipe(map((response: PlayerMatchDTO[]) => {
            return response ? toPlayerMatchList(response) : null;
        }));
    }

}

