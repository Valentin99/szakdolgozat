import { PlayerTransferDTO } from './transfer';

export interface PlayerTransfer {
    playerId: number;
    name: string;
    season: string;
    from: string;
    to: string;
    type: string;
}

export function toPlayerTransferList(transferResponse: PlayerTransferDTO[]): PlayerTransfer[] {
    return transferResponse.map(dto => toTransfer(dto));
}

export function toTransfer(transferDTO: PlayerTransferDTO): PlayerTransfer {
    return {
        playerId: transferDTO.playerId,
        name: transferDTO.name,
        season: transferDTO.season,
        from: transferDTO.from,
        to: transferDTO.to,
        type: transferDTO.type
    };
}
