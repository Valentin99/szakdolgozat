export interface PlayerMatchDTO {
    matchId: number;
    playerId: number;
    playedMinutes: number;
    gameWeek: string;
    league: string;
    homeTeam: string;
    awayTeam: string;
    fulltime: string;
    goal: number;
    yellowCard: number;
    redCard: number;
}

export interface GetPlayerMatchListBydRequest {
    playerId: number;
}
