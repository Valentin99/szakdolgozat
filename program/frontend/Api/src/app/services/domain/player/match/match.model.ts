import { PlayerMatchDTO } from './match';

export interface PlayerMatch {
    matchId: number;
    playerId: number;
    playedMinutes: number;
    gameWeek: string;
    league: string;
    homeTeam: string;
    awayTeam: string;
    fulltime: string;
    goal: number;
    yellowCard: number;
    redCard: number;
}

export function toPlayerMatchList(playerMatchResponse: PlayerMatchDTO[]): PlayerMatch[] {
    return playerMatchResponse.map(dto => toMatch(dto));
}

export function toMatch(playerMatchDTO: PlayerMatchDTO): PlayerMatch {
    return {
        matchId: playerMatchDTO.matchId,
        playedMinutes: playerMatchDTO.playedMinutes,
        playerId: playerMatchDTO.playerId,
        gameWeek: playerMatchDTO.gameWeek,
        league: playerMatchDTO.league,
        homeTeam: playerMatchDTO.homeTeam,
        awayTeam: playerMatchDTO.awayTeam,
        fulltime: playerMatchDTO.fulltime,
        goal: playerMatchDTO.goal,
        yellowCard: playerMatchDTO.yellowCard,
        redCard: playerMatchDTO.redCard
    };
}
