export interface CareerDTO {
    playerId: number;
    season: string;
    league: string;
    team: string;
    apps: number;
    goal: number;
    yellowCard: number;
    redCard: number;
}

export interface GetPlayerCareerByIdRequest {
    playerId: number;
}
