import { HttpHeaders, HttpParameterCodec, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GetPlayerCareerByIdRequest, CareerDTO } from './career/career';
import { GetPlayerHeadlineByIdRequest } from './headline/headline';
import { PlayerHeadLine } from './headline/headline.model';
import { GetPlayerMatchListBydRequest, PlayerMatchDTO } from './match/match';
import { PlayerController } from './player-controller.service';
import { GetPlayerTransfersByIdRequest, PlayerTransferDTO } from './transfer/transfer';

@Injectable()
export class HttpPlayerController implements PlayerController {
    private readonly BASE_URL = `https://localhost:5001/api/player`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }
    public getPlayerCareerById(request: GetPlayerCareerByIdRequest): Observable<CareerDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/career`, request).pipe(
            map((res: CareerDTO[]) => res)
        );
    }
    public getPlayerHeadlineById(request: GetPlayerHeadlineByIdRequest): Observable<PlayerHeadLine> {
        return this.httpClient.post(`${this.BASE_URL}/headline`, request).pipe(
            map((res: PlayerHeadLine) => res)
        );
    }
    public getPlayerMatchListById(request: GetPlayerMatchListBydRequest): Observable<PlayerMatchDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/list`, request).pipe(
            map((res: PlayerMatchDTO[]) => res)
        );
    }
    public getPlayerTransfersById(request: GetPlayerTransfersByIdRequest): Observable<PlayerTransferDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/transfer`, request).pipe(
            map((res: PlayerTransferDTO[]) => res)
        );
    }

}
