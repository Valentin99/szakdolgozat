import { PlayerHeadLineDTO } from './headline';

export interface PlayerHeadLine {
    name: string;
    position: string;
    age: string;
    teamLogo: string;
}

export function toHeadline(playerHeadLineDTO: PlayerHeadLineDTO): PlayerHeadLine {
    return {
        name: playerHeadLineDTO.name,
        position: playerHeadLineDTO.position,
        age: playerHeadLineDTO.age,
        teamLogo: playerHeadLineDTO.teamLogo
    };
}
