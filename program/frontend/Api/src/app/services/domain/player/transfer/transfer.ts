export interface PlayerTransferDTO {
    playerId: number;
    name: string;
    season: string;
    from: string;
    to: string;
    type: string;
}

export interface GetPlayerTransfersByIdRequest {
    playerId: number;
}
