import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { CreatePlayerDTO, GetPlayerByIdRequest, PlayerDTO, UpdatePlayerDTO } from "./player/player-settings";

@Injectable()
export abstract class PlayerSettingsController {
    public abstract createPlayer(request: CreatePlayerDTO);
    public abstract deletePlayer(request: GetPlayerByIdRequest);
    public abstract updatePlayer(request: UpdatePlayerDTO): Observable<string>;
    public abstract getPlayers(): Observable<PlayerDTO[]>;
}