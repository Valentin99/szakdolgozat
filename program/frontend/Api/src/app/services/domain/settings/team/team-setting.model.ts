import { TeamDTO } from "./team-settings";

export interface Team {
    teamId: number;
    name: string;
    nickName: string;
    logo: string;
    stadium: string;
    formation: string
}

export function toTeams(teamResponse: TeamDTO[]): Team[] {
    return teamResponse.map(toTeam);
}

export function toTeam(teamDTO: TeamDTO): Team {
    return {
        teamId: teamDTO.teamId,
        name: teamDTO.name,
        nickName: teamDTO.nickName,
        logo: teamDTO.logo,
        stadium: teamDTO.stadium,
        formation: teamDTO.formation,
    };
}