import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { PlayerSettingsController } from "./player-settings-controller.service";
import { PlayerDTO } from "./player/player-settings";
import { toPlayers } from "./player/player-settings.model";

@Injectable({
    providedIn: 'root',
})
export class PlayerSettingsService {

    constructor(protected controller: PlayerSettingsController) { }

    createPlayer(teamId: number, name: string, nickName: string, age: number, birthday: string, nationality: string, marketValue: string, teamPosition: string, teamYerseyNumber: number) {
        return this.controller.createPlayer({ teamId, name, nickName, age, birthday, nationality, marketValue, teamPosition, teamYerseyNumber }).pipe();
    }

    deletePlayer(playerId: number) {
        return this.controller.deletePlayer({ playerId }).pipe();
    }

    updatePlayer(playerId: number, teamId: number, name: string, nickName: string, age: number, birthday: string, nationality: string, marketValue: string, teamPosition: string, teamYerseyNumber: number) {
        return this.controller.updatePlayer({ playerId, teamId, name, nickName, age, birthday, nationality, marketValue, teamPosition, teamYerseyNumber }).pipe();
    }

    getPlayers() {
        return this.controller.getPlayers().pipe(map((response: PlayerDTO[]) => {
            return response ? toPlayers(response) : null;
        }));
    }

}