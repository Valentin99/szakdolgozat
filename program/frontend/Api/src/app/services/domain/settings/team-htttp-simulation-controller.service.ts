import { HttpHeaders, HttpParameterCodec, HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { GetPlayerByIdRequest } from "./player/player-settings";
import { TeamSettingsController } from "./team-settings-controller.service";
import { CreateTeamDTO, GetTeamByIdRequest, TeamDTO, UpdateTeamDTO } from "./team/team-settings";

@Injectable()
export class TeamHttpSettingsController implements TeamSettingsController {
    private readonly BASE_URL = `https://localhost:5001/api/team`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }  
    public deleteTeam(id: GetTeamByIdRequest) {
        return this.httpClient.delete(`${this.BASE_URL}/${id.teamId}`).pipe();
    }
    public updateTeam(request: UpdateTeamDTO): Observable<string> {
        return this.httpClient.put(`${this.BASE_URL}/${request.teamId}`, request).pipe(
            map((res: string) => res)
        );
    }
    public getTeams(): Observable<TeamDTO[]> {
        return this.httpClient.get(`${this.BASE_URL}`).pipe(
            map((res: TeamDTO[]) => res)
        );
    }
    public createTeam(request: TeamDTO) {
        return this.httpClient.post(`${this.BASE_URL}`, request).pipe();
    }
}
