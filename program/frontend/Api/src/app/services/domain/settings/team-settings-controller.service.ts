import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { GetPlayerByIdRequest } from "./player/player-settings";
import { CreateTeamDTO, GetTeamByIdRequest, TeamDTO, UpdateTeamDTO } from "./team/team-settings";

@Injectable()
export abstract class TeamSettingsController {
    public abstract createTeam(request: CreateTeamDTO);
    public abstract deleteTeam(request: GetTeamByIdRequest);
    public abstract updateTeam(request: UpdateTeamDTO): Observable<string>;
    public abstract getTeams(): Observable<TeamDTO[]>;
}