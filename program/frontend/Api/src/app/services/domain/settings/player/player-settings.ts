export interface PlayerDTO {
  playerId: number;
  teamId: number;
  name: string;
  nickName: string;
  age: number;
  birthday: string;
  nationality: string;
  currentTeam: string;
  marketValue: string;
  teamPosition: string;
  teamYerseyNumber: number;
}

export interface CreatePlayerDTO {
  teamId: number;
  name: string;
  nickName: string;
  age: number;
  birthday: string;
  nationality: string;
  marketValue: string;
  teamPosition: string;
  teamYerseyNumber: number;
}

export interface UpdatePlayerDTO {
  playerId: number;
  teamId: number;
  name: string;
  nickName: string;
  age: number;
  birthday: string;
  nationality: string;
  marketValue: string;
  teamPosition: string;
  teamYerseyNumber: number;
}

export interface GetPlayerByIdRequest {
  playerId: number
}