import { Injectable } from "@angular/core";
import { map } from "rxjs/operators";
import { TeamSettingsController } from "./team-settings-controller.service";
import { toTeams } from "./team/team-setting.model";
import { TeamDTO } from "./team/team-settings";

@Injectable({
    providedIn: 'root',
})
export class TeamSettingsService {

    constructor(protected controller: TeamSettingsController) { }

    createTeam(name: string, nickName: string, stadium: string, formation: string) {
        return this.controller.createTeam({ name, nickName, stadium, formation }).pipe();
    }

    updateTeam(teamId: number, logo: string, name: string, nickName: string, stadium: string, formation: string) {
        return this.controller.updateTeam({ teamId, logo, name, nickName, stadium, formation }).pipe();
    }

    deleteTeam(teamId: number) {
        return this.controller.deleteTeam({ teamId }).pipe();
    }

    getTeams() {
        return this.controller.getTeams().pipe(map((response: TeamDTO[]) => {
            return response ? toTeams(response) : null;
        }));
    }

}