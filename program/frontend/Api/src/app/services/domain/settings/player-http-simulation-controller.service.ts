import { HttpHeaders, HttpParameterCodec, HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { PlayerSettingsController } from "./player-settings-controller.service";
import { CreatePlayerDTO, GetPlayerByIdRequest, PlayerDTO, UpdatePlayerDTO } from "./player/player-settings";

@Injectable()
export class PlayerHttpSettingsController implements PlayerSettingsController {
    private readonly BASE_URL = `https://localhost:5001/api/player`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }
    public createPlayer(request: CreatePlayerDTO) {
        return this.httpClient.post(`${this.BASE_URL}`, request).pipe();
    }
    public deletePlayer(id: GetPlayerByIdRequest) {
        return this.httpClient.delete(`${this.BASE_URL}/${id.playerId}`).pipe();
    }
    public updatePlayer(request: UpdatePlayerDTO): Observable<string> {
        return this.httpClient.put(`${this.BASE_URL}/${request.playerId}`, request).pipe(map((res: string) => res));
    }
    public getPlayers(): Observable<PlayerDTO[]> {
        return this.httpClient.get(`${this.BASE_URL}`).pipe(
            map((res: PlayerDTO[]) => res)
        );
    }
}
