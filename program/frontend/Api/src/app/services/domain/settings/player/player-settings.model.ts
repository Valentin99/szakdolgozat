import { PlayerDTO } from "./player-settings";

export interface Player {
    playerId: number,
    teamId: number;
    name: string;
    nickName: string;
    age: number;
    birthday: string;
    nationality: string;
    currentTeam: string;
    marketValue: string;
    teamPosition: string;
    teamYerseyNumber: number;
}

export function toPlayers(playerResponse: PlayerDTO[]): Player[] {
    return playerResponse.map(toPlayer);
}

export function toPlayer(playerDTO: PlayerDTO): Player {
    return {
        playerId: playerDTO.playerId,
        teamId: playerDTO.teamId,
        name: playerDTO.name,
        nickName: playerDTO.nickName,
        age: playerDTO.age,
        birthday: playerDTO.birthday,
        nationality: playerDTO.nationality,
        currentTeam: playerDTO.currentTeam,
        marketValue: playerDTO.marketValue,
        teamPosition: playerDTO.teamPosition,
        teamYerseyNumber: playerDTO.teamYerseyNumber
    };
}