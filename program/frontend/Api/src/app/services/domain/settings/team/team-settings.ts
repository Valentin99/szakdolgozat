export interface TeamDTO {
  teamId: number;
  name: string;
  nickName: string;
  logo: string;
  stadium: string;
  formation: string;
}

export interface CreateTeamDTO {
  name: string;
  nickName: string;
  stadium: string;
  formation: string;
}

export interface UpdateTeamDTO {
  teamId: number;
  logo: string;
  name: string;
  nickName: string;
  stadium: string;
  formation: string;
}

export interface Formation {
  value: string;
  text: string;
}

export interface League {
  value: string;
  text: string;
}

export interface GetTeamByIdRequest {
  teamId: number;
}