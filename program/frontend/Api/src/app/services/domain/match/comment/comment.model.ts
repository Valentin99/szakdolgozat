import { CommentDTO } from './comment';

export interface Comment {
    description: string;
    minute: number;
}

export function toComments(commentResponse: CommentDTO[]): Comment[] {
    return commentResponse.map(dto => toComment(dto));
}

export function toComment(commentDTO: CommentDTO): Comment {
    return {

        description: commentDTO.description,
        minute: commentDTO.minute
    };
}
