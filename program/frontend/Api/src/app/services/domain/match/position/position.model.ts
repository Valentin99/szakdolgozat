import { PositionDTO } from './position';

export interface Position {
    type: string;
    y: number;
    x: number;
}

export function toPositionList(positionResponse: PositionDTO[]): Position[] {
    return positionResponse.map(dto => toPosition(dto));
}

export function toPosition(positionDTO: PositionDTO): Position {
    return {
        x: positionDTO.x,
        y: positionDTO.y,
        type: positionDTO.type
    };
}


