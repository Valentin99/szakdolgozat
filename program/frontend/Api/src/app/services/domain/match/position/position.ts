export interface PositionDTO {
    type: string;
    y: number;
    x: number;
}

export interface GetPlayerPositionsRequest {
    matchId: number;
}
