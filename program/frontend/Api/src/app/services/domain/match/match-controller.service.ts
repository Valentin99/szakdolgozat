import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GetCommentsByMatchIdRequest, CommentDTO } from 'src/app/services/domain/match/comment/comment';
import { GetLineupByMatchIdRequest, LineupDTO } from 'src/app/services/domain/match/lineup/lineup';
import { GetMatchResultsByTeamIdRequest, MatchDTO } from 'src/app/services/domain/match/list/match';
import { GetMatchReportByIdRequest, MatchReportDTO } from 'src/app/services/domain/match/report/report';
import { GetMatchStatisticsByIdRequest, MatchStatisticsDTO } from 'src/app/services/domain/match/statistics/statistics';
import { GetMatchHeadlineRequest, GetMatchResultRequest, MatchResultDTO, SimulationHeadLineDTO } from '../simulation/headline/headline';
import { GetPlayerPositionsRequest, PositionDTO } from './position/position';

@Injectable()
export abstract class MatchController {
    public abstract getCommentsById(request: GetCommentsByMatchIdRequest): Observable<CommentDTO[]>;
    public abstract getLineupById(request: GetLineupByMatchIdRequest): Observable<LineupDTO[]>;
    public abstract getMatchListById(request: GetMatchResultsByTeamIdRequest): Observable<MatchDTO[]>;                      // főoldal mérközések kilistázása
    public abstract getMatchReportById(request: GetMatchReportByIdRequest): Observable<MatchReportDTO[]>;
    public abstract getMatchStatisticsById(request: GetMatchStatisticsByIdRequest): Observable<MatchStatisticsDTO[]>;
    public abstract getPlayerPositionList(request: GetPlayerPositionsRequest): Observable<PositionDTO[]>;
    public abstract getMatchHeadline(request: GetMatchHeadlineRequest): Observable<SimulationHeadLineDTO>;
    public abstract getMatchResult(request: GetMatchResultRequest): Observable<MatchResultDTO>;
}
