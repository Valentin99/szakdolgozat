export interface CommentDTO {
    description: string;
    minute: number;
}

export interface GetCommentsByMatchIdRequest {
    matchId: number;
    start: number;
    end: number;
}
