export interface MatchStatisticsDTO {
  homeTeam: number;
  awayTeam: number;
  type: string;
}

export interface GetMatchStatisticsByIdRequest {
    matchId: number;
    minute: number;
}
