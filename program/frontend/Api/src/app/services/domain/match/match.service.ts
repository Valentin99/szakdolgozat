import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatchResultDTO, SimulationHeadLineDTO } from '../simulation/headline/headline';
import { toMatchResult, toSimulationHeadLine } from '../simulation/headline/headline.model';
import { CommentDTO, GetCommentsByMatchIdRequest } from './comment/comment';
import { toComments } from './comment/comment.model';
import { GetLineupByMatchIdRequest, LineupDTO } from './lineup/lineup';
import { toLineupList } from './lineup/lineup.model';
import { GetMatchResultsByTeamIdRequest, MatchDTO } from './list/match';
import { toMatchList } from './list/match.model';
import { MatchController } from './match-controller.service';
import { PositionDTO } from './position/position';
import { toPositionList } from './position/position.model';
import { GetMatchReportByIdRequest, MatchReportDTO } from './report/report';
import { toMatchReportList } from './report/report.model';
import { GetMatchStatisticsByIdRequest, MatchStatisticsDTO } from './statistics/statistics';
import { toMatchStatisticsList } from './statistics/statistics.model';

@Injectable({
    providedIn: 'root',
})
export class MatchService {

    constructor(protected controller: MatchController) { }

    getMatchResult(matchId: number, minute: number): Observable<MatchResultDTO> {
        return this.controller.getMatchResult({matchId, minute}).pipe(map((response: MatchResultDTO) => {
            return response ?  toMatchResult(response) : null;
        }));
    }

    getMatchHeadline(matchId: number): Observable<SimulationHeadLineDTO> {
        return this.controller.getMatchHeadline({ matchId }).pipe(map((response: SimulationHeadLineDTO) => {
            return response ? toSimulationHeadLine(response) : null;
        }));
    }

    getPlayerPositionList(matchId: number): Observable<PositionDTO[]> {
        return this.controller.getPlayerPositionList({ matchId }).pipe(map((response: PositionDTO[]) => {
            return response ? toPositionList(response) : null;
        }));
    }

    getLineupById(matchId: number): Observable<LineupDTO[]> {
        return this.controller.getLineupById({matchId}).pipe(map((response: LineupDTO[]) => {
            return response ? toLineupList(response) : null;
        }));
    }

    getCommentsById(matchId: number, start: number, end: number): Observable<CommentDTO[]> {
        return this.controller.getCommentsById({matchId, start, end}).pipe(map((response: CommentDTO[]) => {
            return response ? toComments(response) : null;
        }));
    }

    getMatchReportById(matchId: number, minute: number): Observable<MatchReportDTO[]> {
        return this.controller.getMatchReportById({matchId, minute}).pipe(map((response: MatchReportDTO[]) => {
            return response ? toMatchReportList(response) : null;
        }));
    }

    getMatchListById(param: GetMatchResultsByTeamIdRequest): Observable<MatchDTO[]>{
        return this.controller.getMatchListById(param).pipe(map((response: MatchDTO[]) => {
            return response ? toMatchList(response) : null;
        }));
    }

    getMatchStatisticsById(matchId: number, minute: number): Observable<MatchStatisticsDTO[]> {
        return this.controller.getMatchStatisticsById({ matchId, minute}).pipe(map((response: MatchStatisticsDTO[]) => {
            return response ? toMatchStatisticsList(response) : null;
        }));
    }

}
