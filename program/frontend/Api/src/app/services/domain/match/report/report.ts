export interface MatchReportDTO {
    playerId: number;
    description: string;
    type: string;
}

export interface GetMatchReportByIdRequest {
    matchId: number;
    minute: number;
}