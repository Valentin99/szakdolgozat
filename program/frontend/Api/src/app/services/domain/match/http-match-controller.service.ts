import { HttpClient, HttpHeaders, HttpParameterCodec } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GetMatchHeadlineRequest, GetMatchResultRequest, MatchResultDTO, SimulationHeadLineDTO } from '../simulation/headline/headline';
import { GetCommentsByMatchIdRequest, CommentDTO } from './comment/comment';
import { GetLineupByMatchIdRequest, LineupDTO } from './lineup/lineup';
import { GetMatchResultsByTeamIdRequest, MatchDTO } from './list/match';
import { MatchController } from './match-controller.service';
import { GetPlayerPositionsRequest, PositionDTO } from './position/position';
import { GetMatchReportByIdRequest, MatchReportDTO } from './report/report';
import { GetMatchStatisticsByIdRequest, MatchStatisticsDTO } from './statistics/statistics';

@Injectable()
export class HttpMatchController implements MatchController {
    private readonly BASE_URL = `https://localhost:5001/api/match`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }
    public getMatchResult(request: GetMatchResultRequest): Observable<MatchResultDTO> {
        return this.httpClient.post(`${this.BASE_URL}/result`, request).pipe(
            map((res: MatchResultDTO) => res)
        );
    }
    public getMatchHeadline(request: GetMatchHeadlineRequest): Observable<SimulationHeadLineDTO> {
        return this.httpClient.post(`${this.BASE_URL}/headline`, request).pipe(
            map((res: SimulationHeadLineDTO) => res)
        );
    }
    public getPlayerPositionList(request: GetPlayerPositionsRequest): Observable<PositionDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/position`, request).pipe(
            map((res: PositionDTO[]) => res)
        );
    }
    public getLineupById(request: GetLineupByMatchIdRequest): Observable<LineupDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/lineup`, request).pipe(
            map((res: LineupDTO[]) => res)
        );
    }
    public getMatchListById(request: GetMatchResultsByTeamIdRequest): Observable<MatchDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/list`, request).pipe(
            map((res: MatchDTO[]) => res)
        );
    }
    public getCommentsById(request: GetCommentsByMatchIdRequest): Observable<CommentDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/comment`, request).pipe(
            map((res: CommentDTO[]) => res)
        );
    }
    public getMatchReportById(request: GetMatchReportByIdRequest): Observable<MatchReportDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/report`, request).pipe(
            map((res: MatchReportDTO[]) => res)
        );
    }
    public getMatchStatisticsById(request: GetMatchStatisticsByIdRequest): Observable<MatchStatisticsDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/statistics`, request).pipe(
            map((res: MatchStatisticsDTO[]) => res)
        );
    }

}
