import { MatchReportDTO } from './report';

export interface MatchReport {
    playerId: number;
    description: string;
    type: string;
}

export function toMatchReportList(matchReportResponse: MatchReportDTO[]): MatchReport[] {
    return matchReportResponse.map(dto => toMatchReport(dto));
}

export function toMatchReport(matchReportDTO: MatchReportDTO): MatchReport {
    return {
        playerId: matchReportDTO.playerId,
        description: matchReportDTO.description,
        type: matchReportDTO.type
    };
}
