import { MatchDTO } from './match';

export interface Match {
    matchId: number;
    status: string;
    homeTeamId: number;
    awayTeamId: number;
    gameWeek: string;
    homeTeam: string;
    awayTeam: string;
    fulltime: string;
    halftime: string;
}

export function toMatchList(matchResponse: MatchDTO[]): Match[] {
    return matchResponse.map(dto => toMatch(dto));
}

export function toMatch(matchDTO: MatchDTO): Match {
    return {
        matchId: matchDTO.matchId,
        status: matchDTO.status,
        homeTeamId: matchDTO.homeTeamId,
        awayTeamId: matchDTO.awayTeamId,
        gameWeek: matchDTO.gameWeek,
        homeTeam: matchDTO.homeTeam,
        awayTeam: matchDTO.awayTeam,
        fulltime: matchDTO.fulltime,
        halftime: matchDTO.halftime
    };
}
