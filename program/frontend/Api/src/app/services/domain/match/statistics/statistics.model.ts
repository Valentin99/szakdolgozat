import { MatchStatisticsDTO } from './statistics';

export interface MatchStatistics {
    homeTeam: number;
    awayTeam: number;
    type: string;
}

export function toMatchStatisticsList(matchStatisticsResponse: MatchStatisticsDTO[]): MatchStatistics[] {
    return matchStatisticsResponse.map(dto => toMatchStatistics(dto));
}

export function toMatchStatistics(matchStatisticsDTO: MatchStatisticsDTO): MatchStatistics {
    return {
        homeTeam: matchStatisticsDTO.homeTeam,
        awayTeam: matchStatisticsDTO.awayTeam,
        type: matchStatisticsDTO.type
    };
}

