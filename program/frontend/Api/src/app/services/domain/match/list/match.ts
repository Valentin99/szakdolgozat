export interface MatchDTO {
    matchId: number;
    status: string;
    homeTeamId: number;
    awayTeamId: number;
    gameWeek: string;
    homeTeam: string;
    awayTeam: string;
    fulltime: string;
    halftime: string;
}

export interface GetMatchResultsByTeamIdRequest {
    teamId: number;
}
