import { LineupDTO, FormationDTO } from './lineup';

export interface Formation {
    home: string;
    away: string;
}

export interface Lineup {
    playerId: number;
    name: string;
    type: string;
}

export function toLineupList(lineupResponse: LineupDTO[]): Lineup[] {
    return lineupResponse.map(dto => toLineup(dto));
}

// tslint:disable-next-line: no-shadowed-variable
export function toLineup(LineupDTO: LineupDTO): Lineup {
    return {
        playerId: LineupDTO.playerId,
        name: LineupDTO.name,
        type: LineupDTO.type
    };
}

export function toFormation(formationDTO: FormationDTO): Formation {
    return {
        home: formationDTO.home,
        away: formationDTO.away
    };
}
