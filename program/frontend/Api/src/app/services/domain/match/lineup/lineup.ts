export interface LineupDTO {
    playerId: number;
    name: string;
    type: string;
}

export interface FormationDTO {
    home: string;
    away: string;
}

export interface GetLineupByMatchIdRequest {
    matchId: number;
}
