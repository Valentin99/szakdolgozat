import { TeamResultDTO } from './result';

export interface TeamResult {
    matchId: number;
    homeTeam: string;
    awayTeam: string;
    fulTimeResult: string;
    halfTimeResult: string;
}

export function toTeamResults(resultResponse: TeamResultDTO[]): TeamResult[] {
    return resultResponse.map(dto => toResult(dto));
}

export function toResult(resultDTO: TeamResultDTO): TeamResult {
    return {
        matchId: resultDTO.matchId,
        homeTeam: resultDTO.homeTeam,
        awayTeam: resultDTO.awayTeam,
        fulTimeResult: resultDTO.fulTimeResult,
        halfTimeResult: resultDTO.halfTimeResult
    };
}
