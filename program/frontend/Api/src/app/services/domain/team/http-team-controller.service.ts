import { HttpHeaders, HttpParameterCodec, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GetTeaHeadlineByIdRequest, TeamHeadlineDTO } from './headline/headline';
import { GetTeamPlayersByIdRequest, TeamPlayerDTO } from './player/player';
import { GetTeamResultsById, TeamResultDTO } from './result/result';
import { TeamController } from './team-controller.service';
import { GetTeamTransferRequest, TeamTransferDTO } from './transfer/transfer';

@Injectable()
export class HttpTeamController implements TeamController {
    private readonly BASE_URL = `https://localhost:5001/api/team`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }
    public getTeamResultsById(request: GetTeamResultsById): Observable<TeamResultDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/result`, request).pipe(
            map((res: TeamResultDTO[]) => res)
        );
    }
    public getTeamTransfersById(request: GetTeamTransferRequest): Observable<TeamTransferDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/transfer`, request).pipe(
            map((res: TeamTransferDTO[]) => res)
        );
    }
    public getTeamPlayersById(request: GetTeamPlayersByIdRequest): Observable<TeamPlayerDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/player`, request).pipe(
            map((res: TeamPlayerDTO[]) => res)
        );
    }
    public getTeamHeadlineById(request: GetTeaHeadlineByIdRequest): Observable<TeamHeadlineDTO> {
        return this.httpClient.post(`${this.BASE_URL}/headline`, request).pipe(
            map((res: TeamHeadlineDTO) => res)
        );
    }
}
