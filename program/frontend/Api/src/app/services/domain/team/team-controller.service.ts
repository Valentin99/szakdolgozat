import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GetTeaHeadlineByIdRequest, TeamHeadlineDTO } from './headline/headline';
import { GetTeamPlayersByIdRequest, TeamPlayerDTO } from './player/player';
import { GetTeamResultsById, TeamResultDTO } from './result/result';
import { GetTeamTransferRequest, TeamTransferDTO } from './transfer/transfer';

@Injectable()
export abstract class TeamController {
    public abstract getTeamHeadlineById(request: GetTeaHeadlineByIdRequest): Observable<TeamHeadlineDTO>;
    public abstract getTeamPlayersById(request: GetTeamPlayersByIdRequest): Observable<TeamPlayerDTO[]>;
    public abstract getTeamResultsById(request: GetTeamResultsById): Observable<TeamResultDTO[]>;
    public abstract getTeamTransfersById(request: GetTeamTransferRequest): Observable<TeamTransferDTO[]>;
}
