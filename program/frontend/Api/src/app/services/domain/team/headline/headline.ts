export interface TeamHeadlineDTO {
    title: string;
    logo: string;
}

export interface GetTeaHeadlineByIdRequest {
    teamId: number;
}
