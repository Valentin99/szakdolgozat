import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GetTeaHeadlineByIdRequest, TeamHeadlineDTO } from './headline/headline';
import { toTeamHeadline } from './headline/headline.model';
import { GetTeamPlayersByIdRequest, TeamPlayerDTO } from './player/player';
import { toTeamPlayers } from './player/player.model';
import { GetTeamResultsById, TeamResultDTO } from './result/result';
import { toTeamResults } from './result/result.model';
import { TeamController } from './team-controller.service';
import { GetTeamTransferRequest, TeamTransferDTO } from './transfer/transfer';
import { toTeamTransferList } from './transfer/transfer.model';

@Injectable({
    providedIn: 'root',
})
export class TeamService {

    constructor(private controller: TeamController) { }

    getTeamHeadlineById(teamId: number): Observable<TeamHeadlineDTO> {
        return this.controller.getTeamHeadlineById({ teamId }).pipe(map((response: TeamHeadlineDTO) => {
            return response ? toTeamHeadline(response) : null;
        }));
    }

    getTeamPlayersById(teamId: number): Observable<TeamPlayerDTO[]> {
        return this.controller.getTeamPlayersById({ teamId }).pipe(map((response: TeamPlayerDTO[]) => {
            return response ? toTeamPlayers(response) : null;
        }));
    }

    getTeamResultsById(teamId: number): Observable<TeamResultDTO[]> {
        return this.controller.getTeamResultsById({ teamId }).pipe(map((response: TeamResultDTO[]) => {
            return response ? toTeamResults(response) : null;
        }));
    }

    getTeamTransfersById(teamId: number): Observable<TeamTransferDTO[]> {
        return this.controller.getTeamTransfersById({ teamId }).pipe(map((response: TeamTransferDTO[]) => {
            return response ? toTeamTransferList(response) : null;
        }));
    }

}
