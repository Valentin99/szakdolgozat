export interface TeamResultDTO {
    matchId: number;
    homeTeam: string;
    awayTeam: string;
    fulTimeResult: string;
    halfTimeResult: string;
}

export interface GetTeamResultsById {
    teamId: number;
}
