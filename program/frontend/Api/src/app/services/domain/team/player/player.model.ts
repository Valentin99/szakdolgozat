import { TeamPlayerDTO } from './player';

export interface TeamPlayer {
    playerId: number;
    teamId: number;
    name: string;
    yerseyNumber: number;
    age: number;
    apps: number;
    position: string;
    goal: number;
    yellowCard: number;
    redCard: number;
}

export function toTeamPlayers(playerResponse: TeamPlayerDTO[]): TeamPlayer[] {
    return playerResponse.map(dto => toPlayer(dto));
}

export function toPlayer(playerDTO: TeamPlayerDTO): TeamPlayer {
    return {
        playerId: playerDTO.playerId,
        teamId: playerDTO.teamId,
        name: playerDTO.name,
        yerseyNumber: playerDTO.yerseyNumber,
        age: playerDTO.age,
        apps: playerDTO.apps,
        position: playerDTO.position,
        goal: playerDTO.goal,
        yellowCard: playerDTO.yellowCard,
        redCard: playerDTO.redCard
    };
}
