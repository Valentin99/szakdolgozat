export interface TeamTransferDTO {
    playerId: number;
    name: string;
    from: string;
    to: string;
    movement: string;
}

export interface GetTeamTransferRequest {
    teamId: number;
}
