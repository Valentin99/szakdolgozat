import { TeamHeadlineDTO } from './headline';

export interface TeamHeadline {
    title: string;
    logo: string;
}

export function toTeamHeadline(teamHeadLineDTO: TeamHeadlineDTO): TeamHeadline {
    return {
        title: teamHeadLineDTO.title,
        logo: teamHeadLineDTO.logo
    };
}
