import { TeamTransferDTO } from './transfer';

export interface TeamTransfer {
    playerId: number;
    name: string;
    from: string;
    to: string;
    movement: string;
}

export function toTeamTransferList(transferResponse: TeamTransferDTO[]): TeamTransfer[] {
    return transferResponse.map(dto => toTransfer(dto));
}

export function toTransfer(transferDTO: TeamTransferDTO): TeamTransfer {
    return {
        playerId: transferDTO.playerId,
        name: transferDTO.name,
        from: transferDTO.from,
        to: transferDTO.to,
        movement: transferDTO.movement
    };
}
