export interface TeamPlayerDTO {
    playerId: number;
    teamId: number;
    name: string;
    yerseyNumber: number;
    age: number;
    apps: number;
    position: string;
    goal: number;
    yellowCard: number;
    redCard: number;
}

export interface GetTeamPlayersByIdRequest {
    teamId: number;
}
