export interface SimulationDTO {
    seasonId: number;
    leagueId: number;
    roundId: number;
}

export interface SimulationData {
    id: number;
    value: number;
    text: string;
}
