export interface CleansheetDTO {
    playerId: number;
    position: number;
    playerName: string;
    cleansheet: number;
}

export interface GetCleansheetsById {
    leagueId: number;
}
