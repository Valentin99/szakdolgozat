export interface LastResultDTO {
    matchId: number;
    homeTeam: string;
    awayTeam: string;
    fulltime: string;
}

export interface GetLastResultsById {
    roundId: number;
}
