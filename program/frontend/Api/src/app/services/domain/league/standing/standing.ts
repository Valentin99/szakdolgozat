export interface TeamStandingDTO {
    teamId: number;
    position: number;
    teamName: string;
    playedGames: number;
    win: number;
    draw: number;
    defeat: number;
    logo: string;
    point: number;
}

export interface GetLeagueStandingsById {
    leagueId: number;
    seasonId: number;
    roundId: number;
}
