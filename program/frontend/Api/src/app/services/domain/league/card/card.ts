export interface YellowCardDTO {
    playerId: number;
    position: number;
    playerName: string;
    yellowCard: number;
}

export interface RedCardDTO {
    playerId: number;
    position: number;
    playerName: string;
    redCard: number;
}

export interface GetCardsById {
    leagueId: number;
    seasonId: number;
    roundId: number;
}
