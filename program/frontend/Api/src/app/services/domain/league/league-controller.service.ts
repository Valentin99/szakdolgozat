import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GetLeagueStandingsById, TeamStandingDTO } from 'src/app/services/domain/league/standing/standing';
import { GetCardsById, YellowCardDTO, RedCardDTO } from 'src/app/services/domain/league/card/card';
import { GetGoalscorerListById, GoalscorerListDTO } from 'src/app/services/domain/league/goalscorerlist/goalscorerList';
import { GetLastResultsById, LastResultDTO } from 'src/app/services/domain/league/result/lastResult';

@Injectable()
export abstract class LeagueController {
  public abstract getLeagueStandingsById(request: GetLeagueStandingsById): Observable<TeamStandingDTO[]>;
  public abstract getYellowCardsById(request: GetCardsById): Observable<YellowCardDTO[]>;
  public abstract getRedCardsById(request: GetCardsById): Observable<RedCardDTO[]>;
  public abstract getGoalScorerListById(request: GetGoalscorerListById): Observable<GoalscorerListDTO[]>;
  public abstract getLastResultsById(request: GetLastResultsById): Observable<LastResultDTO[]>;
}
