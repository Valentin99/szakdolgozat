export interface GoalscorerListDTO {
    playerId: number;
    position: number;
    playerName: string;
    goal: number;
    assist: number;
}

export interface GetGoalscorerListById {
    leagueId: number;
    seasonId: number;
    roundId: number;
}
