import { YellowCardDTO, RedCardDTO } from './card';

export interface YellowCard {
    playerId: number;
    position: number;
    playerName: string;
    yellowCard: number;
}

export interface RedCard {
    playerId: number;
    position: number;
    playerName: string;
    redCard: number;
}

export function toYellowCards(yellowCardResponse: YellowCardDTO[]): YellowCard[] {
    return yellowCardResponse.map(dto => toYellowCard(dto));
}

export function toRedCards(redCardResponse: RedCardDTO[]): RedCard[] {
    return redCardResponse.map(dto => toRedCard(dto));
}

export function toYellowCard(yellowCardDTO: YellowCardDTO): YellowCard {
    return {
        playerId: yellowCardDTO.playerId,
        position: yellowCardDTO.position,
        playerName: yellowCardDTO.playerName,
        yellowCard: yellowCardDTO.yellowCard
    };
}

export function toRedCard(redCardDTO: RedCardDTO): RedCard {
    return {
        playerId: redCardDTO.playerId,
        position: redCardDTO.position,
        playerName: redCardDTO.playerName,
        redCard: redCardDTO.redCard
    };
}
