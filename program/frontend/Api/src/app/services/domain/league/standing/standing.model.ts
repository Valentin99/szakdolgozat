import { TeamStandingDTO } from './standing';

export interface TeamStanding {
    teamId: number;
    position: number;
    teamName: string;
    playedGames: number;
    win: number;
    draw: number;
    defeat: number;
    logo: string;
    point: number;
}

export function toStandings(teamStandingDTO: TeamStandingDTO[]): TeamStanding[] {
    return teamStandingDTO.map(dto => toTeamStanding(dto));
}

export function toTeamStanding(teamStandingDTO: TeamStandingDTO): TeamStanding {
    return {
        teamId: teamStandingDTO.teamId,
        position: teamStandingDTO.position,
        teamName: teamStandingDTO.teamName,
        playedGames: teamStandingDTO.playedGames,
        win: teamStandingDTO.win,
        draw: teamStandingDTO.draw,
        defeat: teamStandingDTO.defeat,
        logo: teamStandingDTO.logo,
        point: teamStandingDTO.point
    };
}

