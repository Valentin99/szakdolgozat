import { HttpClient, HttpHeaders, HttpParameterCodec } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LeagueController } from './league-controller.service';
import { GetLeagueStandingsById, TeamStandingDTO } from 'src/app/services/domain/league/standing/standing';
import { GetCardsById, YellowCardDTO, RedCardDTO } from 'src/app/services/domain/league/card/card';
import { GetGoalscorerListById, GoalscorerListDTO } from 'src/app/services/domain/league/goalscorerlist/goalscorerList';
import { GetLastResultsById, LastResultDTO } from 'src/app/services/domain/league/result/lastResult';

@Injectable()
export class HttpLeagueController implements LeagueController {
    private readonly BASE_URL = `https://localhost:5001/api/league`;
    public defaultHeaders = new HttpHeaders();
    public encoder: HttpParameterCodec;
    constructor(private httpClient: HttpClient) { }
    public getLastResultsById(request: GetLastResultsById): Observable<LastResultDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/result`, request).pipe(
            map((res: LastResultDTO[]) => res)
        );
    }
    public getGoalScorerListById(request: GetGoalscorerListById): Observable<GoalscorerListDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/goalscorer`, request).pipe(
            map((res: GoalscorerListDTO[]) => res)
        );
    }
    public getYellowCardsById(request: GetCardsById): Observable<YellowCardDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/yellowcard`, request).pipe(
            map((res: YellowCardDTO[]) => res)
        );
    }
    public getRedCardsById(request: GetCardsById): Observable<RedCardDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/redcard`, request).pipe(
            map((res: RedCardDTO[]) => res)
        );
    }
    public getLeagueStandingsById(request: GetLeagueStandingsById): Observable<TeamStandingDTO[]> {
        return this.httpClient.post(`${this.BASE_URL}/standing`, request).pipe(
            map((res: TeamStandingDTO[]) => res)
        );
    }
}
