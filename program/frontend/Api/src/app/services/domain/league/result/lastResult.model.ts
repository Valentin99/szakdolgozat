import { LastResultDTO } from './lastResult';

export interface LastResult {
    matchId: number;
    homeTeam: string;
    awayTeam: string;
    fulltime: string;
}

export function toLastResults(lastResultResponse: LastResultDTO[]): LastResult[] {
    return lastResultResponse.map(dto => toLastResult(dto));
}

export function toLastResult(lastResultDTO: LastResultDTO): LastResult {
    return {
        matchId: lastResultDTO.matchId,
        homeTeam: lastResultDTO.homeTeam,
        awayTeam: lastResultDTO.awayTeam,
        fulltime: lastResultDTO.fulltime
    };
}

