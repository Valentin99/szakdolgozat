import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LeagueController } from './league-controller.service';
import { map } from 'rxjs/operators';
import { TeamStandingDTO } from 'src/app/services/domain/league/standing/standing';
import { toStandings } from 'src/app/services/domain/league/standing/standing.model';
import { YellowCardDTO, RedCardDTO } from 'src/app/services/domain/league/card/card';
import { toYellowCards, toRedCards } from 'src/app/services/domain/league/card/card.model';
import { CleansheetDTO } from 'src/app/services/domain/league/cleansheet/cleansheet';
import { toCleansheets } from 'src/app/services/domain/league/cleansheet/cleansheet.model';
import { GoalscorerListDTO } from 'src/app/services/domain/league/goalscorerlist/goalscorerList';
import { toGoalscorerList } from 'src/app/services/domain/league/goalscorerlist/goalscorerList.model';
import { LastResultDTO } from 'src/app/services/domain/league/result/lastResult';
import { toLastResults } from 'src/app/services/domain/league/result/lastResult.model';

@Injectable({
  providedIn: 'root',
})
export class LeagueService {

  constructor(protected controller: LeagueController) { }

  getLeagueStandingsById(leagueId: number, seasonId: number, roundId: number): Observable<TeamStandingDTO[]> {
    return this.controller.getLeagueStandingsById({ leagueId, seasonId, roundId }).pipe(map((response: TeamStandingDTO[]) => {
      return response ? toStandings(response) : null;
    }));
  }

  getYellowCardsById(leagueId: number, seasonId: number, roundId: number): Observable<YellowCardDTO[]> {
    return this.controller.getYellowCardsById({ leagueId, seasonId, roundId }).pipe(map((response: YellowCardDTO[]) => {
      return response ? toYellowCards(response) : null;
    }));
  }

  getRedCardsById(leagueId: number, seasonId: number, roundId: number): Observable<RedCardDTO[]> {
    return this.controller.getRedCardsById({ leagueId, seasonId, roundId }).pipe(map((response: RedCardDTO[]) => {
      return response ? toRedCards(response) : null;
    }));
  }

  getGoalscorerListById(leagueId: number, seasonId: number, roundId: number): Observable<GoalscorerListDTO[]> {
    return this.controller.getGoalScorerListById({ leagueId, seasonId, roundId }).pipe(map((response: GoalscorerListDTO[]) => {
      return response ? toGoalscorerList(response) : null;
    }));
  }

  getLastResultstById(roundId: number): Observable<LastResultDTO[]> {
    return this.controller.getLastResultsById({ roundId }).pipe(map((response: LastResultDTO[]) => {
      return response ? toLastResults(response) : null;
    }));
  }

}
