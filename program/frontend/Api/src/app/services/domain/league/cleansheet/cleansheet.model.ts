import { CleansheetDTO } from './cleansheet';

export interface Cleansheet {
    playerId: number;
    position: number;
    playerName: string;
    cleansheet: number;
}

export function toCleansheets(cleansheetResponse: CleansheetDTO[]): Cleansheet[] {
    return cleansheetResponse.map(dto => toCleansheet(dto));
}

export function toCleansheet(cleansheetDTO: CleansheetDTO): Cleansheet {
    return {
        playerId: cleansheetDTO.playerId,
        position: cleansheetDTO.position,
        playerName: cleansheetDTO.playerName,
        cleansheet: cleansheetDTO.cleansheet
    };
}

