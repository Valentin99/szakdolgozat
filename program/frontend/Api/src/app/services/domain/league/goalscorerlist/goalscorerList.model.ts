import { GoalscorerListDTO } from './goalscorerList';

export interface GoalscorerList {
    playerId: number;
    position: number;
    playerName: string;
    goal: number;
    assist: number;
}

export function toGoalscorerList(goalscorerListResponse: GoalscorerListDTO[]): GoalscorerList[] {
    return goalscorerListResponse.map(dto => toGoalscorer(dto));
}

export function toGoalscorer(goalscorerListDTO: GoalscorerListDTO) : GoalscorerList {
    return {
        playerId: goalscorerListDTO.playerId,
        position: goalscorerListDTO.position,
        playerName: goalscorerListDTO.playerName,
        goal: goalscorerListDTO.goal,
        assist: goalscorerListDTO.assist
    };
}

