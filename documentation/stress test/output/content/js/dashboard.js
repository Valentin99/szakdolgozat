/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.3486666666666667, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.16666666666666666, 500, 1500, "Lineup"], "isController": false}, {"data": [0.3, 500, 1500, "Team transfer"], "isController": false}, {"data": [0.1, 500, 1500, "Team match result"], "isController": false}, {"data": [0.6833333333333333, 500, 1500, "Cached comments"], "isController": false}, {"data": [0.38333333333333336, 500, 1500, "Team headline"], "isController": false}, {"data": [0.5333333333333333, 500, 1500, "Player headline"], "isController": false}, {"data": [0.75, 500, 1500, "Match headline"], "isController": false}, {"data": [0.5, 500, 1500, "Get league by id"], "isController": false}, {"data": [0.11666666666666667, 500, 1500, "Red cards"], "isController": false}, {"data": [0.31666666666666665, 500, 1500, "Generate round"], "isController": false}, {"data": [0.0, 500, 1500, "Standings"], "isController": false}, {"data": [0.26666666666666666, 500, 1500, "Round results"], "isController": false}, {"data": [0.5166666666666667, 500, 1500, "League headline"], "isController": false}, {"data": [0.5833333333333334, 500, 1500, "Match result"], "isController": false}, {"data": [0.0, 500, 1500, "Goalscorers"], "isController": false}, {"data": [0.0, 500, 1500, "Get all leagues"], "isController": false}, {"data": [0.48333333333333334, 500, 1500, "Report"], "isController": false}, {"data": [0.8, 500, 1500, "Normal comments"], "isController": false}, {"data": [0.0, 500, 1500, "Yellow cards"], "isController": false}, {"data": [0.75, 500, 1500, "Statistics"], "isController": false}, {"data": [0.13333333333333333, 500, 1500, "Player match list"], "isController": false}, {"data": [0.25, 500, 1500, "Player career"], "isController": false}, {"data": [0.36666666666666664, 500, 1500, "Generate match"], "isController": false}, {"data": [0.7, 500, 1500, "Positions"], "isController": false}, {"data": [0.016666666666666666, 500, 1500, "Team players"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 750, 0, 0.0, 2104.6759999999995, 69, 12817, 1075.0, 4209.3, 6299.449999999988, 12700.78, 13.231013495633764, 27.863601344050455, 2.8151048557819527], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["Lineup", 30, 0, 0.0, 1822.8666666666668, 1280, 3309, 1627.5, 2880.1000000000013, 3164.35, 3309.0, 7.589172780166963, 34.48478608019226, 1.541550720971414], "isController": false}, {"data": ["Team transfer", 30, 0, 0.0, 1604.0666666666668, 199, 4195, 1448.5, 3747.4000000000024, 4114.15, 4195.0, 2.653458340704051, 10.559416736688485, 0.5389837254555104], "isController": false}, {"data": ["Team match result", 30, 0, 0.0, 2325.033333333333, 1072, 4897, 2219.0, 4145.300000000001, 4718.25, 4897.0, 2.318750966146236, 7.5993439867058274, 0.4664674795176998], "isController": false}, {"data": ["Cached comments", 30, 0, 0.0, 723.6333333333336, 247, 1344, 699.0, 1183.8000000000002, 1308.8, 1344.0, 7.520681875156681, 53.94351066996741, 1.7391576836299825], "isController": false}, {"data": ["Team headline", 30, 0, 0.0, 1156.0333333333333, 447, 2205, 1129.0, 1731.8000000000002, 2179.15, 2205.0, 3.084832904884319, 0.6085314910025708, 0.6266066838046273], "isController": false}, {"data": ["Player headline", 30, 0, 0.0, 856.3, 279, 2637, 789.0, 1238.6000000000004, 2205.2499999999995, 2637.0, 4.948861761794787, 1.319374278290993, 1.024569036621577], "isController": false}, {"data": ["Match headline", 30, 0, 0.0, 612.4, 279, 1171, 493.0, 915.2, 1032.9499999999998, 1171.0, 9.674298613350532, 2.7397915994840374, 1.983987020316027], "isController": false}, {"data": ["Get league by id", 30, 0, 0.0, 913.2333333333332, 828, 1020, 919.0, 976.9, 998.0, 1020.0, 27.675276752767527, 5.459380765682656, 5.54046067804428], "isController": false}, {"data": ["Red cards", 30, 0, 0.0, 2123.3, 1210, 3179, 1684.0, 3166.9, 3172.4, 3179.0, 3.2811987312698236, 2.448081865908345, 0.778643839549382], "isController": false}, {"data": ["Generate round", 30, 0, 0.0, 1246.1333333333334, 448, 2696, 915.5, 2160.2000000000007, 2652.5499999999997, 2696.0, 5.501558774986246, 8.263083394461765, 1.1443672061250687], "isController": false}, {"data": ["Standings", 30, 0, 0.0, 2357.5999999999995, 1745, 3565, 2257.5, 2879.9, 3198.6999999999994, 3565.0, 8.361204013377925, 26.814642558528426, 2.0004833821070234], "isController": false}, {"data": ["Round results", 30, 0, 0.0, 1790.3666666666668, 656, 6160, 977.5, 4078.0000000000027, 5087.499999999998, 6160.0, 4.173042147725692, 4.55203913965781, 0.8517244227291696], "isController": false}, {"data": ["League headline", 30, 0, 0.0, 952.3333333333331, 158, 2957, 1016.5, 1068.5, 2573.0999999999995, 2957.0, 7.52823086574655, 1.4556540150564619, 1.5512272584692597], "isController": false}, {"data": ["Match result", 30, 0, 0.0, 614.7000000000002, 303, 1637, 528.5, 863.9, 1247.0499999999995, 1637.0, 7.745933384972889, 1.2708171959721146, 1.701987316034082], "isController": false}, {"data": ["Goalscorers", 30, 0, 0.0, 4217.800000000001, 2001, 7332, 4183.5, 6426.7, 7329.25, 7332.0, 2.700756211739287, 5.364588022146201, 0.6514519377925819], "isController": false}, {"data": ["Get all leagues", 30, 0, 0.0, 12537.700000000003, 12125, 12817, 12612.0, 12810.8, 12816.45, 12817.0, 2.33772305774176, 0.5912795624561676, 0.3858156218343334], "isController": false}, {"data": ["Report", 30, 0, 0.0, 789.8999999999999, 513, 1562, 741.5, 1067.0, 1391.4999999999998, 1562.0, 5.782575173477255, 7.787276527563609, 1.2592912731303008], "isController": false}, {"data": ["Normal comments", 30, 0, 0.0, 430.69999999999993, 69, 1050, 428.0, 694.9000000000001, 876.1999999999998, 1050.0, 8.130081300813009, 71.99568089430895, 1.9293064024390245], "isController": false}, {"data": ["Yellow cards", 30, 0, 0.0, 4502.066666666667, 3117, 6211, 4176.5, 5860.3, 6028.4, 6211.0, 2.5033377837116157, 11.641498560580775, 0.603832453688251], "isController": false}, {"data": ["Statistics", 30, 0, 0.0, 516.8000000000001, 190, 892, 498.5, 742.7, 843.05, 892.0, 7.078810759792354, 6.069527194431335, 1.5761414582350164], "isController": false}, {"data": ["Player match list", 30, 0, 0.0, 2395.6, 1046, 3615, 2249.5, 3602.1000000000004, 3614.45, 3615.0, 3.3890646181653863, 6.662292066764572, 0.6917133839810212], "isController": false}, {"data": ["Player career", 30, 0, 0.0, 2042.2333333333331, 988, 4045, 1482.5, 3624.0, 3832.1499999999996, 4045.0, 3.964583058015066, 1.0724506904982158, 0.8130492599444958], "isController": false}, {"data": ["Generate match", 30, 0, 0.0, 1250.6999999999996, 247, 2583, 1057.5, 2473.0, 2523.0499999999997, 2583.0, 4.628915290850177, 0.41587910816232065, 0.9673709689862676], "isController": false}, {"data": ["Positions", 30, 0, 0.0, 513.7666666666667, 271, 932, 529.0, 730.8, 852.8, 932.0, 9.04431715405487, 9.521263566475731, 1.8547916038589085], "isController": false}, {"data": ["Team players", 30, 0, 0.0, 4321.633333333331, 1047, 6942, 4671.5, 5847.700000000001, 6471.199999999999, 6942.0, 2.7570995312930795, 12.40425541080783, 0.5573433622828784], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 750, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
